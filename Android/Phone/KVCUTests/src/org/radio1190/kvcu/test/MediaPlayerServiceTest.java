package org.radio1190.kvcu.test;

import android.content.Intent;
import android.test.ServiceTestCase;
import org.radio1190.kvcuappfinal.KVCUMediaPlayerService;

public class MediaPlayerServiceTest extends ServiceTestCase<KVCUMediaPlayerService> {
	// Activity Context
	KVCUMediaPlayerService mService;
	
	public MediaPlayerServiceTest(Class<KVCUMediaPlayerService> serviceClass) {
		super(serviceClass);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		// Get Service Context
		mService = getService();
	}
	
	public void testPreConditions() {
		
	}
	
	/*
	 * Test Basic startup/shutdown of Service
	 */
	public void testStartable() {
		Intent startIntent = new Intent();
		startIntent.setClass(getContext(), KVCUMediaPlayerService.class);
		startService(startIntent);
	}

}
