package org.radio1190.kvcu.test;

import org.radio1190.kvcuappfinal.KVCUMediaPlayer;
import android.test.ActivityInstrumentationTestCase2;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.ProgressBar;


public class MediaPlayerActivityTest extends ActivityInstrumentationTestCase2<KVCUMediaPlayer> {
	// Activity Context
	private KVCUMediaPlayer mActivity;
	// Buttons
	private Button startButton;
	//private Button muteButton;
	//private ProgressBar volumeSlider;
	
	@SuppressWarnings("deprecation")
	public MediaPlayerActivityTest() {
		super("org.radio1190.kvcuapp", KVCUMediaPlayer.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		// Disable Touch (has to be called before getActivity())
		setActivityInitialTouchMode(false);
		// Get Activity Context
		mActivity = getActivity();
		// Initialize UI components
		startButton = (Button) mActivity.findViewById(org.radio1190.kvcuappfinal.R.id.startButton);
		//muteButton = (Button) mActivity.findViewById(org.radio1190.kvcuapp.R.id.mute);
		//volumeSlider = (ProgressBar) mActivity.findViewById(org.radio1190.kvcuapp.R.id.volumebar);
	}
	
	public void testPreConditions() {
		// Enabled
		assertTrue(startButton.isEnabled());
		//assertTrue(muteButton.isEnabled());
		//assertTrue(volumeSlider.isEnabled());
	}
	
	public void testStartButtonUI() {
		mActivity.runOnUiThread(new Runnable() {
			public void run() {
				startButton.requestFocus();
			}
		});
		
		this.sendKeys(KeyEvent.KEYCODE_DPAD_CENTER);
		this.sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
		// Check if it's disabled for allowing buffering
		this.sendKeys(KeyEvent.KEYCODE_DPAD_CENTER);
	}
}

