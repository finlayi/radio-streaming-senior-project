/**
 *
 * @file    KVCUSchedule.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */
package org.radio1190.kvcuappfinal;

import android.app.Activity;
import android.os.Bundle;

public class KVCUSchedule extends Activity {
	public void onCreate(Bundle saveInstanceState) {
		super.onCreate(saveInstanceState);
		setContentView(R.layout.schedule_layout);
	}
}