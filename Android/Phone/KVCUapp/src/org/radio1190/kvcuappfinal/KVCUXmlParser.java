/**
 *
 * @file    KVCUXmlParser.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */
package org.radio1190.kvcuappfinal;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Log;
import android.util.Xml;


/**
 * A KVCUXmlParser is a parser which extracts an InputStream that contains 
 * a XML page. Specifically for extracting playlist, current song, and calendar
 * events.
 * @author Ryan
 */
public class KVCUXmlParser {
	//Don't use namespaces when calling XmlPullparser
	private static final String ns = null;
	
	public List<KVCUEntry> parsePlaylist(InputStream in) throws XmlPullParserException, IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES,  false);
			parser.setInput(in, null);
			Log.v("Verbose", "Running parse()");
			parser.nextTag();
			return readPlaylist(parser);
		} finally {
			in.close();
		}
	}
	
	public KVCUEntry parseCurrentSong(InputStream in) throws XmlPullParserException, IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readCurrentSong(parser);
		} finally {
			in.close();
		}
	}
	
	public ArrayList<KVCUCalendarEventEntry> parseCalendarEvents(InputStream in) throws XmlPullParserException, IOException {
		XmlPullParser parser = Xml.newPullParser();
		parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		parser.setInput(in, null);
		Log.v("calendar", "Running parseCalendarEvents()");
		parser.nextTag();
		return readCalendarEventLists(parser);
	}
	
	public List<KVCUEventEntry> parseEventList(InputStream in) throws XmlPullParserException, IOException {
		XmlPullParser parser = Xml.newPullParser();
		parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		parser.setInput(in, null);
		Log.v("eventlist", "Running parseEventlist()");
		parser.nextTag();
		return readEventList(parser);
	}
	
	public List<KVCUEntry> parseSimilarTracks(InputStream in) throws XmlPullParserException, IOException {
		try {
			Log.v("similartrack", "Running parseSimilarTracks()");
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readSimilarTracks(parser);
		} finally {
			in.close();
		}
	}
	
	public String parseAlbumImage(InputStream in) throws XmlPullParserException, IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readAlbumImageTag(parser);
		} finally {
			in.close();
		}
	}
	
	
	public List<KVCUBlogEntry> parseBlog(InputStream in) throws XmlPullParserException, IOException {
		try {
			Log.v("blog", "Running parseBlog()");
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readBlog(parser);
		} finally {
			in.close();
		}
	}
	
	@SuppressWarnings("unused")
	private List<KVCUEntry> readSimilarTracks(XmlPullParser parser) throws IOException, XmlPullParserException {
		if (checkLFMStatus(parser)) {
			parser.require(XmlPullParser.START_TAG, ns, "similartracks");
			Log.v("similartrack", parser.getName());
			List<KVCUEntry> entries = new ArrayList<KVCUEntry>();
			final int limit = 8;
			String song = null;
			String artist = null;
			String image = null;
			int count = 0;
			
			while (parser.next() != XmlPullParser.END_TAG) {
				if (parser.getEventType() != XmlPullParser.START_TAG)
					continue;
				String name = parser.getName();
				if (name.equals("track")) {
					entries.add(readTrack(parser));
					count++;
					Log.v("similartrack", "Number of count is " + count);
				} else {
					skip(parser);
				}
				if (count == limit)
					break;
			}
			return entries;
		}
		return null;
	}
	
	private List<KVCUEventEntry> readEventList(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "events");
		Log.v("eventlist", "Running readEventList()");
		List<KVCUEventEntry> entries = new ArrayList<KVCUEventEntry>();
		String tagName = null;
		String title = null;
		String content = null;
		String imageUrl = null;
		String startDate = null;
		String endDate = null;
		String venue = null;
		String phone = null;
		String address = null;
		while (parser.next() != XmlPullParser.END_TAG){
			if (parser.getEventType() != XmlPullParser.START_TAG)
				continue;
			tagName = parser.getName();
			if (tagName.equals("title"))
				title = readTag(parser, tagName);
			else if (tagName.equals("content"))
				content = readTag(parser, tagName);
			else if (tagName.equals("imageLink"))
				imageUrl = readTag(parser, tagName);
			else if (tagName.equals("startDate"))
				startDate = readTag(parser, tagName);
			else if (tagName.equals("endDate"))
				endDate = readTag(parser, tagName);
			else if (tagName.equals("venue"))
				venue = readTag(parser, tagName);
			else if (tagName.equals("phone"))
				phone = readTag(parser, tagName);
			else if (tagName.equals("address"))
				address = readTag(parser, tagName);
			else 
				skip(parser);
			
			// Save entry and resets strings when a whole entry is found.
			if (parser.getEventType() == XmlPullParser.END_TAG && tagName.equals("address"))
			{
				Log.v("eventlist", title + content + imageUrl + startDate + endDate + venue + phone + address);
				entries.add(new KVCUEventEntry(title, content, imageUrl, startDate, endDate, venue, phone, address));
				title = content = imageUrl = startDate = endDate = venue = phone = address = null;
			}
				
		}
		return entries;
	}
	
	private ArrayList<KVCUCalendarEventEntry> readCalendarEventLists(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "events");
		Log.v("calendar", "Running readCalendarEventLists()");
		ArrayList<KVCUCalendarEventEntry> entries = new ArrayList<KVCUCalendarEventEntry>();
		String eventTitle = null;
		String eventUrl = null;
		String eventDate = null;
		String eventDay = null;
		String tagName = null;
		// Event Regular Expression Patterns
		Pattern patternTitle = Pattern.compile("event-(\\d+)(\\z)");
		Pattern patternLink = Pattern.compile("event-(\\d+)-link");
		Pattern patternDate = Pattern.compile("event-(\\d+)-date");
		Matcher matcherTitle, matcherLink, matcherDate;
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG)
				continue;
			tagName = parser.getName();
			matcherTitle = patternTitle.matcher(tagName);
			matcherLink = patternLink.matcher(tagName);
			matcherDate = patternDate.matcher(tagName);
			if (tagName.equals("day"))
				eventDay = readTag(parser, tagName);
			else if (matcherTitle.find())
				eventTitle = readTag(parser, tagName);
			else if (matcherLink.find()) {
				eventUrl = readTag(parser, tagName);
				//Log.v("calendar", tagName);
			} else if (matcherDate.find()) {
				eventDate = readTag(parser, tagName);
				// Save entry and resets strings when a whole entry is found.
				if (parser.getEventType() == XmlPullParser.END_TAG)
				{
					/*Log.v("calendar", "_______________________________");
					if (eventDate != null)
					Log.v("calendar", eventDate);
					if (eventTitle != null)
					Log.v("calendar", eventTitle);
					if (eventDay != null)
					Log.v("calendar", eventDay);
					if (eventUrl != null)
					Log.v("calendar", eventUrl);*/
					entries.add(new KVCUCalendarEventEntry(eventTitle, eventUrl, eventDate, eventDay));
					eventTitle = eventUrl = eventDate = null;
				}
			} else 
				skip(parser);
		}
		return entries;
	}
	
	private String readAlbumImageTag(XmlPullParser parser) throws XmlPullParserException, IOException {
		if (checkLFMStatus(parser)) {
			String albumUrl = "";
			parser.require(XmlPullParser.START_TAG,  ns,  "album");
			while (parser.next() != XmlPullParser.END_TAG) {
				if (parser.getEventType() != XmlPullParser.START_TAG) 
					continue;
				String name = parser.getName();
				if (name.equals("image") && parser.getAttributeValue(0).equals("large")) {
					albumUrl = readAlbumImage(parser);
					if (albumUrl == null || albumUrl == "") {
						Log.v("albumimage", "albumUrl is null or \n");
						return null;
					} else {
						Log.v("albumimage", albumUrl);
					}
					return albumUrl;
				} else {
					skip(parser);
				}
			}
		}
		return null;
	}
	
	private KVCUEntry readCurrentSong(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG,  ns, "current");
		KVCUEntry entry = null;
		String song = null;
		String artist = null;
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG)
				continue;
			String name = parser.getName();
			if (name.equals("song")) {
				song = readSong(parser);
			} else if (name.equals("artist")) {
				artist = readArtist(parser);
			} else {
				skip(parser);
			}
			if (parser.getEventType() == XmlPullParser.END_TAG && name.equals("song")) {
				entry = new KVCUEntry(null, artist, song, null, null);
				break;
			}
		}
		return entry;
	}
	
	private KVCUEntry readTrack(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG,  ns, "track");
		String song = null;
		String artist = null;
		String image = null;
		while (parser.next() != XmlPullParser.END_TAG) {
			Log.v("similartrack", "Running readTrack() iteratively");
			if (parser.getEventType() != XmlPullParser.START_TAG)
				continue;
			String name = parser.getName();
			if (name.equals("name")) {
				song = readSimilarTrackSong(parser);
			} else if (name.equals("artist")) {
				artist = readSimilarTrackArtist(parser);
			} else if (name.equals("image") && parser.getAttributeValue(0).equals("large")) {
				image = readAlbumImage(parser);
			} else {
				skip(parser);
			}
		}
		return new KVCUEntry(null, artist, song, null, image);
	}
	
	// Parsers the contents of an entry. If it encounters a title, summary, or link tag, hands them off 
	// to their respective "read" methods for processing. Otherwise, skips the tag.
	private List<KVCUEntry> readPlaylist(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "playlist");
		List<KVCUEntry> entries = new ArrayList<KVCUEntry>();
		String tagName = null;
		String time = null;
		String artist = null;
		String song = null;
		String album = null;
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG)
				continue;
			tagName = parser.getName();
			if (tagName.equals("time")) {
				time = readTime(parser);
			} else if (tagName.equals("artist")) {
				artist = readArtist(parser);
			} else if (tagName.equals("song")) {
				song = readSong(parser);
			} else if (tagName.equals("album")) {
				album = readAlbum(parser);
			} else {
				skip(parser);
			}
			// Save entry and resets strings when a whole entry is found.
			if (parser.getEventType() == XmlPullParser.END_TAG && tagName.equals("album"))
			{
				entries.add(new KVCUEntry(time, artist, song, album, null));
				time = null;
				artist = null;
				song = null;
				album = null;
			}
		}
		return entries;
	}
	
	private List<KVCUBlogEntry> readBlog(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "events");
		List<KVCUBlogEntry> entries = new ArrayList<KVCUBlogEntry>();
		String tagName = null;
		String title = null;
		String content = null;
		String imageLink = null;
		String blogLink = null;
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) 
				continue;
			tagName = parser.getName();
			if (tagName.equals("title")) {
				title = readTag(parser, tagName);
				Log.v("blog", "Blog Read title");
			} else if (tagName.equals("content")) {
				content = readTag(parser, tagName);
				Log.v("blog", "Blog Read content");
			} else if (tagName.equals("imageLink")) {
				imageLink = readTag(parser, tagName);
				Log.v("blog", "Blog Read imageLink");
			} else if (tagName.equals("blogLink")) {
				blogLink = readTag(parser, tagName);
				Log.v("blog", "Blog Read blogLink");
			} else {
				skip(parser);
			}
			if (parser.getEventType() == XmlPullParser.END_TAG && tagName.equals("blogLink")) {
				entries.add(new KVCUBlogEntry(title, content, imageLink, blogLink));
				title = null;
				content = null;
				imageLink = null;
				blogLink = null;
			}
		}
		return entries;
	}
	// Processes time tags in the feed.
	private String readTime(XmlPullParser parser) throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, "time");
		String time = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "time");
		Log.v("Verbose", "Time worked");
		return time;
	}
	// Processes artist tags in the feed.
	private String readArtist(XmlPullParser parser) throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, "artist");
		String artist = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "artist");
		Log.v("Verbose", "Artist worked");
		return artist;
	}
	// Processes song tags in the feed.
	private String readSong(XmlPullParser parser) throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, "song");
		String song = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "song");
		Log.v("Verbose", "Song worked");
		return song;
	}
	// Processes album tags in the feed.
	private String readAlbum(XmlPullParser parser) throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, "album");
		String album = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "album");
		Log.v("Verbose", "Album worked");
		return album;
	}
	
	// Process image tags in the feed for similar songs album cover url.
	private String readAlbumImage(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "image");
		String image = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "image");
		Log.v("similartrack", "Image worked = " + image);
		return image;
	}
	private String readSimilarTrackSong(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "name");
		String name = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "name");
		Log.v("similartrack", "readSimilarTrackSong() worked = " + name);
		return name;
	}
	
	private String readTag(XmlPullParser parser, String tag) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, tag);
		String text = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, tag);
		Log.v("readtag", tag + " has been read: " + text);
		return text;
	}
	
	private String readSimilarTrackArtist(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "artist");
		String artist = null;
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG)
				continue;
			String name = parser.getName();
			if (name.equals("name")) {
				parser.require(XmlPullParser.START_TAG, ns, "name");
				artist = readText(parser);
				parser.require(XmlPullParser.END_TAG, ns, "name");
			} else {
				skip(parser);
			}
		}
		parser.require(XmlPullParser.END_TAG, ns, "artist");
		Log.v("similartrack", "readSimilarSongArtist() worked! = " + artist);
		return artist;
	}
	private boolean checkLFMStatus(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "lfm");
		Log.v("similartrack", "status=" + parser.getAttributeValue(0));
		if (parser.getAttributeValue(0).equals("ok")) {
			parser.nextTag();
			return true;
		}
		else {
			return false;
		}
	}
	// For the tags, extracts their text values.
	private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
		String result = null;
		Log.v("Verbose", "Running readText()");
		if (parser.next() == XmlPullParser.TEXT) {
			result = parser.getText();
			parser.nextTag();
		}
		return result;
	}
	// Skips tags in the parser isn't interested in. Uses depth to handle nested tags. i.e.,
	// if the next tag after a START_TAG isn't a matching END_TAG, it keeps going until it 
	// finds the matching END_TAG (as indicated by the values of "depth" being 0);
	private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}
}