/**
 *
 * @file    KVCUContacts.java
 * @project KVCUapp
 *
 * @creator Katie Pham
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */

package org.radio1190.kvcuappfinal;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * KVCUAbout implements the functionalities of the Contacts view. It contains the necessary information to 
 * contact the Radio 1190/KVCU team.
 * @author Katie
 */

public class KVCUContacts extends Activity {
    /** 
     * Buttons below for specific contacts (Facebook, Twitter, Instagram), along with URLs/Intents
     */
    private Button aboutButton;
    private ImageButton facebookButton;
    private ImageButton twitterButton;
    private ImageButton instagramButton;
    private final String facebookURL = "https://www.facebook.com/Radio1190";
    private final String twitterURL = "https://twitter.com/Radio1190";
    private final String instagramURL = "http://instagram.com/radio1190/";
    private Intent facebookPage;
    private Intent twitterPage;
    private Intent instagramPage;
    private TextView TextLabel;
    
    /**
     *  Static string values for the "actual" contacts--phone, email and address
     */
    private String[] title = {"Studio Line:", "Studio Email:", "Address"};
    private String[] label = {"Call", "Email", "Map"};
    private String[] info = {"303-492-1190", "dj@radio1190.org", "Radio 1190\nCampus Box 207\nUniversity of Colorado\nBoulder, CO 80303"};
    int size = title.length;    // We can really use any of the arrays for the length
    // Used for map purposes
    private final String stationAddress = "geo:0,0?q=Radio+1190%2C+University+of+Colorado+Boulder+CO+80303";
    private final String webStationAddress = "http://maps.google.com/maps?q=Radio+1190+University+of+Colorado+Boulder+CO+80303";
    
    /**
     * Everything is generated when the KVCUContacts activity is created, from the back button, to the three contacts, and to the three
     * social media links. The onClick functions for each of the buttons are also handled through onCreate.
     */
    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.contacts_layout);
        
        // Show About ("back") button
        aboutButton = (Button) findViewById(R.id.aboutButton);
        // ...And finish the activity when they want to go back
        aboutButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                KVCUContacts.this.finish();
            }
        });
        
        // Inflate the contacts_item view
        LayoutInflater inflaterCI = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewCI = inflaterCI.inflate(R.layout.contacts_item, null);

        ListView listview = (ListView) findViewById(R.id.contactItem);
        ArrayList<HashMap<String, String>> items = new ArrayList<HashMap<String, String>>();
        // Loop through size times for each contact entry
        for (int i = 0; i < size; i++) {
            HashMap<String, String> content = new HashMap<String, String>();
            content.put("title", title[i]);
            content.put("label", label[i]);
            content.put("info", info[i]);

            // Let's see if all of this is printing out right
            Log.v("title", "Title: " + title[i]);
            Log.v("label", "Label: " + label[i]);
            Log.v("info", "Info: " + info[i]);
            
            items.add(content);
        }
        
        // Create an adapter to use
        ListAdapter adapter = new SimpleAdapter(KVCUContacts.this, items, R.layout.contacts_item, new String[] {"title", "label", "info"},
                new int[] {R.id.contactTitle, R.id.contactLabel, R.id.contactInfo});
        
        // Create an inflater to use another xml layout (the Facebook/Twitter/Instagram buttons)
        LayoutInflater inflaterFooter = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View footerView = inflaterFooter.inflate(R.layout.contacts_footer, null);
        
        // Define their clicks
        facebookButton = (ImageButton) footerView.findViewById(R.id.facebookButton);
        twitterButton = (ImageButton) footerView.findViewById(R.id.twitterButton);
        instagramButton = (ImageButton) footerView.findViewById(R.id.instagramButton);
        
        facebookButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Open up the Facebook page when clicked
                facebookPage = new Intent(android.content.Intent.ACTION_VIEW);
                facebookPage.setData(Uri.parse(facebookURL));
                startActivity(facebookPage);
            }
        });
        
        twitterButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Open up the Twitter page when clicked
                twitterPage = new Intent(android.content.Intent.ACTION_VIEW);
                twitterPage.setData(Uri.parse(twitterURL));
                startActivity(twitterPage);
            }
        });

        instagramButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Open up the Instagram page when clicked
                instagramPage = new Intent(android.content.Intent.ACTION_VIEW);
                instagramPage.setData(Uri.parse(instagramURL));
                startActivity(instagramPage);
            }
        });
        
        // Add the footer to the listview, then set adapter
        // MUST BE CALLED IN THIS ORDER!
        listview.addFooterView(footerView);
        listview.setAdapter(adapter);
        
        // Allows calling, emailing, or opening the map
        // Note that this only works if we have ONE method of contact per "box"
        listview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {          
                String itemLabel = ((TextView) view.findViewById(R.id.contactLabel)).getText().toString();
                String itemInfo = ((TextView) view.findViewById(R.id.contactInfo)).getText().toString();
                Log.v("onItemClick", "itemInfo = " + itemInfo + ", itemLabel = " + itemLabel);
                
                // Call the number
                if (itemLabel.equals("Call")) {
                    try {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + itemInfo));
                        startActivity(callIntent);
                    } catch (ActivityNotFoundException activityException) {
                        Log.e("Calling a Phone Number", "Call failed", activityException);
                    }
                }
                // Email
                else if (itemLabel.equals("Email")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri data = Uri.parse("mailto:" + itemInfo);
                    intent.setData(data);
                    startActivity(intent);
                }
                // Open the map
                // If we don't have Google Maps, open up a web URL
                else if (itemLabel.equals("Map")) {
                    try {
                        Log.v("mapping", "App link: " + stationAddress);
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(stationAddress)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        Log.v("mapping", "Web link: " + webStationAddress);
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(webStationAddress)));
                    }
                }
            }
        });
    }
    
    /**
     * onPause is used for the notification widget that appears when the top bar is pulled down from the screen.
     */
    @Override
    protected void onPause() {
    	super.onPause();
    	// Show Notification Widget
    	startService(new Intent(KVCUMediaPlayerService.ACTION_SHOW_NOTIFICATION, null, getApplicationContext(), KVCUMediaPlayerService.class));
    }
}
