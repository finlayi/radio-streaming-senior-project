/**
 *
 * @file    KVCUEntry.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */

package org.radio1190.kvcuappfinal;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.radio1190.kvcuappfinal.KVCUPlaylistDetail.ImageAdapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;


/**
 * KVCUEntry is used for storing time, artist, song and album when extracted
 * by the KVCUXmlParser.
 * @author Ryan
 *
 */
public class KVCUEntry {
	public final String time;
	public final String artist;
	public final String song;
	public final String album;
	
	// For Similar tracks
	public final String image;
	private Bitmap bitMapImage;
	private ImageAdapter adapter;
	
	public KVCUEntry(String time, String artist, String song, String album, String image) {
		this.time = time;
		this.artist = artist;
		this.song = song;
		this.album = album;
		this.image = image;
		this.bitMapImage = null;
	}
	
	public KVCUEntry(KVCUEntry entry) {
		this.time = entry.time;
		this.artist = entry.artist;
		this.song = entry.song;
		this.album = entry.album;
		this.image = entry.image;
		this.bitMapImage = null;
	}
	
	public Bitmap getImage() {
		return bitMapImage;
	}
	
	public void loadImage(ImageAdapter adapter) {
		this.adapter = adapter;
		if (image != null && bitMapImage == null) 
			new RetrieveImageBitmapTask().execute(image);
	}
	
	private class RetrieveImageBitmapTask extends AsyncTask<String, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... url) {
			URL URL;
			try {
				Log.v("similartrack", url[0]);
				URL = new URL(url[0]);
				HttpURLConnection conn = (HttpURLConnection) URL.openConnection();
				conn.setDoInput(true);
				conn.connect();
				return BitmapFactory.decodeStream(conn.getInputStream());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(Bitmap bMap) {
			if (bMap != null) {
				bitMapImage = bMap;
				if (adapter != null) 
					adapter.notifyDataSetChanged();
			}
		}
	}
}