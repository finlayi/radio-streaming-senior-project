/**
 *
 * @file    KVCUConcertCalendar.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */
package org.radio1190.kvcuappfinal;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.xmlpull.v1.XmlPullParserException;

import com.caldroid.CaldroidFragment;
import com.caldroid.CaldroidListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * KVCUConcertCalendar is used to parse the full list of upcoming events on the calendar. It parses the XML that is generated and displays
 * the calendar events to the user using a Caldroid.
 * @author Ryan
 */

public class KVCUConcertCalendar extends FragmentActivity {
	private final String fullCalendarUrl = "http://hireianfinlay.com/radio1190/eventsFullCal.php?id=%argument";
	private final String eventListUrl = "http://www.hireianfinlay.com/radio1190/eventsCal.php?id=%page";
	private final String eventUrlArgument = "yyyy-MM";
	private SimpleDateFormat sdf;
	// Keep track of the current month
	private Calendar currentCalendar;
	private ProgressBar progressBar;
	private ListView listView;
	private ListAdapter listViewAdapter;
	// For Disabling Touches
	private boolean isLoading;
	
	private CaldroidFragment caldroidFragment;
	// Calendar Data Stuff
	HashMap<String, ArrayList<KVCUCalendarEventEntry>> calendarItems;
	
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle saveInstanceState) {
		super.onCreate(saveInstanceState);
		setContentView(R.layout.calendar_layout);
		
		// Progress Bar
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		// List View
		listView = (ListView) findViewById(R.id.eventlists);
		// Initialize Calendar Items
		calendarItems = new HashMap<String, ArrayList<KVCUCalendarEventEntry>>();
		// Date format
		sdf = new SimpleDateFormat(eventUrlArgument, Locale.US);
		Log.v("calendar", sdf.format(new Date()));
		// Keep track of current month
		currentCalendar = Calendar.getInstance();
		// Calendar Fragment
		caldroidFragment = new CaldroidFragment();
		Bundle args = new Bundle();
		args.putInt("month", currentCalendar.get(Calendar.MONTH) + 1);
		args.putInt("year", currentCalendar.get(Calendar.YEAR));
		args.putBoolean("enableSwipe", true);
		args.putCharArray("disabledates", null);
		caldroidFragment.setArguments(args);

		caldroidFragment.setCaldroidListener(new CaldroidListener() {

			@Override
			public void onSelectDate(Date date, View view) {
				if (!isLoading) {
					caldroidFragment.setSelectedDates(date, date);
					// Check if it's within the same month
					int month = currentCalendar.get(Calendar.MONTH);
					Date Date = currentCalendar.getTime();
					currentCalendar.setTime(date);
					if (currentCalendar.get(Calendar.MONTH) == month) {
						String selectedDay = Integer.toString(currentCalendar.get(Calendar.DAY_OF_MONTH));
						ArrayList<KVCUCalendarEventEntry> queryEntries = calendarItems.get(selectedDay);
						ArrayList<HashMap<String, String>> items = new ArrayList<HashMap<String, String>>();
						if (queryEntries != null) {
							for (KVCUCalendarEventEntry entry : queryEntries) {
								HashMap<String, String> content = new HashMap<String, String>();
								content.put("Title", entry.eventTitle);
								content.put("Date", entry.eventDate);
								content.put("Link", entry.eventLink);
								items.add(content);
							}
						}
						listViewAdapter = new SimpleAdapter(KVCUConcertCalendar.this, items, R.layout.calendar_item, new String[] {"Title", "Date", "Link"}, new int[] {R.id.eventtitle, R.id.eventdate, R.id.eventlink});
						listView.setAdapter(listViewAdapter);
						listView.setOnItemClickListener(new OnItemClickListener() {
							public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
								Intent intent = new Intent(getApplicationContext(), KVCUConcertCalendarDetail.class);
								intent.putExtra("eventLink", ((TextView) view.findViewById(R.id.eventlink)).getText().toString());
								KVCUTabLayout.showNotification = false;
								startActivity(intent);
							}	
						});
					}
					caldroidFragment.refreshView();
				}
			}
			
			@Override
			public void onChangeMonth(int month, int year) {
				currentCalendar.set(Calendar.DAY_OF_MONTH, 20); // (Prevents Month Overroll)
				currentCalendar.set(Calendar.YEAR, year);
				currentCalendar.set(Calendar.MONTH, month - 1);
				if (isNetworkAvailable())
					loadCalendarEvents();
			};
			
		});

		FragmentTransaction t = getSupportFragmentManager().beginTransaction();
		t.add(R.id.calendar1, caldroidFragment);
		t.commit();
		
		loadCalendarEvents();
	}
	
	@Override
	public void onStart() {
		super.onStart();
		// Check for Internet Connectivity 
		if (!isNetworkAvailable()) {
			Toast.makeText(this, "CONNECTION NOT FOUND.\nPLEASE ENABLE WIFI OR DATA.", 5000).show();
		}
	}
	
	public void loadCalendarEvents() {
		// Disable Clicking of Calendar
		isLoading = true;
		// Set Progress Bar to Loading
		progressBar.setVisibility(ProgressBar.VISIBLE);
		sdf = new SimpleDateFormat(eventUrlArgument, Locale.US);
		String temp = fullCalendarUrl;
		temp = temp.replaceFirst("%argument", sdf.format(currentCalendar.getTime()));
		new DownloadCalendarEventsTask().execute(temp);
	}
	
	private class DownloadEventXmlTask extends AsyncTask<String, Void, List<KVCUEventEntry>> {
		@Override
		protected List<KVCUEventEntry> doInBackground(String... urls) {
			try {
				return loadEventXmlFromNetwork(urls[0]);
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(final List<KVCUEventEntry> entries) {
			Log.v("eventlist", "In onPostExecute");
			ArrayList<HashMap<String, String>> items = new ArrayList<HashMap<String, String>>();
			if (entries != null) {
				Log.v("eventlist", "Entries isn't null");
				for (KVCUEventEntry entry : entries) {
					HashMap<String, String> content = new HashMap<String, String>();
					content.put("title", entry.title);
					content.put("startDate", entry.startDate);
					content.put("endDate", entry.endDate);
					content.put("content", entry.content);
					content.put("venue", entry.venue);
					content.put("address", entry.phone);
					items.add(content);
				}
				
				/*ImageAdapter adapter = new ImageAdapter(KVCUConcertCalendar.this, items, R.layout.calendar_item, 
						new String[] {"title", "startDate", "endDate", "content", "venue", "address"}, 
						new int[] {R.id.eventtitle, R.id.startDate, R.id.endDate, R.id.content, R.id.venue, R.id.address}, entries);

				listView.setAdapter(adapter);
				// Loads all the images into Bitmaps
				for (KVCUEventEntry entry : entries) {
					entry.loadImage(adapter);
				}
				listView.setOnItemClickListener(new OnItemClickListener() {
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						String title = ((TextView) view.findViewById(R.id.eventtitle)).getText().toString();
						String startDate = ((TextView) view.findViewById(R.id.startDate)).getText().toString();
						String endDate = entries.get(position).endDate;
						String content = entries.get(position).content;
						String venue = entries.get(position).venue;
						String address = entries.get(position).address;
						String imageUrl = entries.get(position).imageUrl;
						Intent in = new Intent(getApplicationContext(), KVCUConcertCalendarDetail.class);
						in.putExtra("title", title);
						in.putExtra("startDate", startDate);
						in.putExtra("endDate", endDate);
						in.putExtra("content", content);
						in.putExtra("venue", venue);
						in.putExtra("address", address);
						in.putExtra("imageUrl", imageUrl);
						KVCUTabLayout.showNotification = false;
						startActivity(in);
					}
				});*/
			}
			progressBar.setVisibility(ProgressBar.INVISIBLE);
		}
	}
	
	
	private class DownloadCalendarEventsTask extends AsyncTask<String, Void, ArrayList<KVCUCalendarEventEntry>> {
		@Override
		protected ArrayList<KVCUCalendarEventEntry> doInBackground(String... urls) {
			try {
				return loadCalendarEventsFromNetwork(urls[0]);
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(ArrayList<KVCUCalendarEventEntry> entries) {
			Log.v("eventlist", "In onPostExecute");
			
			if (entries != null) {
				calendarItems = new HashMap<String, ArrayList<KVCUCalendarEventEntry>>();
				// Calculate the Span of the Month and Initialize the HashMap
				int beginningOfMonth = getBeginningOfCurrentMonth();
				int endOfMonth = getEndOfCurrentMonth();
				for (int i = beginningOfMonth; i <= endOfMonth; i++) {
					calendarItems.put(Integer.toString(i), new ArrayList<KVCUCalendarEventEntry>());
				}
				
				Log.v("calendar", "Entries isn't null");
				// Start adding each event entry into the HashMap
				for (KVCUCalendarEventEntry entry : entries) {
					// Get The ArrayList that Exists and Add the current entry
					ArrayList<KVCUCalendarEventEntry> events = calendarItems.get(entry.eventDay);
					events.add(entry);
					calendarItems.put(entry.eventDay, events);
				}
				// Generate A list of Disabled Dates and Set it to Calendar Fragment
				ArrayList<Date> disabledDates = new ArrayList<Date>();
				Calendar tempCalendar = (Calendar) currentCalendar.clone();
				for (int i = beginningOfMonth; i <= endOfMonth; i++) {
					tempCalendar.set(Calendar.DAY_OF_MONTH, i);
					ArrayList<KVCUCalendarEventEntry> events = calendarItems.get(Integer.toString(i));
					if (events.isEmpty()) {
						disabledDates.add(tempCalendar.getTime());
					}
				}
				caldroidFragment.setDisableDates(disabledDates);
				// Set Minimum and Maximum Dates
				tempCalendar.set(Calendar.DAY_OF_MONTH, beginningOfMonth);
				caldroidFragment.setMinDate(tempCalendar.getTime());
				tempCalendar.set(Calendar.DAY_OF_MONTH, endOfMonth);
				caldroidFragment.setMaxDate(tempCalendar.getTime());
			}
			caldroidFragment.refreshView();
			progressBar.setVisibility(ProgressBar.INVISIBLE);
			isLoading = false;
		}
	}
	
	private List<KVCUEventEntry> loadEventXmlFromNetwork(String urlString) throws XmlPullParserException, IOException {
		KVCUXmlParser parser = new KVCUXmlParser();
		InputStream stream = downloadUrl(urlString);
		if (stream != null) {
			List<KVCUEventEntry> entries = parser.parseEventList(stream);
			stream.close();
			return entries;
		}
		return null;
	}
	
	private ArrayList<KVCUCalendarEventEntry> loadCalendarEventsFromNetwork(String urlString) throws XmlPullParserException, IOException {
		KVCUXmlParser parser = new KVCUXmlParser();
		InputStream stream = downloadUrl(urlString);
		if (stream != null) {
			ArrayList<KVCUCalendarEventEntry> entries = parser.parseCalendarEvents(stream);
			stream.close();
			return entries;
		}
		return null;
	}
	
	private InputStream downloadUrl(String urlString) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		conn.connect();
		if (conn.getResponseCode() == 403)
			return null;
		return conn.getInputStream();
	}
	
	/*public class ImageAdapter extends SimpleAdapter {
		
		private LayoutInflater mInflater;
		private List<KVCUEventEntry> entries;
		
		public ImageAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to, List<KVCUEventEntry> entries) {
			super(context, data, resource, from, to);
			mInflater = LayoutInflater.from(context);
			this.entries = entries;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			KVCUEventEntry entry = entries.get(position);
			if (convertView != null) {
				ImageView eventArt = (ImageView) convertView.findViewById(R.id.eventart);
				if (entry.imageBitmap != null) 
					eventArt.setImageBitmap(entry.imageBitmap);
				return convertView;
			} else {
				convertView = mInflater.inflate(R.layout.calendar_item, null);
				ImageView eventArt = (ImageView) convertView.findViewById(R.id.eventart);
				TextView title = (TextView) convertView.findViewById(R.id.eventtitle);
				TextView startDate = (TextView) convertView.findViewById(R.id.startDate);
				title.setText(entry.title);
				startDate.setText(entry.startDate);
				if (entry.imageBitmap != null) 
					eventArt.setImageBitmap(entry.imageBitmap);
			}
			return convertView;
		}
	}*/
	
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager 
              = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    
    private int getBeginningOfCurrentMonth() {
    	currentCalendar.set(Calendar.DAY_OF_MONTH, currentCalendar.getActualMinimum(Calendar.DAY_OF_MONTH));
    	//setTimeToBeginningOfDay(currentCalendar);
    	Log.v("calendar", "Month Start: " + Integer.toString(currentCalendar.get(Calendar.DAY_OF_MONTH)));
    	return currentCalendar.get(Calendar.DAY_OF_MONTH);
    }
    
    private int getEndOfCurrentMonth() {
    	currentCalendar.set(Calendar.DAY_OF_MONTH, currentCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
    	//setTimeToEndOfDay(currentCalendar);
    	Log.v("calendar", "Month Maximum: " + Integer.toString(currentCalendar.get(Calendar.DAY_OF_MONTH)));
		return currentCalendar.get(Calendar.DAY_OF_MONTH);
    }
}


