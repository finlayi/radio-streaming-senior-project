/**
 *
 * @file    KVCURemoteControlClient.java
 * @project KVCUapp
 *
 * @creator Katie Pham
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */

package org.radio1190.kvcuappfinal;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.RemoteControlClient;
import android.os.Build;
import android.util.Log;


/**
 * KVCURemoteControlClient allows access of the media player's buttons on the lock screen.
 * It also checks the current phone's build version, and it should allow access on API levels
 * 14 and above (Ice Cream Sandwich, Jellybean).
 * @author Katie
 *
 */
public class KVCURemoteControlClient {
    
    private RemoteControlClient RCC;

    @TargetApi(14)
    public void setupControl(Context context) {
        // Check the current API
        // Do nothing if it's below 14 (Gingerbread, Froyo...)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
            return;
        
        Log.v("RCC", "In setup");
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (RCC == null) {
            Log.v("RCC", "Setting up the RCC");
            Intent intent = new Intent(Intent.ACTION_MEDIA_BUTTON);
            intent.setComponent(new ComponentName(context, KVCUNotificationReceiver.class));
            RCC = new RemoteControlClient(PendingIntent.getBroadcast(context, 0, intent, 0));
            audioManager.registerRemoteControlClient(RCC);
        }
        
        // Set the state for the RemoteControlClient
        RCC.setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);
        
        // Set the control flags
        RCC.setTransportControlFlags(RemoteControlClient.FLAG_KEY_MEDIA_PLAY_PAUSE);
    }
    
    @TargetApi(14)
    public void setRCCPlaying() {
        if (RCC == null)
            return;
        Log.v("RCC", "PlaybackState now playing");
        RCC.setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);
    }
    
    @TargetApi(14)
    public void setRCCPaused() {
        if (RCC == null)
            return;
        Log.v("RCC", "PlaybackState now paused");
        RCC.setPlaybackState(RemoteControlClient.PLAYSTATE_PAUSED);
    }
    
    @TargetApi(14)
    public void removeRCC() {
        if (RCC != null)
            RCC.setPlaybackState(RemoteControlClient.PLAYSTATE_STOPPED);
    }
}
