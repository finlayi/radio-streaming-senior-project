/**
 *
 * @file    KVCUBlogEntry.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */
package org.radio1190.kvcuappfinal;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

/**
 * KVCUBlogEntry is used in conjunction with KVCUBlog and is used to parse the blog posts.
 * @author Ryan
 */

public class KVCUBlogEntry {
	public final String title;
	public final String content;
	public final String imageLink;
	public final String blogLink;
	
	public ImageView imageView;
	
	public Bitmap imageBitmap;
	
	KVCUBlogEntry(String Title, String Content, String ImageLink, String BlogLink) {
		this.title = Title;
		this.content = Content;
		this.imageLink = ImageLink;
		this.blogLink = BlogLink;
		
		imageBitmap = null;
	}
	
	public void loadImage() {
		if (imageLink != null && imageBitmap == null) {
			new RetrieveImageBitmapTask().execute(imageLink);
		}
	}
	
	private class RetrieveImageBitmapTask extends AsyncTask<String, Void, Bitmap> {
		@Override
		protected Bitmap doInBackground(String... urls) {
			URL URL;
			try {
				URL = new URL(urls[0]);
				// Scale down the image so there isn't a memory issue
				Bitmap bMap = decodeSampledBitmapFromStream(URL, 256, 128);
				
				if (bMap == null) {
					Log.v("bitmap", "Bitmap is null");
				}
				URL = null;
				return bMap;
			} catch (IOException e) {
				e.printStackTrace();
				URL = null;
			} 
			return null;
		}
		@Override
		protected void onPostExecute(Bitmap bMap) {
			if (bMap != null) {
				imageBitmap = bMap;
				imageView.setImageBitmap(imageBitmap);
			}
		}
		
		public Bitmap decodeSampledBitmapFromStream(URL url, int reqWidth, int reqHeight) throws IOException {
			// Setup connection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.connect();
		    // First decode with inJustDecodeBounds=true to check dimensions
		    final BitmapFactory.Options options = new BitmapFactory.Options();
		    options.inJustDecodeBounds = true;
		    BitmapFactory.decodeStream(conn.getInputStream(), null, options);
		    // Calculate inSampleSize
		    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
		    // Reset the connection to get a new InputStream for retrieving
		    conn.disconnect();
		    conn = (HttpURLConnection) url.openConnection();
		    conn.setDoInput(true);
		    conn.connect();
		    // Decode bitmap with inSampleSize set
		    options.inJustDecodeBounds = false;
		    try {
		    	return BitmapFactory.decodeStream(conn.getInputStream(), null, options);
		    } finally {
		    	conn.disconnect();
		    }
		}
		
		public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		    // Raw height and width of image
		    final int height = options.outHeight;
		    final int width = options.outWidth;
		    int inSampleSize = 1;
	
		    if (height > reqHeight || width > reqWidth) {
	
		        // Calculate ratios of height and width to requested height and width
		        final int heightRatio = Math.round((float) height / (float) reqHeight);
		        final int widthRatio = Math.round((float) width / (float) reqWidth);
		        
		        // Choose the smallest ratio as inSampleSize value, this will guarantee
		        // a final image with both dimensions larger than or equal to the
		        // requested height and width.
		        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		    }
		    return inSampleSize;
		}	
	}
}
