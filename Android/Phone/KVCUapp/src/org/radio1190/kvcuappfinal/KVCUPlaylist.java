/**
 *
 * @file    KVCUPlaylist.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */
package org.radio1190.kvcuappfinal;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.xmlpull.v1.XmlPullParserException;

import com.markupartist.android.widget.PullToRefreshListView;
import com.markupartist.android.widget.PullToRefreshListView.OnRefreshListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;


/**
 * The KVCUPlaylist contains the necessary functions to make a HTTP request 
 * for the KVCUXmlParser. It would later occupy the play list view list.
 * @author Ryan
 *
 */
public class KVCUPlaylist extends Activity {
	public static final String WIFI = "Wi-Fi";
	public static final String ANY = "Any";
	//private static final String playlistLink = "http://csel.cs.colorado.edu/~finlayi/radio1190/index.php?id=%date";
	private static final String playlistLink = "http://www.hireianfinlay.com/radio1190/playlist.php?id=%date";
    // Whether the display should be refreshed.
    public static boolean refreshDisplay = true;
    ProgressBar progressBar = null;
    // The user's current network preference setting.
    public static String sPref = null;
    // Previous and Next Day Buttons
    private Button previousButton;
    private Button nextButton;
    private TextView mDate;
    private Calendar currentCalendar;
    private Date currentDate;
    private Date listDate;
    private DateFormat dateFormat;
    private short deltaDate = 0;
    private short dateLimit = 5;
    // List View
    private PullToRefreshListView listView;
    private boolean isRefreshing = false;
    //private ListView listView;
    private ListAdapter adapter;
    
    
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle saveInstanceState) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
		super.onCreate(saveInstanceState);
		setContentView(R.layout.playlist_layout);
		// Setup Pull to Refresh Widget
		listView = (PullToRefreshListView) findViewById(R.id.PlaylistViewList);
		
		previousButton = (Button) findViewById(R.id.previousday);
		nextButton = (Button) findViewById(R.id.nextday);
		currentCalendar = Calendar.getInstance();
		currentDate = new Date();
		listDate = currentDate;
		
		listView.setOnRefreshListener(new OnRefreshListener() {
			public void onRefresh() {
				// Setup query string
				String playlistUrl = playlistLink;
				String dateString = dateFormat.format(listDate).replaceAll("/", "-");
				playlistUrl = playlistUrl.replaceFirst("%date", dateString);
				isRefreshing = true;
				loadPage(playlistUrl);
			}
		});
		
		if (deltaDate == 0) {
			nextButton.setVisibility(View.INVISIBLE);
			nextButton.setEnabled(false);
		}
		previousButton.setOnClickListener(new OnClickListener( ) {
			public void onClick(View v) {
				Log.v("playlist", "previous");
				if (deltaDate < dateLimit) {
					if (nextButton.getVisibility() == View.INVISIBLE) {
						nextButton.setVisibility(View.VISIBLE);
						nextButton.setEnabled(true);
					}
					currentCalendar.add(Calendar.DATE, -1);
					listDate = currentCalendar.getTime();
					++deltaDate;
					// Setup query string
					String playlistUrl = playlistLink;
					String dateString = dateFormat.format(listDate).replaceAll("/", "-");
					playlistUrl = playlistUrl.replaceFirst("%date", dateString);
					loadPage(playlistUrl);
					listView.setEnabled(false);
					progressBar.setVisibility(ProgressBar.VISIBLE);
					mDate.setText(DateFormat.getDateInstance().format(listDate));
					if (deltaDate == dateLimit){ 
						// Set previous day button to unclickable and invisible
						previousButton.setVisibility(View.INVISIBLE);
						previousButton.setEnabled(false);
					}
				}
			}
		});
		nextButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.v("playlist", "next");
				if (deltaDate > 0) {
					if (previousButton.getVisibility() == View.INVISIBLE) {
						previousButton.setVisibility(View.VISIBLE);
						previousButton.setEnabled(true);
					}
					currentCalendar.add(Calendar.DATE, 1);
					listDate = currentCalendar.getTime();
					--deltaDate;
					// Setup query string
					String playlistUrl = playlistLink;
					String dateString = dateFormat.format(listDate).replaceAll("/", "-");
					playlistUrl = playlistUrl.replaceFirst("%date", dateString);
					loadPage(playlistUrl);
					listView.setEnabled(false);
					progressBar.setVisibility(ProgressBar.VISIBLE);
					mDate.setText(DateFormat.getDateInstance().format(listDate));
					if (deltaDate == 0){
						nextButton.setVisibility(View.INVISIBLE);
						nextButton.setEnabled(false);
					}
				}
			}
		});
		// Progress Bar
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		
		// Set Date
		mDate = (TextView) findViewById(R.id.date);
		String dateString = DateFormat.getDateInstance().format(currentDate);
		mDate.setText(dateString);
		// Set up the initial parsing url
		String playlistUrl = playlistLink;
		dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
		dateString = dateFormat.format(currentDate);
		dateString = dateString.replaceAll("/", "-");
		playlistUrl = playlistUrl.replaceFirst("%date", dateString);
		Log.v("playlist", playlistUrl);
		if (refreshDisplay) {
			loadPage(playlistUrl);
		}
	}
	
	@Override
	public void onStart() {
		super.onStart();
		// Check for Internet Connectivity 
		if (!isNetworkAvailable()) {
			Toast.makeText(this, "CONNECTION NOT FOUND.\nPLEASE ENABLE WIFI OR DATA.", 5000).show();
		}
	}
	
	private void loadPage(String playlistUrl) {
		new DownloadXmlTask().execute(playlistUrl);
	}
	
	private class DownloadXmlTask extends AsyncTask<String, Void, List<KVCUEntry>> {
		@Override
		protected List<KVCUEntry> doInBackground(String... urls) {
			try {
				return loadXmlFromNetwork(urls[0]);
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(List<KVCUEntry> entries) {
			progressBar.setVisibility(ProgressBar.INVISIBLE);
			ArrayList<HashMap<String, String>> items = new ArrayList<HashMap<String, String>>();
			if (entries != null) {
				for (KVCUEntry entry: entries) {
					HashMap<String, String> content = new HashMap<String, String>();
					content.put("artist", entry.artist);
					content.put("song", entry.song);
					content.put("time", entry.time);
					content.put("album", entry.album);
					items.add(content);
				}
			} else {
				progressBar.setVisibility(ProgressBar.INVISIBLE);
			}
			adapter = new SimpleAdapter(KVCUPlaylist.this, items, R.layout.playlist_item, new String[] {"artist", "song", "time", "album"}, new int[] {R.id.artist, R.id.song, R.id.time, R.id.album});
			
			listView.setEnabled(true);
			listView.setAdapter(adapter);
			listView.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					// Set up and start new detailed activity
					String artist = ((TextView) view.findViewById(R.id.artist)).getText().toString();
					String song = ((TextView) view.findViewById(R.id.song)).getText().toString();
					String album = ((TextView) view.findViewById(R.id.album)).getText().toString();
					Intent in = new Intent(getApplicationContext(), KVCUPlaylistDetail.class);
					
					in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					in.putExtra("artist", artist);
					in.putExtra("song", song);
					in.putExtra("album", album);
					
					// Set to not display notification
					KVCUTabLayout.showNotification = false;
					//View intentView = (KVCUPlaylist.this).getLocalActivityManager().startActivity("PlaylistDetail", in).getDecorView();
					//KVCUPlaylist.this.setContentView(intentView);
					startActivity(in);
				}
			});
			
			if (isRefreshing) {
				isRefreshing = false;
				listView.onRefreshComplete();
			}
		}
	}
	
	private List<KVCUEntry> loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException {
		KVCUXmlParser parser = new KVCUXmlParser();
		InputStream stream = downloadUrl(urlString);
		if (stream != null) {
			List<KVCUEntry> entries = parser.parsePlaylist(stream);
			stream.close();
			return entries;
		} 
		return null;
	}
	
	private InputStream downloadUrl(String urlString) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		conn.connect();
		if (conn.getResponseCode() == 403)
			return null;
		return conn.getInputStream();
	}
	
	private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager 
              = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
