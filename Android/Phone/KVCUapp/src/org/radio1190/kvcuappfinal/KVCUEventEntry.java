/**
 *
 * @file    KVCUEventEntry.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */
package org.radio1190.kvcuappfinal;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

//import org.radio1190.kvcuapp.KVCUConcertCalendar.ImageAdapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

/**
 * KVCUEventEntry is used to store the event information when clicked on.
 * @author Ryan
 */

public class KVCUEventEntry {
	public Bitmap imageBitmap;
	//public ImageAdapter adapter;
	
	public final String title;
	public final String content;
	public final String imageUrl;
	public final String startDate;
	public final String endDate;
	public final String venue;
	public final String phone;
	public final String address;
	
	KVCUEventEntry(String Title, String Content, String ImageUrl, String StartDate, String EndDate, String Venue, String Phone, String Address) {
		this.title = Title;
		this.content = Content;
		this.imageUrl =  ImageUrl;
		this.startDate = StartDate;
		this.endDate = EndDate;
		this.venue = Venue;
		this.phone = Phone;
		this.address = Address;
		
		imageBitmap = null;
	}
	
	/*public void loadImage(ImageAdapter adapter) {
		this.adapter = adapter;
		Log.v("eventlist", "imageUrl: " + imageUrl);
		if (imageUrl != null && imageBitmap == null) {
			Log.v("eventlist", "GOT HERE!");
			new RetrieveImageBitmapTask().execute(imageUrl);
		}
	}*/
	
	/*private class RetrieveImageBitmapTask extends AsyncTask<String, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... url) {
			URL URL;
			try {
				Log.v("similartrack", url[0]);
				URL = new URL(url[0]);
				HttpURLConnection conn = (HttpURLConnection) URL.openConnection();
				conn.setDoInput(true);
				conn.connect();
				Log.v("eventlist", "Bitmapping: " + url[0]);
				return BitmapFactory.decodeStream(conn.getInputStream());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(Bitmap bMap) {
			if (bMap != null) {
				imageBitmap = bMap;
				if (adapter != null) 
					adapter.notifyDataSetChanged();
			}
		}
	}*/
}
