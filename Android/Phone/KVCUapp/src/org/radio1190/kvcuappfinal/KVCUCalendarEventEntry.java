package org.radio1190.kvcuappfinal;

/**
 * KVCUCalendarEventEntry is used in conjunction with KVCUConcertCalendar and is used to parse the calendar events.
 * @author Ryan
 */

public class KVCUCalendarEventEntry {
	String eventTitle;
	String eventLink;
	String eventDate;
	String eventDay;
	
	public KVCUCalendarEventEntry(String title, String link, String date, String day) {
		eventTitle = title;
		eventLink = link;
		eventDate = date;
		eventDay = day;
	}
}

