/**
 *
 * @file    KVCUConcertCalendarDetail.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */

package org.radio1190.kvcuappfinal;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * KVCUConcertCalendarDetail is used to display individual concert events when clicked upon from the KVCUConcertCalendar activity. 
 * @author Ryan
 */

public class KVCUConcertCalendarDetail extends Activity {
	
	public String title;
	public String content;
	public String startDate;
	public String endDate;
	public String venue;
	public String imageUrl;
	
	private Button backButton;
	private ProgressBar progressBar;
	private WebView webView;
	
	@Override
	public void onCreate(Bundle saveInstanceState) {
		super.onCreate(saveInstanceState);
		setContentView(R.layout.calendardetail_layout);
		
		backButton = (Button) findViewById(R.id.backButton);
		webView = (WebView) findViewById(R.id.calendarWebView);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished (WebView view, String url) {
				view.loadUrl("javascript:$('#header-container').css('display', 'none');$('.socialbox-container').css('display', 'none');$('.masonry').css('display', 'none');$('.widecolumn').attr('style', 'width: 100% !important; margin: 0 !important; padding: 0 !important');$('.navigation').css('display', 'none');$('img').attr('style', 'width: 100% !important; height: 100% !important');$('.back').css('display', 'none');");
				view.setVisibility(WebView.VISIBLE);
			}
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				view.setVisibility(WebView.INVISIBLE);
			}
		});
		webView.loadUrl(getIntent().getStringExtra("eventLink"));
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		backButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				KVCUConcertCalendarDetail.this.finish();
			}
		});
		
		//loadImage(this.imageUrl);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		// Start Notification Widget
		startService(new Intent(KVCUMediaPlayerService.ACTION_SHOW_NOTIFICATION, null, getApplicationContext(), KVCUMediaPlayerService.class));
	}
	
	/*private void loadImage(String url) {
		if (url != null)
			new LoadEventImageTask().execute(url);
	}*/
	
	/*private class LoadEventImageTask extends AsyncTask<String, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... urls) {
			try {
				Log.v("eventimage", urls[0]);
				InputStream inputStream = downloadUrl(urls[0]);
				if (inputStream != null) {
					Bitmap bitmapImage = BitmapFactory.decodeStream(inputStream);
					inputStream.close();
					return bitmapImage;
				}
				return null;
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(Bitmap bitmapImage) {
			if (bitmapImage != null) {
				ImageView albumImage = (ImageView) KVCUConcertCalendarDetail.this.findViewById(R.id.eventart);
				albumImage.setImageBitmap(bitmapImage);
			}
		}
	}*/
	
	/*private InputStream downloadUrl(String urlString) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		conn.connect();
		return conn.getInputStream();
	}*/
}
