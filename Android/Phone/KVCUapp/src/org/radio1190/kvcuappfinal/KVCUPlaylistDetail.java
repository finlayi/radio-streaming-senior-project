/**
 *
 * @file    KVCUPlaylistDetail.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */

package org.radio1190.kvcuappfinal;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;


/**
 * KVCUPlaylistDetail displays the song information the user has pressed and 
 * extracts similar songs from the Last.fm API.
 * @author Ryan Sheng
 *
 */
public class KVCUPlaylistDetail extends Activity {
	private Button button;
	private ProgressBar progressBar;
	private ProgressBar progressBarAlbumImage;
	private final String similarTrackRoot = "http://ws.audioscrobbler.com/2.0/?method=track.getsimilar&artist=%artist&track=%track&api_key=e4ea5fc7d39e64268e9a37278395d8f9";
	private final String albumImageRoot = "http://ws.audioscrobbler.com/2.0/?method=album.getinfo&api_key=e4ea5fc7d39e64268e9a37278395d8f9&artist=%artist&album=%album";
	private String artist;
	private String song;
	private String album;
	
	// Variables for redirecting the user to the store
    private ImageButton storeButton;
    private String searchCategory = "&c=music";
    private String storeAppLink = "market://search?q=";
    private String storeWebLink = "https://play.google.com/store/search?q=";
    private String searchQuery = "";
    
    // AsyncTask
    ParseSimilarArtistTask parseSimilarArtistTask;
    LoadAlbumImageTask loadAlbumImageTask;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.playlistdetail_layout);
		// Initialize AsyncTasks
		parseSimilarArtistTask = new ParseSimilarArtistTask();
		loadAlbumImageTask = new LoadAlbumImageTask();
		
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		progressBarAlbumImage = (ProgressBar) findViewById(R.id.progressBar2);
		TextView song = (TextView) findViewById(R.id.song);
		TextView artist = (TextView) findViewById(R.id.artist);
		TextView album = (TextView) findViewById(R.id.album);
		Intent in = getIntent();
		this.artist = in.getStringExtra("artist");
		this.song = in.getStringExtra("song");
		this.album = in.getStringExtra("album");
		
		song.setText(this.song);
		artist.setText(this.artist);
		album.setText(this.album);
		
		button = (Button) findViewById(R.id.backbutton);
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Release memory of asyncTask
				parseSimilarArtistTask.cancel(true);
				loadAlbumImageTask.cancel(true);
				parseSimilarArtistTask = null;
				loadAlbumImageTask = null;;
				// Finish this Activity
				finish();
			}
		});
		
		storeButton = (ImageButton) findViewById(R.id.storeButton);
		loadAlbumImage();
		loadSimilarTrack();
	}
	@Override
	public void onStart() {
		super.onStart();
		
		// Button to take the user to the store
        storeButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Start parsing/concatenating the artist and song name into one giant chunk
                // Replace all spaces with a + 
                searchQuery = artist + "+" + song + searchCategory;
                searchQuery = searchQuery.replaceAll(" ", "+");
                Log.v("searchQuery", "searchQuery = " + searchQuery);
                // Will either launch the Google Play Store or the web browser, depending if the app is installed or not
                try {
                    Log.v("searchQuery", "App link: " + storeAppLink + searchQuery);
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(storeAppLink + searchQuery)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    Log.v("searchQuery", "Web link: " + storeWebLink + searchQuery);
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(storeWebLink + searchQuery)));
                }
            }
        });
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		// Start Notification Widget
		startService(new Intent(KVCUMediaPlayerService.ACTION_SHOW_NOTIFICATION, null, getApplicationContext(), KVCUMediaPlayerService.class));
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	/**
	 * Builds necessary URL with request parameters
	 */
	private void loadSimilarTrack() {
		String URL = similarTrackRoot;
		if (URL != null) {
			URL = URL.replaceFirst("%artist", this.artist);
			URL = URL.replaceFirst("%track", this.song);
			URL = URL.replaceAll(" ", "%20");
			parseSimilarArtistTask.execute(URL);
		}
	}
	
	private void loadAlbumImage() {
		String URL = albumImageRoot;
		if (URL != null) {
			URL = URL.replaceFirst("%artist", this.artist);
			URL = URL.replaceFirst("%album", this.album);
			URL = URL.replaceAll(" ", "%20");
			loadAlbumImageTask.execute(URL);
		}
	}
	
	private class ParseSimilarArtistTask extends AsyncTask<String, Void, List<KVCUEntry>> {

		@Override
		protected List<KVCUEntry> doInBackground(String... urls) {
			try {
				Log.v("similartrack", "Doing doInBackground()");
				return loadXmlFromNetwork(urls[0]);
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		/**
		 * Runs through all the entries and set them to the list adapter
		 */
		@Override
		protected void onPostExecute(List<KVCUEntry> entries) {
			if (entries != null) {
				Log.v("similartrack", "Doing onPostExecute()");
				ListView listView = (ListView) findViewById(R.id.similartracklist);
				ArrayList<HashMap<String, String>> items = new ArrayList<HashMap<String, String>>();
				if (entries != null) {
					for (KVCUEntry entry: entries) {
						HashMap<String, String> content = new HashMap<String, String>();
						content.put("artist", entry.artist);
						content.put("song", entry.song);
						content.put("image", entry.image);
						items.add(content);
					}
				}
				ImageAdapter adapter = new ImageAdapter(KVCUPlaylistDetail.this, items, R.layout.playlistdetail_item, new String[] {"artist", "song", "image"}, new int[] {R.id.artist, R.id.song, R.id.albumart}, entries);
				listView.setAdapter(adapter);
				// Loads all the images into Bitmaps
				for (KVCUEntry entry : entries) {
					entry.loadImage(adapter);
				}
			}
			progressBar.setVisibility(ProgressBar.INVISIBLE);
		}
	}
	
	private class LoadAlbumImageTask extends AsyncTask<String, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... urls) {
			try {
				Log.v("albumimage", urls[0]);
				String albumUrl = loadImageXmlFromNetwork(urls[0]);
				InputStream inputStream = downloadUrl(albumUrl);
				if (inputStream != null) {
					Bitmap bitmapImage = BitmapFactory.decodeStream(inputStream);
					inputStream.close();
					return bitmapImage;
				}
				return null;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(Bitmap bitmapImage) {
			if (bitmapImage != null) {
				ImageView albumImage = (ImageView) KVCUPlaylistDetail.this.findViewById(R.id.albumcanvas);
				albumImage.setImageBitmap(bitmapImage);
				Log.v("albumimage", "Got past postExecute()");
			}
			progressBarAlbumImage.setVisibility(ProgressBar.INVISIBLE);
		}
	}
	
	private List<KVCUEntry> loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException {
		Log.v("similartrack", "Doing loadXmlFromNetwork()");
		KVCUXmlParser parser = new KVCUXmlParser();
		Log.v("similartrack", urlString);
		InputStream stream = downloadUrl(urlString);
		if (stream != null) {
			List<KVCUEntry> entries = parser.parseSimilarTracks(stream);
			Log.v("similartrack", "passed parsesimilartracks()");
			stream.close();
			return entries;
		}
		return null;
	}
	
	private String loadImageXmlFromNetwork(String urlString) throws IOException, XmlPullParserException {
		Log.v("albumimage", "Doing loadImageXmlFromNetwork()");
		KVCUXmlParser parser = new KVCUXmlParser();
		InputStream stream = downloadUrl(urlString);
		if (stream != null) {
			String albumURL = parser.parseAlbumImage(stream);
			stream.close();
			return albumURL;
		}
		return null;
	}
	
	private InputStream downloadUrl(String urlString) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		conn.connect();
		return conn.getInputStream();
	}
	
	public class ImageAdapter extends SimpleAdapter {
		
		private LayoutInflater mInflater;
		private List<KVCUEntry> entries;
		
		public ImageAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to, List<KVCUEntry> entries) {
			super(context, data, resource, from, to);
			mInflater = LayoutInflater.from(context);
			this.entries = entries;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			KVCUEntry entry = entries.get(position);
			if (convertView != null) {
				ImageView imageView = (ImageView) convertView.findViewById(R.id.albumart);
				if (entry.getImage() != null) 
					imageView.setImageBitmap(entry.getImage());
				return convertView;
			} else {
				convertView = mInflater.inflate(R.layout.playlistdetail_item, null);
				ImageView imageView = (ImageView) convertView.findViewById(R.id.albumart);
				TextView artist = (TextView) convertView.findViewById(R.id.artist);
				TextView song = (TextView) convertView.findViewById(R.id.song);
				artist.setText(entry.artist);
				song.setText(entry.song);
				if (entry.getImage() != null) 
					imageView.setImageBitmap(entry.getImage());
			}
			convertView.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					String artist = ((TextView) v.findViewById(R.id.artist)).getText().toString();
					String song = ((TextView) v.findViewById(R.id.song)).getText().toString();
					String search_query = artist + "+" + song + searchCategory;
	                search_query = search_query.replaceAll(" ", "+");
	                Log.v("searchQuery", "searchQuery = " + search_query);
	                // Will either launch the Google Play Store or the web browser, depending if the app is installed or not
	                try {
	                    Log.v("searchQuery", "App link: " + storeAppLink + search_query);
	                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(storeAppLink + search_query)));
	                } catch (android.content.ActivityNotFoundException anfe) {
	                    Log.v("searchQuery", "Web link: " + storeWebLink + search_query);
	                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(storeWebLink + search_query)));
	                }
				}
				
			});
			return convertView;
		}
	}
}
