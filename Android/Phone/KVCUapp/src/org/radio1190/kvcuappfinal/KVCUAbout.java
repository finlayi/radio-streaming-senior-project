/**
 *
 * @file    KVCUAbout.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */
package org.radio1190.kvcuappfinal;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;


/**
 * KVCUAbout implements the functionalities of the About view. It contains the necessary information to 
 * display what KVCU 1190 is and implements the donate button, along with the contacts button/activity
 * as well.
 * @author Ryan/Katie
 *
 */
public class KVCUAbout extends Activity {
    private Button donateButton;
    private Intent donatePage;
    private final String donateURL = "https://m.stayclassy.org/checkout/donation?eid=23327";
    
    private Button contactButton;
    
    /**
     * Everything for the About page/activity is generated through onCreate. For this activity specifically, the donate and contact buttons are
     * the only things that are generated. Their respective onClick functions have also been defined within onCreate.
     */
	public void onCreate(Bundle saveInstanceState) {
		super.onCreate(saveInstanceState);
		setContentView(R.layout.about_layout);
		
		// Find donate button and initialize it
		donateButton = (Button) findViewById(R.id.donateButton);
		donateButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Open up the donate page from the site when clicked
                donatePage = new Intent(android.content.Intent.ACTION_VIEW);
                donatePage.setData(Uri.parse(donateURL));
                startActivity(donatePage);
            }
        });
		
		// Find contact button and initialize it
		contactButton = (Button) findViewById(R.id.contactButton);
		contactButton.setOnClickListener(new OnClickListener() {
		    public void onClick(View v) {
		        // Open up a new activity upon click/press--the contacts page itself
		        Intent contactInfo = new Intent(KVCUAbout.this, KVCUContacts.class);
		        KVCUTabLayout.showNotification = false;
		        startActivity(contactInfo);
		    }
		});
	}
	
}
