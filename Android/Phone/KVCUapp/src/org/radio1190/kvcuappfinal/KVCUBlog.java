/**
 *
 * @file    KVCUBlog.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */
package org.radio1190.kvcuappfinal;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * KVCUBlog implements the functionalities of the Blog activity. It parses the XML that is generated and displays the blog posts to the user,
 * along with checking for an internet connection.
 * @author Ryan
 */

public class KVCUBlog extends Activity {
	private final String blogXmlUrl = "http://www.hireianfinlay.com/radio1190/blogXML.php?id=%page";
	private TableLayout blogTable;
	private Button loadMoreButton;
	private ProgressBar buttonProgressBar;
	private View loadMoreView;
	private ListView listView;
	private ProgressBar progressBar;
	private int currentPage = 1;
	private boolean isLoadingNew = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.blog_table_layout);
		listView = (ListView) findViewById(R.id.blogListView);
		loadMoreView = LayoutInflater.from(this).inflate(R.layout.blog_table_load_more, null);
		loadMoreButton = (Button) loadMoreView.findViewById(R.id.loadmorebutton);
		buttonProgressBar = (ProgressBar) loadMoreView.findViewById(R.id.buttonProgressBar);
		blogTable = (TableLayout) findViewById(R.id.blog_table);
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		loadMoreButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				++currentPage;
				isLoadingNew = true;
				buttonProgressBar.setVisibility(ProgressBar.VISIBLE);
				loadMoreButton.setVisibility(Button.INVISIBLE);
				blogTable.setEnabled(false);
				loadPage();
			}
		});
		loadPage();
	}
	
	@Override
	public void onStart() {
		super.onStart();
		// Check for Internet Connectivity 
		if (!isNetworkAvailable()) {
			Toast.makeText(this, "CONNECTION NOT FOUND.\nPLEASE ENABLE WIFI OR DATA.", 5000).show();
		}
	}
	
	private class DownloadBlogXmlTask extends AsyncTask<String, Void, List<KVCUBlogEntry>> {
	
		@Override
		protected List<KVCUBlogEntry> doInBackground(String... urls) {
			try {
				return loadXmlFromNetwork(urls[0]);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(List<KVCUBlogEntry> entries) {
			if (entries != null) {
				//LayoutInflater mInflater = LayoutInflater.from(KVCUBlog.this);
				LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View view;
				if (isLoadingNew) {
					isLoadingNew = false;
					blogTable.removeView(loadMoreView);
					Log.v("blog", "Got here in the if statement");
				}
				for (final KVCUBlogEntry entry : entries) {
					view = mInflater.inflate(R.layout.blog_table_item, null);
					TextView title = (TextView) view.findViewById(R.id.blogTitle);
					TextView content = (TextView) view.findViewById(R.id.blogContent);
					view.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							Intent intent = new Intent(getApplicationContext(), KVCUBlogDetail.class);
							intent.putExtra("blogLink", entry.blogLink);
							// Stop Notification When new Activity started
							KVCUTabLayout.showNotification = false;
							startActivity(intent);
						}
					});
					entry.imageView = (ImageView) view.findViewById(R.id.blogImage);
					title.setText(entry.title);
					content.setText(entry.content);
					blogTable.addView(view);
					entry.loadImage();
				}
				// Add Last Footer View of Load More
				loadMoreButton.setVisibility(Button.VISIBLE);
				buttonProgressBar.setVisibility(ProgressBar.INVISIBLE);
				blogTable.addView(loadMoreView);
				progressBar.setVisibility(ProgressBar.INVISIBLE);
				blogTable.setEnabled(true);
			}
			else {
				blogTable.removeView(loadMoreView);
				blogTable.setEnabled(true);
				isLoadingNew = false;
			}
			progressBar.setVisibility(ProgressBar.INVISIBLE);
		}
	}
	
	private void loadPage() {
		String blogUrl = blogXmlUrl;
		blogUrl = blogUrl.replaceFirst("%page", String.valueOf(currentPage));
		Log.v("blog", blogUrl);
		new DownloadBlogXmlTask().execute(blogUrl);
	}
	
	private List<KVCUBlogEntry> loadXmlFromNetwork(String urlString) throws IOException, XmlPullParserException {
		KVCUXmlParser parser = new KVCUXmlParser();
		InputStream stream = downloadUrl(urlString);
		if (stream != null) {
			List<KVCUBlogEntry> entries = parser.parseBlog(stream);
			stream.close();
			return entries;
		}
		return null;
	}
	
	private InputStream downloadUrl(String urlString) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		conn.connect();
		if (conn.getResponseCode() == HttpURLConnection.HTTP_INTERNAL_ERROR) {
			return null;
		}
		return conn.getInputStream();
	}
	
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager 
              = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
