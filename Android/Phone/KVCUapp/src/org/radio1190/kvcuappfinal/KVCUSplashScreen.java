/**
 *
 * @file    KVCUSplashScreen.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */

package org.radio1190.kvcuappfinal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;


/**
 * KVCUSplashScreen loads an activity with Radio 1190's main logo before preceding onto the main application.
 * @author Ryan
 *
 */
public class KVCUSplashScreen extends Activity {
	Handler mHandler;
	Runnable mRunnable;
	@Override
	public void onCreate(Bundle saveInstanceState) { 
		super.onCreate(saveInstanceState);
		setContentView(R.layout.splash_layout);
		// Turn Screen Timeout Off. Keeps Application looking Good
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		mHandler = new Handler();
		mRunnable = new Runnable() {
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(KVCUSplashScreen.this, KVCUTabLayout.class);
                KVCUSplashScreen.this.startActivity(mainIntent);
                KVCUSplashScreen.this.finish();
            }
		};
		mHandler.postDelayed(mRunnable, 2000);
	}
	@Override
	public void onDestroy() {
		super.onDestroy();
		mHandler.removeCallbacks(mRunnable);
		// Turn Screen Timeout On when Done with Activity
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
}
