/**
 *
 * @file    KVCUMediaPlayerService.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */

package org.radio1190.kvcuappfinal;

import java.io.IOException;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

/**
 * KVCUMediaPlayerService is used in conjunction with KVCUMediaPlayer as a service.
 * @author Ryan/Katie
 *
 */

public class KVCUMediaPlayerService extends Service {
	private MediaPlayer mp;
	
	public static boolean isPlaying = true;
	private boolean wasPlaying;
	private final String musicURL = "http://radio1190.colorado.edu:8000/high.mp3";
	
	public static final String ACTION_PLAY = "org.radio1190.kvcuapp.action.PLAY";
	public static final String ACTION_PAUSE = "org.radio1190.kvcuapp.action.PAUSE";
	public static final String ACTION_PLAY_PAUSE = "org.radio1190.kvcuapp.action.PLAY_PAUSE";
	public static final String ACTION_SHOW_NOTIFICATION = "org.radio1190.kvcuapp.action.SHOW_NOTIFICATION";
	public static final String ACTION_DELETE_NOTIFICATION = "org.radio1190.kvcuapp.action.DELETE_NOTIFICATION";
	
	// For RemoteControlClient
    KVCURemoteControlClient KVCURCC;
	
	// Call Listener
	PhoneStateListener phoneStateListener;
	// Notification Things
	Intent intent;
	Intent intentPlay;
	private int notificationID = 0;
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private final OnAudioFocusChangeListener audioFocusCL = new OnAudioFocusChangeListener() {
        public void onAudioFocusChange(int focusChange) {
            Log.v("onAudioFocusChange", "Changing audio focus");
            // Temporary loss
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
                // Pause playback
                if (mp.isPlaying()) {
                    Log.v("onAudioFocusChange", "loss_transient, pausing playback");
                    processPause();
                }
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                // Resume playback
                if (mp != null && !mp.isPlaying()) {
                    Log.v("onAudioFocusChange", "gain, resuming playback");
                    processPlay();
                }
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                // Stop playback completely
                Log.v("onAudioFocusChange", "loss, stopping playback");
                if (mp.isPlaying()) {
                    processPause();
                }
                mp.release();
                mp = null;
            }
        }
    };
	
	
	@Override
	public void onCreate() {
		initializeMP();
		initializeNotificationIntents();
		initializeCallListener();
	}

	@Override
	public void onDestroy() {
		deleteNotification();
		mp.stop();
		mp.reset();
		mp.release();
		mp = null;
		// Destroy Call Listener
		TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		if(mgr != null) {
		    mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
		}
		
		// Give up audio focus
		AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		audioManager.abandonAudioFocus(audioFocusCL);
		// Fixes Previous Service State with isPlaying
		isPlaying = true;
		// Remove lock screen controls here? Might or might not be necessary but leave it in for now
		KVCURCC.removeRCC();
	}
	
	@Override
	public void onStart(Intent intent, int startid) {
		String action = intent.getAction();
		if (action.equals(ACTION_PLAY))
			processPlay();
		else if (action.equals(ACTION_PAUSE))
			processPause();
		else if (action.equals(ACTION_PLAY_PAUSE))
			processWidgetButtonRequest();
		else if (action.equals(ACTION_SHOW_NOTIFICATION))
			showNotification();
		else if (action.equals(ACTION_DELETE_NOTIFICATION))
			deleteNotification();	
	}
	
	void processWidgetButtonRequest() {
		if (mp.isPlaying()) {
			processPause();
		}
		else {
			processPlay();
		}
		updateNotification();
	}
	
	void processPause() {
		if (!mp.isPlaying()) {
			return;
		}
		mp.pause();
		
		//KVCURCC.setRCCPaused();
		
		isPlaying = mp.isPlaying();
		wasPlaying = true;
	}
	void processPlay() {
		if (mp.isPlaying()) {
			return;
		}
    	mp.reset();
    	mp.release();
    	initializeMP();
    	
    	//getAudioFocus();
        //KVCURCC.setRCCPlaying();
    	
		isPlaying = mp.isPlaying();
	}
	
	public void getAudioFocus() {
	    AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int result = audioManager.requestAudioFocus(audioFocusCL, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        Log.v("audioFocus", "Requesting audio focus...");
        // Check if request is okay
        //if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) { 
        if (result == AudioManager.AUDIOFOCUS_REQUEST_FAILED) {
            Log.v("audioFocus", "Focus request failed");
        }
        
        Log.v("getAudioFocus", "Outside of the check");
        ComponentName eventReceiver = new ComponentName(this, KVCUNotificationReceiver.class);
        
        audioManager.registerMediaButtonEventReceiver(eventReceiver);
    }
	
	void showNotification() {
		if (mp.isPlaying()) {
			PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			
			PendingIntent pIntentPlay = PendingIntent.getService(this, 0, intentPlay, PendingIntent.FLAG_CANCEL_CURRENT);
			
			Notification notification = new NotificationCompat.Builder(this)
				.setContentTitle("Radio 1190 KVCU")
				.setContentText("Now Streaming...")
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentIntent(pIntent)
				.addAction(R.drawable.pause_button, "Pause", pIntentPlay)
				.setAutoCancel(true)
				.getNotification();
			notification.flags |= Notification.FLAG_ONGOING_EVENT;
			
			NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			notificationManager.notify(notificationID, notification);
		}
	}
	void deleteNotification() {
		// Get rid of previous notification widget
		NotificationManager notificationManager = (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
		notificationManager.cancelAll();
	}
	void updateNotification() {
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		PendingIntent pIntentPlay = PendingIntent.getService(this, 0, intentPlay, PendingIntent.FLAG_CANCEL_CURRENT);
		Notification notification;
		if (mp.isPlaying()) {
			notification = new NotificationCompat.Builder(this)
			.setContentTitle("Radio 1190 KVCU")
			.setContentText("Now Streaming...")
			.setSmallIcon(R.drawable.ic_launcher)
			.setContentIntent(pIntent)
			.addAction(R.drawable.pause_button, "Pause", pIntentPlay)
			.setAutoCancel(true)
			.getNotification();
		notification.flags |= Notification.FLAG_ONGOING_EVENT;
		}
		else {
			notification = new NotificationCompat.Builder(this)
				.setContentTitle("Radio 1190 KVCU")
				.setContentText("Now Streaming...")
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentIntent(pIntent)
				.addAction(R.drawable.play_button, "Play", pIntentPlay)
				.setAutoCancel(true)
				.getNotification();
			notification.flags |= Notification.FLAG_ONGOING_EVENT;
		}
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(notificationID, notification);
	}
	
	private void initializeNotificationIntents() {
		intent = new Intent(this, KVCUTabLayout.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		
		intentPlay = new Intent(KVCUMediaPlayerService.ACTION_PLAY_PAUSE, null, getApplicationContext(), this.getClass());
		intentPlay.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	}
	
    // Initializes the MediaPlayer
    private void initializeMP() {
        if (KVCURCC == null)
            KVCURCC = new KVCURemoteControlClient();
        //KVCURCC.setupControl(this);
        
        // Create a new KVCUMediaPlayer
        mp = new MediaPlayer();
        // Set to Stream
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        // Enable volume control using STREAM_MUSIC type for the Application
        mp.setOnPreparedListener(new OnPreparedListener() {
			public void onPrepared(MediaPlayer mp) {
				mp.start();
				isPlaying = mp.isPlaying();
				KVCUMediaPlayer.startButton.setEnabled(true);
				KVCUMediaPlayer.firstPlay = false;
				if (!KVCUTabLayout.showNotification)
					updateNotification();
			}
        });
        try {
            mp.setDataSource(musicURL);
        } catch (IllegalArgumentException e1) {
            e1.printStackTrace();
        } catch (SecurityException e1) {
            e1.printStackTrace();
        } catch (IllegalStateException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
		mp.prepareAsync();
        isPlaying = true;
    }
    
    // Initialize Phone Call Listener
    private void initializeCallListener() {
    	phoneStateListener = new PhoneStateListener() {
    	    @Override
    	    public void onCallStateChanged(int state, String incomingNumber) {
    	        if (state == TelephonyManager.CALL_STATE_RINGING) {
    	            if (mp.isPlaying()) {
    	            	processPause();
    	            	wasPlaying = true;
    	            }
    	        } else if(state == TelephonyManager.CALL_STATE_IDLE) {
    	            if (wasPlaying)
    	            	processPlay();
    	        } else if(state == TelephonyManager.CALL_STATE_OFFHOOK) {
    	            if (mp.isPlaying()) {
    	            	processPause();
    	            	wasPlaying = true;
    	        	}
    	        }
    	        super.onCallStateChanged(state, incomingNumber);
    	    }
    	};
    	TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
    	if(mgr != null) {
    	    mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
    	}
    }
}
