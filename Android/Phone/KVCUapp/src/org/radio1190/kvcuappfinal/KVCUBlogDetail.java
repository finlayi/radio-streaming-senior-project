/**
 *
 * @file    KVCUBlogDetail.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */
package org.radio1190.kvcuappfinal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * KVCUBlogDetail is used to display individual blog posts when clicked upon from the KVCUBlog activity. 
 * @author Ryan
 */

public class KVCUBlogDetail extends Activity {
	private Button backButton;
	private ProgressBar progressBar;
	private WebView webView;
	// Handler to run a progress runnable
	private Handler mHandler = new Handler();
	@Override
	public void onCreate(Bundle saveInstanceState) {
		super.onCreate(saveInstanceState);
		setContentView(R.layout.blog_detail);
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		webView = (WebView) findViewById(R.id.blogWebView);
		// Set Progress Updates
		progressBar.setIndeterminate(false);

		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished (WebView view, String url) {
				view.loadUrl("javascript:$('#header-container').css('display', 'none');$('.socialbox-container').css('display', 'none');$('.masonry').css('display', 'none');$('.post-container').css('display', 'none');$('.single-image img').attr('style', 'height:100% !important; width:100% !important; margin:0px 0px 0px 0px');");
				view.setVisibility(WebView.VISIBLE);
			}
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				view.setVisibility(WebView.INVISIBLE);
			}
		});
		webView.loadUrl(getIntent().getStringExtra("blogLink"));
		
		backButton = (Button) findViewById(R.id.backButton);
		backButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				KVCUBlogDetail.this.finish();
			}
		});
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		// Start Notification Widget
		startService(new Intent(KVCUMediaPlayerService.ACTION_SHOW_NOTIFICATION, null, getApplicationContext(), KVCUMediaPlayerService.class));
	}
	
	public void setProgressUpdates() {
		/*new Thread(new Runnable() {
			public void run() {
				int progress = webView.getProgress();;
				while (progress < 100) {
					progress = webView.getProgress();
					mHandler.post(new Runnable() {
						public void run() {
							progressBar.setProgress(progress);
						}
					});
				}
			}
		}).start();*/
	}
}
