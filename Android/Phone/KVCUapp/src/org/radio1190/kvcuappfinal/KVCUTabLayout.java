/**
 *
 * @file    KVCUTabLayout.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */
package org.radio1190.kvcuappfinal;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TabActivity;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.*;


/**
 * KVCUTabLayout implements the general tabs view that would invoke KVCUPlaylist, KVCUMediaPlayer,
 * KVCUConcertCalendar, and KVCUAbout activities. 
 * @author Ryan/Katie
 *
 */
@SuppressWarnings("deprecation")
public class KVCUTabLayout extends TabActivity {
	// Show Notification Based on Whether an Internal Activity is Created. True when back is pressed. False when Users opens up a new activity
    public static boolean showNotification = true;
	TabHost mTabHost;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_tab);
		mTabHost = getTabHost();
		
		//MediaPlayer
		TabSpec mediaplayerSpec = mTabHost.newTabSpec("MediaPlayer");		
		View tabIndicatorMP = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
		((TextView) tabIndicatorMP.findViewById(R.id.title)).setText("Player");
		((ImageView) tabIndicatorMP.findViewById(R.id.icon)).setImageResource(R.drawable.radio_tab_bar_selected);
		Intent mediaplayerIntent = new Intent(this, KVCUMediaPlayer.class);
        mediaplayerSpec.setContent(mediaplayerIntent);
		mediaplayerSpec.setIndicator(tabIndicatorMP);
		
		//Playlist
		TabSpec playlistSpec = mTabHost.newTabSpec("Playlist");
		View tabIndicatorPlaylist = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
		((TextView) tabIndicatorPlaylist.findViewById(R.id.title)).setText("Playlist");
        ((ImageView) tabIndicatorPlaylist.findViewById(R.id.icon)).setImageResource(R.drawable.playlist_tab_bar_unselected);
        Intent playlistIntent = new Intent(this, KVCUPlaylist.class);
        playlistSpec.setContent(playlistIntent);
        playlistSpec.setIndicator(tabIndicatorPlaylist);

		//Concert Calendar
		TabSpec calendarSpec = mTabHost.newTabSpec("Calendar");
		View tabIndicatorCalendar = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
        ((TextView) tabIndicatorCalendar.findViewById(R.id.title)).setText("Calendar");
        ((ImageView) tabIndicatorCalendar.findViewById(R.id.icon)).setImageResource(R.drawable.cal_tab_bar_unselected);
		Intent calendarIntent = new Intent(this, KVCUConcertCalendar.class);
		calendarSpec.setContent(calendarIntent);
		calendarSpec.setIndicator(tabIndicatorCalendar);
		
		//Blog
        TabSpec blogSpec = mTabHost.newTabSpec("Blog");
        View tabIndicatorBlog = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
        ((TextView) tabIndicatorBlog.findViewById(R.id.title)).setText("Blog");
        ((ImageView) tabIndicatorBlog.findViewById(R.id.icon)).setImageResource(R.drawable.blog_tab_bar_unselected);
     
        Intent blogIntent = new Intent(this, KVCUBlog.class);
        blogSpec.setContent(blogIntent);
        blogSpec.setIndicator(tabIndicatorBlog);
		
		//About Page
		TabSpec aboutSpec = mTabHost.newTabSpec("About");
		View tabIndicatorAbout = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
        ((TextView) tabIndicatorAbout.findViewById(R.id.title)).setText("About");
        ((ImageView) tabIndicatorAbout.findViewById(R.id.icon)).setImageResource(R.drawable.about_tab_bar_unselected);
		Intent aboutIntent = new Intent(this, KVCUAbout.class);
		aboutSpec.setContent(aboutIntent);
		aboutSpec.setIndicator(tabIndicatorAbout);
		
		//Adding all TabSpecs to TabHost
		mTabHost.addTab(mediaplayerSpec);
		mTabHost.addTab(playlistSpec);
		mTabHost.addTab(calendarSpec);
		mTabHost.addTab(blogSpec);
		mTabHost.addTab(aboutSpec);
		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {
    		public void onTabChanged(String tabId) {
    			ImageView mediaplayerView = (ImageView) getTabWidget().getChildAt(0).findViewById(R.id.icon);
    			ImageView playlistView = (ImageView) getTabWidget().getChildAt(1).findViewById(R.id.icon);	
    			ImageView calendarView = (ImageView) getTabWidget().getChildAt(2).findViewById(R.id.icon);
    			ImageView blogView = (ImageView) getTabWidget().getChildAt(3).findViewById(R.id.icon);
    			ImageView aboutView = (ImageView) getTabWidget().getChildAt(4).findViewById(R.id.icon);
    			if (tabId.equals("MediaPlayer")) {
    				Log.v("tabview", "MediaPlayer");
    				mediaplayerView.setImageResource(R.drawable.radio_tab_bar_selected);
    				playlistView.setImageResource(R.drawable.playlist_tab_bar_unselected);
    				calendarView.setImageResource(R.drawable.cal_tab_bar_unselected);
    				blogView.setImageResource(R.drawable.blog_tab_bar_unselected);
    				aboutView.setImageResource(R.drawable.about_tab_bar_unselected);
    			} else if (tabId.equals("Playlist")) {
    				Log.v("tabview", "Playlist");
    				mediaplayerView.setImageResource(R.drawable.radio_tab_bar_unselected);
    				playlistView.setImageResource(R.drawable.playlist_tab_bar_selected);
    				calendarView.setImageResource(R.drawable.cal_tab_bar_unselected);
    				blogView.setImageResource(R.drawable.blog_tab_bar_unselected);
    				aboutView.setImageResource(R.drawable.about_tab_bar_unselected);

    			} else if (tabId.equals("Calendar")) {
    				Log.v("tabview", "Calendar");
    				mediaplayerView.setImageResource(R.drawable.radio_tab_bar_unselected);
    				playlistView.setImageResource(R.drawable.playlist_tab_bar_unselected);
    				calendarView.setImageResource(R.drawable.cal_tab_bar_selected);
    				blogView.setImageResource(R.drawable.blog_tab_bar_unselected);
    				aboutView.setImageResource(R.drawable.about_tab_bar_unselected);

    			} else if (tabId.equals("Blog")) {
    				Log.v("tabview", "Blog");
    				mediaplayerView.setImageResource(R.drawable.radio_tab_bar_unselected);
    				playlistView.setImageResource(R.drawable.playlist_tab_bar_unselected);
    				calendarView.setImageResource(R.drawable.cal_tab_bar_unselected);
    				blogView.setImageResource(R.drawable.blog_tab_bar_selected);
    				aboutView.setImageResource(R.drawable.about_tab_bar_unselected);

    			} else if (tabId.equals("About")) {
    				Log.v("tabview", "About");
    				mediaplayerView.setImageResource(R.drawable.radio_tab_bar_unselected);
    				playlistView.setImageResource(R.drawable.playlist_tab_bar_unselected);
    				calendarView.setImageResource(R.drawable.cal_tab_bar_unselected);
    				blogView.setImageResource(R.drawable.blog_tab_bar_unselected);
    				aboutView.setImageResource(R.drawable.about_tab_bar_selected);
    			}
    		}
        });

	}
	
	@Override
	public void onStart() {
		super.onStart();
		stopNotification();
		showNotification = true;
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if (showNotification) 
			startService(new Intent(KVCUMediaPlayerService.ACTION_SHOW_NOTIFICATION, null, getApplicationContext(), KVCUMediaPlayerService.class));
		showNotification = false;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		stopService(new Intent(this, KVCUMediaPlayerService.class));
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			showNotification = false;
			finish();
		    return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private void stopNotification() {
		// Get rid of previous notification widget
		NotificationManager notificationManager = (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
		notificationManager.cancelAll();
	}

}
