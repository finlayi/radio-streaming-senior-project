/**
 *
 * @file    KVCUMediaPlayer.java
 * @project KVCUapp
 *
 * @creator Ryan Sheng
 * Copyright (c) 2012 CU Senior Projects. All rights reserved.
 * 
 */
package org.radio1190.kvcuappfinal;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.*;
import android.view.View.OnClickListener;


/**
 * KVCUMediaPlayer implements the necessary connection to the media stream to play songs, 
 * volume controls and parsing to display the current song.
 * @author Ryan/Katie
 *
 */
public class KVCUMediaPlayer extends Activity {
	private MediaPlayer mp;
	private AudioManager audioManager;
	public static Button startButton;
	private Button muteButton;
	private SeekBar volumeControlButton;
	private TextView songView;
	private TextView artistView;
	private ProgressBar progressBar;
	
	public static boolean isPlaying = true;
	private boolean isNotMute = true;
	private boolean isUpdating = true;
	
	private final String musicURL = "http://radio1190.colorado.edu:8000/high.mp3";
	private final String currentSongUrl = "http://radio1190.colorado.edu/playlist/current/";
	private final String trackInfoUrl = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=e4ea5fc7d39e64268e9a37278395d8f9&artist=%artist&track=%song";
	
	private Handler mHandler;
	private Runnable updateRunnable;
    private int INTERVAL = 7000; // 7 Seconds
    
    // First Player (Stop Users from Messing up the prepareAsync from the first click of the playbutton)
    public static boolean firstPlay = true;
    
    // MediaPlayer Service
    KVCUMediaPlayerService mpService;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mediaplayer_layout);
        audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        initializeUI();
        // Start mediaplayer Service
        initializeMPService();
        //initializeMP();
        initializeSongUpdate();
        
        //TESTING CALENDAR REGULAR EXPRESSIONS
        /*String inputString[] = {"event-0", "event-1", "event-h", "hihihi-h", "event-10", "event-11-date", "event-20-link"};
        Pattern p = Pattern.compile("event-(\\d+)(\\z)");
        for (String item : inputString) {
        	Matcher m = p.matcher(item);
        	if (m.find()) {
	        	String stuff = m.group();
	        	Log.v("calendar", stuff);
        	}
        	//String stuff = m.group(1);
        	//if (stuff != null)
        	//Log.v("calendar", stuff);
        }*/
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	// Fix Layout based on the Media Player Service
    	this.isPlaying = KVCUMediaPlayerService.isPlaying;
    	if (this.isPlaying) {
    		startButton.setBackgroundResource(R.drawable.pause_button);
    	} else {
    		startButton.setBackgroundResource(R.drawable.play_button);
    	}
    	
		// Check for Internet Connectivity 
		if (!isNetworkAvailable()) {
			Toast.makeText(this, "CONNECTION NOT FOUND.\nPLEASE ENABLE WIFI OR DATA.", 5000).show();
			isPlaying = false;
	    	startButton.setBackgroundResource(R.drawable.play_button);
		}
    }
    
    @Override
    public void onDestroy()
    {
    	super.onDestroy();
    	isUpdating = false;
    	stopService(new Intent(this, KVCUMediaPlayerService.class));
    }
    
    // Initializes buttons for the MediaPlayer
    public void initializeUI() {
        startButton = (Button) findViewById(R.id.startButton);
        muteButton = (Button) findViewById(R.id.mute);
		songView = (TextView) findViewById(R.id.song);
		artistView = (TextView) findViewById(R.id.artist);
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		progressBar.setVisibility(ProgressBar.INVISIBLE);
        // Find buttons and initialize them
		//startButton.setEnabled(false); // Prevents user from messing up async call between the music player and the look of the button
        startButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Change the text and the playing status
            	if (!firstPlay) {
	                if (!isPlaying) {
	                	if (isNetworkAvailable()) {
		                	Toast.makeText(getApplicationContext(), "Buffering Radio Station...", 1000).show();
		                	startButton.setEnabled(false);
		                	startButton.setBackgroundResource(R.drawable.pause_button);
		                	isPlaying = true;
		                	startService(new Intent(KVCUMediaPlayerService.ACTION_PLAY, null, getApplicationContext(), KVCUMediaPlayerService.class));
	                	}
	                } else {
	                	startButton.setBackgroundResource(R.drawable.play_button);
	                    isPlaying = false;
	                	startService(new Intent(KVCUMediaPlayerService.ACTION_PAUSE, null, getApplicationContext(), KVCUMediaPlayerService.class));
	                }
            	}
            }
        });
        //Initialize the volume bar
        volumeControlButton = (SeekBar)findViewById(R.id.volumebar);
        volumeControlButton.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        volumeControlButton.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        volumeControlButton.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (isNotMute == false) {
					audioManager.setStreamMute(AudioManager.STREAM_MUSIC, isNotMute);
					muteButton.setBackgroundResource(R.drawable.muted_icon);
					isNotMute = true;
				}
				Log.v("Verbose", "The current volume after sliding is " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
				audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
			}

			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
        //Initialize the mute button
        muteButton.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		if (isNotMute) {
        			audioManager.setStreamMute(AudioManager.STREAM_MUSIC, isNotMute);
        			volumeControlButton.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        			isNotMute = false;
        			v.setBackgroundResource(R.drawable.muted_red_icon);
        		} else {
        			audioManager.setStreamMute(AudioManager.STREAM_MUSIC, isNotMute);
        			volumeControlButton.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        			Log.v("Verbose", "The current volume after unmuting is " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        			isNotMute = true;
        			v.setBackgroundResource(R.drawable.muted_icon);
        		}
        	}
        });
    }
        
    private void initializeMPService() {
    	// Start Music Service Automatically
    	startService(new Intent(KVCUMediaPlayerService.ACTION_PLAY, null, this, KVCUMediaPlayerService.class));
    	// Enable volume control using STREAM_MUSIC type for the Application
    	setVolumeControlStream(AudioManager.STREAM_MUSIC); 
    	// Display the Pause Button
    	startButton.setBackgroundResource(R.drawable.pause_button);
    }
    
    // Overrides the volume buttons on the side of the phone to adjust the volume
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
	        case KeyEvent.KEYCODE_VOLUME_UP:
	        	if (isNotMute == false) {
	        		audioManager.setStreamMute(AudioManager.STREAM_MUSIC, isNotMute);
	        		muteButton.setBackgroundResource(R.drawable.muted_icon);
	        		isNotMute = true;
	        	}
	            audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, 0);
	            volumeControlButton.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
	            return true;
	        case KeyEvent.KEYCODE_VOLUME_DOWN:
	            audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, 0);
	            volumeControlButton.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
	            return true;
	        default:
	            return false;
        }
    }
    
    public void loadPage() {
    	new DownloadXmlTask().execute(currentSongUrl);
    }
    
    public void loadAlbumImage(String artist, String song) {
    	String URL = trackInfoUrl;
		if (URL != null) {
			URL = URL.replaceFirst("%artist", artist);
			URL = URL.replaceFirst("%song", song);
			URL = URL.replaceAll(" ", "%20");
			Log.v("mediaplayer", URL);
			new DownloadAlbumImageTask().execute(URL);
		}
    }
    
    private class DownloadXmlTask extends AsyncTask<String, Void, KVCUEntry> {
		@Override
		protected KVCUEntry doInBackground(String... urls) {
			try {
				return loadXmlFromNetwork(urls[0]);
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(KVCUEntry entry) {
			@SuppressWarnings("unused")
			ListView listView = (ListView) findViewById(R.id.PlaylistViewList);
			if (entry != null && entry.artist != null && entry.song != null) {
				artistView.setText(entry.artist);
				songView.setText(entry.song);
				//loadAlbumImage(entry.artist, entry.song);
			} else {
				// Set Default Values
				artistView.setText("www.radio1190.org");
				songView.setText("Radio 1190 KVCU");
			}
		}
	}
    
	private class DownloadAlbumImageTask extends AsyncTask<String, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... urls) {
			try {
				Log.v("albumimage", urls[0]);
				String albumUrl = loadImageXmlFromNetwork(urls[0]);
				InputStream inputStream = downloadUrl(albumUrl);
				if (inputStream != null) {
					Bitmap bitmapImage = BitmapFactory.decodeStream(inputStream);
					inputStream.close();
					return bitmapImage;
				}
				return null;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(Bitmap bitmapImage) {
			if (bitmapImage != null) {
				ImageView albumImage = (ImageView) KVCUMediaPlayer.this.findViewById(R.id.albumart);
				albumImage.setImageBitmap(bitmapImage);
				Log.v("albumimage", "Got past postExecute()");
			}
		}
	}
    
	private KVCUEntry loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException {
		KVCUXmlParser parser = new KVCUXmlParser();
		InputStream stream = downloadUrl(urlString);
		KVCUEntry entry = parser.parseCurrentSong(stream);
		stream.close();
		return entry;
	}
	
	private String loadImageXmlFromNetwork(String urlString) throws XmlPullParserException, IOException {
		KVCUXmlParser parser = new KVCUXmlParser();
		InputStream stream = downloadUrl(urlString);
		if (stream != null) {
			String albumImageUrl = parser.parseAlbumImage(stream);
			stream.close();
			return albumImageUrl;
		}
		return null;
	}
	
	private InputStream downloadUrl(String urlString) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		//Starts the query
		conn.connect();
		InputStream stream = conn.getInputStream();
		return stream;
	}
	
	private void initializeSongUpdate() {
		mHandler = new Handler();
		updateRunnable = new Runnable() {
			public void run() {
				if (isUpdating) {
					loadPage();
					mHandler.postDelayed(updateRunnable, INTERVAL);
					Log.v("update", "Running artist and song update");
				}
			}
		};
		updateRunnable.run();
	}
    
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager 
              = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}