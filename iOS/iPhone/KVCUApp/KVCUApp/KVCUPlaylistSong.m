//
//  KVCUPlaylistSong.m
//  KVCUApp
//
//  Created by Ian Finlay on 12/5/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import "KVCUPlaylistSong.h"

@implementation KVCUPlaylistSong

@synthesize artist;
@synthesize time;
@synthesize song;
@synthesize album;

- (id)init
{
    self = [super init];
    return self;
}

@end
