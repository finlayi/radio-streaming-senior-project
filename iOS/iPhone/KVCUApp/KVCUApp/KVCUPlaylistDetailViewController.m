//
//  KVCUPlaylistDetailViewController.m
//  KVCUApp
//
//  Created by Ian Finlay on 12/6/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import "KVCUPlaylistDetailViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface KVCUPlaylistDetailViewController ()

@end

@implementation KVCUPlaylistDetailViewController

@synthesize artistLabel;
@synthesize songLabel;
@synthesize albumLabel;
@synthesize artistName;
@synthesize songName;
@synthesize albumName;
@synthesize albumArt;
@synthesize detailTableView;
@synthesize loadingTable;
@synthesize similarSongsLabel;
@synthesize connectionData;
@synthesize connection;
@synthesize staticAlbumLabel;
@synthesize staticArtistLabel;
@synthesize staticSongLabel;
@synthesize whiteBackground;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

//  Use data that segue passed to set defaults
- (void)viewDidLoad
{
    artistLabel.text = artistName;
    songLabel.text = songName;
    albumLabel.text = albumName;

    detailTableView.artist = artistName;
    detailTableView.song = songName;
    detailTableView.album = albumName;
    self.title = songName;
    self.buyButtonLink = nil;
    self.connectionData = nil;
    [self.detailTableView setHidden:YES];
    
    [self.albumArt.layer setBorderColor: [[UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f] CGColor]];
    [self.albumArt.layer setBorderWidth:4.0];
    [self.albumArt.layer setCornerRadius:4.0];
    [self.albumArt setClipsToBounds:YES];
    
    [self.detailTableView.layer setCornerRadius:4.0];
    [self.detailTableView setClipsToBounds:YES];
    
    [whiteBackground.layer setShadowRadius:7];
    [whiteBackground.layer setMasksToBounds:NO];
    [whiteBackground.layer setShadowOpacity:1];
    [whiteBackground.layer setShadowColor:[UIColor blackColor].CGColor];
    
    staticSongLabel.font = [UIFont fontWithName:@"Novecento wide" size:14];
    staticArtistLabel.font = [UIFont fontWithName:@"Novecento wide" size:14];
    staticAlbumLabel.font = [UIFont fontWithName:@"Novecento wide" size:14];
    similarSongsLabel.font = [UIFont fontWithName:@"Novecento wide" size:16];
    
    [detailTableView getSimilarSongswithDelegate:self];
    [loadingTable startAnimating];
    
    [super viewDidLoad];

    [self getAlbumArt];
}

- (void)viewDidLayoutSubviews
{
    self.artistLabel.numberOfLines = 2;
    [self.artistLabel sizeToFit];
    self.songLabel.numberOfLines = 2;
    [self.songLabel sizeToFit];
    self.albumLabel.numberOfLines = 2;
    [self.albumLabel sizeToFit];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//  Get album art by parsing json from iTunes search api (needs to have urlconnection
//  and needs refactoring)
- (void)getAlbumArt
{
    NSMutableString *urlToAppend = [[NSMutableString alloc] initWithString:@"https://itunes.apple.com/search?term="];
    NSString *titleString = self.songName;
    NSString *artistString = self.artistName;
    NSString *albumString = self.albumName;
    NSCharacterSet *charactersToRemove = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    
    self.albumArt.image = [UIImage imageNamed:@"missingAlbum.png"];
    [self.buyButton setHidden:YES];
    
    titleString = [[titleString componentsSeparatedByCharactersInSet:charactersToRemove]
                   componentsJoinedByString:@"+"];
    artistString = [[artistString componentsSeparatedByCharactersInSet:charactersToRemove]
                    componentsJoinedByString:@"+"];
    albumString = [[albumString componentsSeparatedByCharactersInSet:charactersToRemove]
                    componentsJoinedByString:@"+"];
    
    [urlToAppend appendString:titleString];
    [urlToAppend appendString:@"+"];
    [urlToAppend appendString:artistString];
    [urlToAppend appendString:@"+"];
    [urlToAppend appendString:albumString];
    
    NSString *jsonUrl = urlToAppend;
    
    jsonUrl = [jsonUrl stringByReplacingOccurrencesOfString:@"++" withString:@"+"];
    jsonUrl = [[jsonUrl stringByReplacingOccurrencesOfString:@"++" withString:@"+"] lowercaseString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:jsonUrl]];
    DLog(@"%@", jsonUrl);
    NSURLConnection *newConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    
    self.connection = newConnection;
        
    //NSData *jsonToParse = [NSData dataWithContentsOfURL:
                         //  [NSURL URLWithString:jsonUrl]];
}

//  Use iTunes link to send to iTunes store
- (IBAction)buyButtonPressed:(id)sender
{
    if (self.buyButtonLink != nil) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.buyButtonLink]];
    } else {
        self.buyButton = nil;
    }
}

//  Table loading animation for similar songs
- (void)KVCUPlaylistDetailTableViewDidLoad:(KVCUPlaylistDetailTableView *)tableView withCount:(NSUInteger)count
{
    if (count != 0) {
        [loadingTable setHidden:YES];
        [loadingTable stopAnimating];
        [self.detailTableView setHidden:NO];
    } else {
        [loadingTable setHidden:YES];
        [self.similarSongsLabel setHidden:YES];
    }
}

- (void)KVCUPlaylistDetailTableViewLoadFailed:(KVCUPlaylistDetailTableView *)tableView error:(NSError *)paramError
{
    NSLog(@"Table Load Failed");
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.connectionData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    self.connectionData = [[NSMutableData alloc] init];
    [self.connectionData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"A connection error has occured");
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (self.connectionData != nil) {
        NSInputStream *jsonStream = [[NSInputStream alloc] initWithData:self.connectionData];
        [jsonStream open];

        if (jsonStream) {
            NSError *parseError = nil;
            id jsonObject = [NSJSONSerialization JSONObjectWithStream:jsonStream options:NSJSONReadingAllowFragments error:&parseError];
            if ([jsonObject respondsToSelector:@selector(objectForKey:)]) {
                for (NSDictionary *song in [jsonObject objectForKey:@"results"]) {
                    NSString *artUrl = [song objectForKey:@"artworkUrl100"];
                    NSString *itunesURL = [song objectForKey:@"trackViewUrl"];
                    itunesURL = [itunesURL stringByReplacingOccurrencesOfString:@"https" withString:@"itms-apps"];
                    UIImage *art = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:
                                                                  [NSURL URLWithString:artUrl]]];
                    
                    
                    [UIView transitionWithView:self.view
                                      duration:0.2f
                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                    animations:^{
                                        self.albumArt.image = art;
                                    } completion:NULL];
                    self.buyButtonLink = itunesURL;
                    
                    if (self.buyButtonLink != nil) {
                        [self.buyButton setHidden:NO];
                    }
                }
            }
        } else {
            NSLog(@"Failed to open json stream");
        }
    }
}

- (IBAction)shareButtonPressed:(id)sender {
    NSMutableString* text = [NSMutableString stringWithString:@"Listening to"];
    [text appendString:@" "];
    [text appendString:songName];
    [text appendString:@" by "];
    [text appendString:artistName];
    [text appendString:@" on the Radio1190 iPhone App!"];
    
    NSArray* activityItems = [NSArray arrayWithObjects:text, nil];
    
    UIActivityViewController *avc = [[UIActivityViewController alloc] initWithActivityItems: activityItems applicationActivities:nil];
    avc.excludedActivityTypes = [NSArray arrayWithObjects:UIActivityTypeAssignToContact, UIActivityTypeMail, UIActivityTypePrint, UIActivityTypeSaveToCameraRoll, UIActivityTypeMessage, UIActivityTypeCopyToPasteboard, nil];
    [self presentViewController:avc animated:YES completion:nil];
    
}

@end
