//
//  KVCUAppDelegate.h
//  KVCUApp
//
//  Created by Jackie Myrose on 11/23/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "ECSlidingViewController.h"
#import "KVCURadioViewController.h"

/**
 * @brief
 * Delegate for KVCU Application
 */
@interface KVCUAppDelegate : UIResponder <UIApplicationDelegate>

/// Window for the application
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) IBOutlet KVCURadioViewController *radioView;

@end

/** \mainpage KVCU App
 
 \section intro_sec Introduction
 
 This iOS application was created for KVCU Radio 1190, the University of Colorado at Boudler
 radio station. <br>
 It was developed by a group of students at CU Boulder as part of the senior
 capstone project class.
 
 The functionality of the app includes:
    <ul>
        <li> Listening to KVCU radio
        <li> Viewing today and previous day's playlists
        <li> Viewing a list of upcoming events
        <li> Viewing the DJ schedule
        <li> Viewing current and previous KVCU blog posts
        <li> Reading about Radio 1190
        <li> Viewing KVCU contact information
        <li> Viewing a website to make donations to KVCU
    </ul>
 
 \section running_sec App + Xcode
 
 \subsection running Running the App
 To run the app from XCode, simply select the 'KVCUApp' build scheme and the appropriate device or
 simulator and click run.
 
 \subsection testing Testing the App
 The application does have a few unit tests. To run the tests, ensure that the 'KVCUApp' build scheme
 is selected, then select Product->Test or hit Command+U. <br>
 This will run the tests on whichever device you have selected and the output will appear in the Xcode
 console.
 
 \subsection docs Generating Docs
 This app uses Doxygen for documentation generation (this html site). So first ensure that you
 have Doxygen installed on your machine. <br> 
 Then, select the 'KVCUDocs' build scheme (the device doesn't
 matter as the target will only run a simple script). Then hit the Run button.<br>
 The documentation will appear in the KVCUDocs folder.
 
 Currently, only HTML docs are being generated, but other options such as Latex are possible with
 Doxygen. Just edit the Doxyfile in the top level folder.
 
 \section class_diagram_sec Class Diagram
 
 */