//
//  KVCUPhotoCell.h
//  KVCUApp
//
//  Created by Ian Finlay on 4/2/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVCULazyLoadImage.h"

@interface KVCUPhotoCell : UICollectionViewCell


/// Lazy load image class
@property (nonatomic, strong) IBOutlet KVCULazyLoadImage* image;

@end
