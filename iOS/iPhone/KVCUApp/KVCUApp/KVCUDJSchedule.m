//
//  KVCUDJSchedule.m
//  KVCUApp
//
//  Created by Ian Finlay on 2/26/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUDJSchedule.h"

@implementation KVCUDJSchedule

@synthesize startTime;
@synthesize endTime;
@synthesize showName;
@synthesize showDesc;
@synthesize showSite;

@end
