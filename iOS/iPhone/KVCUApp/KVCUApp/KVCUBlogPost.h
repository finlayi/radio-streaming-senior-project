//
//  KVCUBlogPost.h
//  KVCUApp
//
//  Created by Ian Finlay on 2/12/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief
 * Class for holding information for each blog post
 */
@interface KVCUBlogPost : NSObject

/// Title of the blog post
@property (nonatomic, strong) NSString* title;

/// Content within the blog post
@property (nonatomic, strong) NSString* content;

/// The main image for the post
@property (nonatomic, strong) NSString* image;

/// The link to the blog post
@property (nonatomic, strong) NSString* blogLink;

@end
