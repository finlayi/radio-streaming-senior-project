//
//  KVCUXMLElement.m
//  KVCUApp
//
//  Created by Ian Finlay on 12/4/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import "KVCUXMLElement.h"

@implementation KVCUXMLElement

@synthesize name;
@synthesize text;
@synthesize parent;
@synthesize children;
@synthesize attributes;

- (id) init {
    self = [super init];
    
    if (self != nil) {
        NSMutableArray *childrenArray = [[NSMutableArray alloc] init];
        children = [childrenArray mutableCopy];
        NSMutableDictionary *newAttributes = [[NSMutableDictionary alloc] init];
        attributes = [newAttributes mutableCopy];
    }
    
    return self;
}

@end
