//
//  KVCUConstants.m
//  KVCUApp
//
//  Created by Jackie Myrose on 3/3/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUConstants.h"

@implementation KVCUConstants : NSObject 

NSString *const kKVCUBlogPostsURL = @"http://hireianfinlay.com/radio1190/blogXML.php?id=";
NSString *const kKVCUDonateURL = @"https://m.stayclassy.org/checkout/donation?eid=23327";
NSString *const kKVCUFacebookURL = @"fb://profile/298525772021";
NSString *const kKVCUTwitterURL = @"twitter://user?screen_name=radio1190";
NSString *const kKVCUInstagramURL = @"instagram://user?username=radio1190";
NSString *const kKVCUCurrentPlaylistURL = @"http://radio1190.colorado.edu/playlist/current/";
NSString *const kKVCURadioURL = @"http://radio1190.colorado.edu:8000/high.mp3";
NSString *const kKVCUPlaylistURL = @"http://hireianfinlay.com/radio1190/playlist.php?id=";
NSString *const kKVCUDJScheduleURL = @"http://hireianfinlay.com/radio1190/djschedule.php";
NSString *const kKVCUEventsURL = @"http://hireianfinlay.com/radio1190/eventsCal.php?id=";
NSString *const kKVCUPhotosURL = @"http://hireianfinlay.com/radio1190/photos.php";
NSString *const kKVCUFullCalenderURL = @"http://hireianfinlay.com/radio1190/eventsFullCal.php?id=";

NSString *const kKVCUDefaultSongText = @"Radio 1190 KVCU";
NSString *const kKVCUDefaultArtistText = @"www.radio1190.org";
NSString *const kKVCULoadingText = @"Loading...";

@end
