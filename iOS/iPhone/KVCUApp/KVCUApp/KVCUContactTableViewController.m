//
//  KVCUContactTableViewController.m
//  KVCUApp
//
//  Created by Ian Finlay on 2/4/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUContactTableViewController.h"
#import "KVCUMenuViewController.h"

@interface KVCUContactTableViewController ()

@end

@implementation KVCUContactTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.tableView.separatorColor = [UIColor clearColor];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.view.layer.shadowOpacity = 0.75f;
    self.navigationController.view.layer.shadowRadius = 10.0f;
    self.navigationController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[KVCUMenuViewController class]]) {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    self.slidingViewController.underRightViewController = nil;
    
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    // Return the number of sections.
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    // Return the number of rows in the section.
//    return 0;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    
//    // Configure the cell...
//    
//    return cell;
//}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate


//  Build links on static table
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *urlToOpen = @"";
    
    UIView *selectionColor = [[UIView alloc] init];
    [selectionColor setBackgroundColor:[UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f]];
    
    if (indexPath.section == 0 && indexPath.row == 1) {
        urlToOpen = @"tel:303-492-1190";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlToOpen]];
    }
    if (indexPath.section == 1 && indexPath.row == 1) {
        urlToOpen = @"mailto:?to=dj@radio1190.org";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlToOpen]];
    }
    if (indexPath.section == 2 && indexPath.row == 1) {
        urlToOpen = @"http://maps.apple.com/?q=UMC+Boulder+CO";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlToOpen]];
    }
}

- (IBAction)facebookButtonPressed:(id)sender {
    NSURL *url = [NSURL URLWithString:kKVCUFacebookURL];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)twitterButtonPressed:(id)sender {
    NSURL *url = [NSURL URLWithString:kKVCUTwitterURL];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)instagramButtonPressed:(id)sender {
    NSURL *url = [NSURL URLWithString:kKVCUInstagramURL];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

@end
