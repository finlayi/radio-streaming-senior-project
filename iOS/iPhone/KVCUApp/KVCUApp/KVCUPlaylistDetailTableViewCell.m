//
//  KVCUPlaylistDetailTableViewCell.m
//  KVCUApp
//
//  Created by Ian Finlay on 12/8/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import "KVCUPlaylistDetailTableViewCell.h"

@implementation KVCUPlaylistDetailTableViewCell

@synthesize songLabel;
@synthesize artistLabel;
@synthesize albumImage;
@synthesize cellBackground;

@end
