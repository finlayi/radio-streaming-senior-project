//
//  KVCUBlogCell.m
//  KVCUApp
//
//  Created by Ian Finlay on 2/12/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUBlogCell.h"

@implementation KVCUBlogCell

@synthesize titleLabel;
@synthesize contentLabel;
@synthesize image;
@synthesize readMore;

@end
