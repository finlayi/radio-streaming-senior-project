//
//  KVCUPlaylistCell.h
//  KVCUApp
//
//  Created by Ian Finlay on 12/5/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief
 * Custom playlist table view cell
 */
@interface KVCUPlaylistCell : UITableViewCell

/// Time song was played
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;

/// Name of song
@property (nonatomic, weak) IBOutlet UILabel *songLabel;

/// Name of artist
@property (nonatomic, weak) IBOutlet UILabel *artistLabel;

@end
