//
//  KVCUBlogPost.m
//  KVCUApp
//
//  Created by Ian Finlay on 2/12/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUBlogPost.h"

@implementation KVCUBlogPost

@synthesize image;
@synthesize title;
@synthesize content;
@synthesize blogLink;

@end
