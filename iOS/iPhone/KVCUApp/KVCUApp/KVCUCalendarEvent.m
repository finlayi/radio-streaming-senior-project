//
//  KVCUCalendarEvent.m
//  KVCUApp
//
//  Created by Ian Finlay on 2/11/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUCalendarEvent.h"

@implementation KVCUCalendarEvent

@synthesize title;
@synthesize image;
@synthesize startDate;
@synthesize endDate;
@synthesize venue;
@synthesize phone;
@synthesize address;

- (id)init
{
    self = [super init];
    return self;
}

@end
