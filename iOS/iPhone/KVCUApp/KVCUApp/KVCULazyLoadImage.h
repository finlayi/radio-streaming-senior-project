//
//  KVCULazyLoadImage.h
//  KVCUApp
//
//  Created by Ian Finlay on 2/10/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KVCULazyLoadImage;

/**
 * @brief
 * The delegate protocol for the lazy image loading
 */
@protocol KVCULazyLoadImageDelegate <NSObject>

/**
 * Method to indicate that the image has finished loading
 *
 * @param paramSender   the loaded image
 */
- (void)imageDidFinishLoading:(KVCULazyLoadImage *)paramSender;

@end

/**
 * @brief
 * The class for lazy image loading.
 * Allows images to be loaded independently of the rest of the content loading.
 */
@interface KVCULazyLoadImage : UIImageView {
    /**
     * The image data received from the URL request
     */
    NSMutableData *receivedData;
}

/// The delegate to handle when the image has loaded
@property (nonatomic, weak) id<KVCULazyLoadImageDelegate> delegate;

/// The index where the image belongs
@property (nonatomic, strong) NSIndexPath *indexPath;

/**
 * Detailed initialization method
 *
 * @param url               the URL to download the image from
 * @param imageName         the name of the image used as a placeholder
 * @param paramDelegate     the delegate for image loading
 * @param imageIndexPath    the index of the image in the table
 */
- (id)initWithUrl:(NSURL *)url withPlaceholder:(NSString *)imageName withDelegate:(id<KVCULazyLoadImageDelegate>)paramDelegate withIndex:(NSIndexPath *)imageIndexPath;

/**
 * Less detailed initialization method
 *
 * @param url               the URL to download the image from
 * @param imageName         the name of the image used as a placeholder
 */
- (id)initWithUrl:(NSURL *)url withPlaceholder:(NSString *)imageName;

/**
 * Method to download data from a URL
 *
 * @param url               the URL to download the image from
 */
- (void)loadWithUrl:(NSURL *)url;

@end
