//
//  KVCUPhotosViewController.h
//  KVCUApp
//
//  Created by Ian Finlay on 4/2/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ECSlidingViewController.h"
#import "KVCUXMLDocument.h"
#import "KVCULazyLoadImage.h"
#import "MWPhotoBrowser.h"

@interface KVCUPhotosViewController : UICollectionViewController<UICollectionViewDelegate, KVCUXMLDocumentDelegate, KVCULazyLoadImageDelegate, MWPhotoBrowserDelegate>
{
    @public
        KVCUXMLDocument *xmlDocument;
}

/// Xml document to pull photos from
@property (nonatomic, strong) KVCUXMLDocument *xmlDocument;

/// Array of photo objects
@property (nonatomic, strong) NSMutableArray *photoList;

/// Dictionary lazy loaded images
@property (nonatomic, strong) NSMutableDictionary* imageDict;

/// Array of objects for photoViewer
@property (nonatomic, strong) NSMutableArray *photos;

/// Refresh controls for photos
@property (nonatomic, strong) UIRefreshControl *refreshControl;

- (IBAction)revealMenu:(id)sender;

@end
