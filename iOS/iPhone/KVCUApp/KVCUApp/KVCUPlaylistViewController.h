//
//  KVCUSecondViewController.h
//  KVCUApp
//
//  Created by Jackie Myrose on 11/23/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ECSlidingViewController.h"
#import "KVCUXMLDocument.h"

/**
 * @brief
 * The Playlist screen view controller.
 */
@interface KVCUPlaylistViewController : UITableViewController<KVCUXMLDocumentDelegate>
{
    @public
    /**
     * The XML document to parse to retrieve playlist data
     */
    KVCUXMLDocument *xmlDocument;
}

@property (nonatomic, strong) KVCUXMLDocument *xmlDocument;

/// The array containing all played song information
@property (nonatomic, strong) NSArray *playlist;

/// Loading indicator
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loading;

/// The table view where the playlist information is displayed
@property (nonatomic, strong) IBOutlet UITableView *tableView;

/// The selected playlist date
@property (nonatomic, strong) NSDate *currentDate;

/// Button to go to next date
@property (nonatomic, strong) IBOutlet UIBarButtonItem *nextButton;

/// Button to go to previous date
@property (nonatomic, strong) IBOutlet UIBarButtonItem *lastButton;

@end
