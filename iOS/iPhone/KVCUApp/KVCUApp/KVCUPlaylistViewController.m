//
//  KVCUSecondViewController.m
//  KVCUApp
//
//  Created by Jackie Myrose on 11/23/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import "KVCUPlaylistViewController.h"
#import "KVCUXMLElement.h"
#import "KVCUPlaylistSong.h"
#import "KVCUPlaylistCell.h"
#import "KVCUPlaylistDetailViewController.h"
#import "KVCUMenuViewController.h"

@interface KVCUPlaylistViewController ()

@end

@implementation KVCUPlaylistViewController

@synthesize xmlDocument;
@synthesize playlist;
@synthesize loading;
@synthesize tableView;
@synthesize currentDate;
@synthesize nextButton;
@synthesize lastButton;

//  Add refreshControl, nextButton and lastButton
//  Try to load currentDates playlist
- (void)viewDidLoad
{
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl = refreshControl;
    self.refreshControl.tintColor = [UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f];
    
    [self.refreshControl addTarget:self action:@selector(loadPlaylistForDay) forControlEvents:UIControlEventValueChanged];

    self.currentDate = [NSDate date];
    
    UIImage *leftArrowImage = [UIImage imageNamed:@"previousPlaylist@2x.png"];
    UIButton *previousPlaylist = [UIButton buttonWithType:UIButtonTypeCustom];
    [previousPlaylist setBackgroundImage:leftArrowImage forState:UIControlStateNormal];
    [previousPlaylist setFrame:CGRectMake(0, 0, 22, 22)];
    [previousPlaylist addTarget:self action:@selector(getLastPlaylist:) forControlEvents:UIControlEventTouchDown];
    
    UIImage *rightArrowImage = [UIImage imageNamed:@"nextPlaylist@2x.png"];
    UIButton *nextPlaylist = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextPlaylist setBackgroundImage:rightArrowImage forState:UIControlStateNormal];
    [nextPlaylist setFrame:CGRectMake(0, 0, 22, 22)];
    [nextPlaylist addTarget:self action:@selector(getNextPlaylist:) forControlEvents:UIControlEventTouchDown];
    
    
    //[self.lastButton setCustomView:previousPlaylist];
    //[self.nextButton setCustomView:nextPlaylist];
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menuIcon@2x.png"] landscapeImagePhone:nil style:UIBarButtonItemStyleBordered target:self action:@selector(revealMenu:)];
    
    UIBarButtonItem *prevList = [[UIBarButtonItem alloc] initWithCustomView:previousPlaylist];
    UIBarButtonItem *nextList = [[UIBarButtonItem alloc] initWithCustomView:nextPlaylist];
    UIBarButtonItem *rightFixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    rightFixedSpaceBarButtonItem.width = 80;
    UIBarButtonItem *leftFixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    leftFixedSpaceBarButtonItem.width = 40;
    
    self.nextButton = nextList;
    self.lastButton = prevList;
    
    self.navigationItem.rightBarButtonItems = @[rightFixedSpaceBarButtonItem, rightFixedSpaceBarButtonItem, self.nextButton];
    self.navigationItem.leftBarButtonItems = @[menuButton, leftFixedSpaceBarButtonItem, self.lastButton];
    
    [self loadPlaylistForDay];
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // shadowPath, shadowOffset, and rotation is handled by ECSlidingViewController.
    // You just need to set the opacity, radius, and color.
    self.navigationController.view.layer.shadowOpacity = 0.75f;
    self.navigationController.view.layer.shadowRadius = 10.0f;
    self.navigationController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[KVCUMenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    
    self.slidingViewController.underRightViewController = nil;
    
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

//  Formats todays date into MM-dd-YYYY for php parser
//  Sends request for php script
//  On xml return, fills table for current date
//  Determines whether or not to show nextButton based on currentDate
- (void)loadPlaylistForDay
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"MST"]];
    [dateFormat setDateFormat:@"MM-dd-YYYY"];
    NSString *dateToAppend = [dateFormat stringFromDate:self.currentDate];
    
    NSMutableString *urlToAppend = [[NSMutableString alloc] initWithString:kKVCUPlaylistURL];
    [urlToAppend appendString:dateToAppend];
    
    if ([[dateFormat stringFromDate:self.currentDate]
         isEqualToString:[dateFormat stringFromDate:[NSDate date]]]) {
        self.navigationItem.rightBarButtonItems = nil;
    } else {
        UIBarButtonItem *rightFixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        rightFixedSpaceBarButtonItem.width = 80;
        self.navigationItem.rightBarButtonItems = @[rightFixedSpaceBarButtonItem, self.nextButton];
        self.refreshControl = nil;
    }
    
    UIActivityIndicatorView *loadDay = [[UIActivityIndicatorView alloc] init];
    [loadDay startAnimating];
    self.navigationItem.titleView = loadDay;

    NSString *xmlPath = urlToAppend;
    KVCUXMLDocument *newDocument = [[KVCUXMLDocument alloc] initWithDelegate:self];
    self.xmlDocument = newDocument;
    [self.xmlDocument parseRemoteXMLWithPath:xmlPath];
}

//  Changes current date to the next day to call loadPlaylistForDay
//  Reinitializes refresh control if currentDate is today
- (IBAction)getNextPlaylist:(id)sender
{    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"MST"]];
    [dateFormat setDateFormat:@"MM-dd-YYYY"];
    
    NSDateComponents *componentsToAdd = [[NSDateComponents alloc] init];
    [componentsToAdd setDay:1];
    
    NSDate *next = [[NSCalendar currentCalendar] dateByAddingComponents:componentsToAdd toDate:self.currentDate options:0];
    
    self.currentDate = next;
    
    if ([[dateFormat stringFromDate:self.currentDate]
         isEqualToString:[dateFormat stringFromDate:[NSDate date]]]) {
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
        self.refreshControl = refreshControl;
        self.refreshControl.tintColor = [UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f];
        
        [self.refreshControl addTarget:self action:@selector(loadPlaylistForDay) forControlEvents:UIControlEventValueChanged];
    }
    [self loadPlaylistForDay];
}

//  Changes currentDate to previous day to call loadPlaylistForDay
- (IBAction)getLastPlaylist:(id)sender
{
    NSDateComponents *componentsToSubtract = [[NSDateComponents alloc] init];
    [componentsToSubtract setDay:-1];
    
    NSDate *last = [[NSCalendar currentCalendar] dateByAddingComponents:componentsToSubtract toDate:self.currentDate options:0];
    
    self.currentDate = last;
    [self loadPlaylistForDay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([playlist count] == 0) {
        return 0;
    }
    return [playlist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *myIdentifier = @"myIdentifier";
    
    KVCUPlaylistCell *cell = [self.tableView dequeueReusableCellWithIdentifier:myIdentifier];
    if (cell == nil) {
        cell = [[KVCUPlaylistCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:myIdentifier];
    }
    KVCUPlaylistSong *song = [playlist objectAtIndex:indexPath.row];

    if ([playlist count] != 0) {
        cell.artistLabel.text = song.artist;
        cell.songLabel.text = song.song;
        cell.timeLabel.text = song.time;
    }
    return cell;
}

//  Only sets selection color, segue does not happen here
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIView *selectionColor = [[UIView alloc] init];
    [selectionColor setBackgroundColor:[UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f]];
    [[self.tableView cellForRowAtIndexPath:indexPath] setSelectedBackgroundView:selectionColor];
}

- (void)xmlDocumentDelegateParsingFinished:(KVCUXMLDocument *)paramSender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM d"];
    NSString *date = [dateFormat stringFromDate:self.currentDate];
    
    [self getXMLContents:self.xmlDocument.rootElement];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 1)] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.refreshControl endRefreshing];
    
    self.navigationItem.titleView = nil;
    self.navigationItem.title = date;
}

- (void)xmlDocumentDelegateParsingFailed:(KVCUXMLDocument *)paramSender withError:(NSError *)paramError
{
    NSLog(@"Failed to download/parse xml: %@", paramError);
    [self.refreshControl endRefreshing];
    [self showNoNetworkAlert];
}

- (void)getXMLContents:(KVCUXMLElement *)paramElement
{
    NSMutableArray *playlistArray = [NSMutableArray array];
    
    KVCUPlaylistSong *temp = [[KVCUPlaylistSong alloc] init];
    
    for(KVCUXMLElement *child in paramElement.children) {
        if ([child.name isEqualToString:@"time"]) {
            temp.time = child.text;
        }
        if ([child.name isEqualToString:@"artist"]) {
            temp.artist = child.text;
        }
        if ([child.name isEqualToString:@"song"]) {
            temp.song = child.text;
        }
        if ([child.name isEqualToString:@"album"]) {
            temp.album = child.text;
            [playlistArray addObject:temp];
            temp = nil;
            temp = [KVCUPlaylistSong alloc];
        }
    }
    playlist = playlistArray;
}

- (void)showNoNetworkAlert
{
	UIAlertView *baseAlert = [[UIAlertView alloc]
							  initWithTitle:@"No Network" message:@"Could not update playlist at this time."
							  delegate:nil cancelButtonTitle:nil
							  otherButtonTitles:@"OK", nil];
	[baseAlert show];
}


//  Passes all information to detailViewController for segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"playlistDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        KVCUPlaylistSong *song = [playlist objectAtIndex:indexPath.row];
        KVCUPlaylistDetailViewController *destViewController = segue.destinationViewController;
        destViewController.songName= song.song;
        destViewController.artistName = song.artist;
        destViewController.albumName = song.album;
    }
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

@end
