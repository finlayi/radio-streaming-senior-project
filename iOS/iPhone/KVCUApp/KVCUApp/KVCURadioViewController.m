//
//  KVCUFirstViewController.m
//  KVCUApp
//
//  Created by Jackie Myrose on 11/23/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import "KVCURadioViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "KVCUXMLElement.h"
#import "KVCUAppDelegate.h"
#import <MediaPlayer/MediaPlayer.h>

@interface KVCURadioViewController ()

@end

@implementation KVCURadioViewController

@synthesize player;
@synthesize xmlDocument;
@synthesize song;
@synthesize artist;
@synthesize playPauseButton;
@synthesize volumeManager;
@synthesize navBar;

//  Set default song and artist for the application on load, try to play stream and retrieve
//  song and artist information
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    KVCUAppDelegate *appDelegate = (KVCUAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.radioView = self;
    
    self.song.text = kKVCUDefaultSongText;
    self.artist.text = kKVCUDefaultArtistText;
    
    [self getSongAndArtist];
    
    NSTimer* myTimer;
    myTimer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(getSongAndArtist) userInfo:nil repeats: YES];
    
    UIImage *minVolume = [UIImage imageNamed:@"volumeActive.png"];
    UIImage *maxVolume = [UIImage imageNamed:@"volumeInactive.png"];
    minVolume = [minVolume resizableImageWithCapInsets:UIEdgeInsetsMake(0, 9, 0, 9)];
    maxVolume = [maxVolume resizableImageWithCapInsets:UIEdgeInsetsMake(0, 9, 0, 9)];
    [volumeManager setMinimumVolumeSliderImage:minVolume forState:UIControlStateNormal];
    [volumeManager setMaximumVolumeSliderImage:maxVolume forState:UIControlStateNormal];
    
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menuIcon@2x.png"] landscapeImagePhone:nil style:UIBarButtonItemStyleBordered target:self action:@selector(revealMenu:)];
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:nil];
    navItem.leftBarButtonItem = menuButton;
    navItem.hidesBackButton = YES;
    [self.navBar pushNavigationItem:navItem animated:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // shadowPath, shadowOffset, and rotation is handled by ECSlidingViewController.
    // You just need to set the opacity, radius, and color.
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[KVCUMenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    
    self.slidingViewController.underRightViewController = nil;
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
}

//  Set first responder for remote controls
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self resignFirstResponder];
}

//  Stub function for observeValueForKeyPath
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
//    AVPlayerItem *playerItem = object;
//    if ([keyPath isEqualToString:@"timedMetadata"]) {
//        for (AVMetadataItem* metadata in playerItem.timedMetadata) {
//            DLog(@"%@, %@, %@, %@", [metadata.key description], metadata.keySpace, metadata.commonKey, metadata.stringValue);
//        }
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//  Retrieve song and artist from xml, set title, artist for lock screen
- (void)getSongAndArtist
{
    if (self.player.rate == 1.0) {
        NSString *xmlPath = kKVCUCurrentPlaylistURL;
        KVCUXMLDocument *newDocument = [[KVCUXMLDocument alloc] initWithDelegate:self];
        self.xmlDocument = newDocument;
        [self.xmlDocument parseRemoteXMLWithPath:xmlPath];
        
        UIImage *lockScreenDisplay = [UIImage imageNamed:@"logo.png"];
        MPMediaItemArtwork *lockScreenArt = [[MPMediaItemArtwork alloc] initWithImage:lockScreenDisplay];
        MPNowPlayingInfoCenter *infoCenter = [MPNowPlayingInfoCenter defaultCenter];
        infoCenter.nowPlayingInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                     self.song.text, MPMediaItemPropertyTitle,
                                     self.artist.text, MPMediaItemPropertyArtist,
                                     lockScreenArt,MPMediaItemPropertyArtwork,
                                     nil];
    }
}

//  Play the AVPlayerItem (stream), set up player attributes if player is nil
- (void)playPlayer
{
    NSString *urlAddress = kKVCURadioURL;
    NSURL *urlStream = [NSURL URLWithString:urlAddress];
    
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:urlStream];
    self.player = [AVPlayer playerWithPlayerItem:playerItem];
    
    self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    [self.player addObserver:self forKeyPath:@"timedMetadata" options:NSKeyValueObservingOptionNew context:nil];
    
    if (self.player != nil) {        
        [self.player play];                
        UIImage *lockScreenDisplay = [UIImage imageNamed:@"logo.png"];
        MPMediaItemArtwork *lockScreenArt = [[MPMediaItemArtwork alloc] initWithImage:lockScreenDisplay];
        MPNowPlayingInfoCenter *infoCenter = [MPNowPlayingInfoCenter defaultCenter];
        infoCenter.nowPlayingInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                     self.song.text, MPMediaItemPropertyTitle,
                                     self.artist.text, MPMediaItemPropertyArtist,
                                     lockScreenArt, MPMediaItemPropertyArtwork,
                                     nil];
    }
}

//  Stop AVPlayerItem
- (void)stopPlayer
{
    [self.player pause];
    [self.player removeObserver:self forKeyPath:@"timedMetadata"];
}

//  Play/Pause AVPlayerItem and switch button image
- (IBAction)playPause:(id)sender
{
    UIImage *play = [UIImage imageNamed:@"playButton.png"];
    UIImage *pause = [UIImage imageNamed:@"pauseButton.png"];
    
    if (self.player.rate == 1.0) {
        [playPauseButton setImage:play forState:UIControlStateNormal];
        [self stopPlayer];
    } else {
        [playPauseButton setImage:pause forState:UIControlStateNormal];
        [self playPlayer];
        [self getSongAndArtist];
    }
}


//  Functions of delegate for xml parsing
- (void)xmlDocumentDelegateParsingFinished:(KVCUXMLDocument *)paramSender
{
    [self getXMLContents:self.xmlDocument.rootElement];
}

- (void)xmlDocumentDelegateParsingFailed:(KVCUXMLDocument *)paramSender withError:(NSError *)paramError
{
    NSLog(@"Failed to download/parse xml");
    [self showNoNetworkAlert];
    [playPauseButton setImage:[UIImage imageNamed:@"playButton.png"] forState:UIControlStateNormal];
    [self stopPlayer];
}


//  Get xml contents from xml, artist and song
- (void)getXMLContents:(KVCUXMLElement *)paramElement
{
    NSUInteger count = [paramElement.children count];
    if (count == 0) {
        self.song.text = kKVCUDefaultSongText;
        self.artist.text = kKVCUDefaultArtistText;
    } else {
        for (KVCUXMLElement *child in paramElement.children) {
            if ([child.name isEqualToString:@"artist"]) {
                self.artist.text = child.text;
            }
            if ([child.name isEqualToString:@"song"]) {
                self.song.text = child.text;
            }
        }
    }
}

// Alert for when there is no network connection or when connection to address fails
- (void)showNoNetworkAlert
{
	UIAlertView *baseAlert = [[UIAlertView alloc]
							  initWithTitle:@"No Network" message:@"A network connection is required.  Please verify your network settings and try again."
							  delegate:nil cancelButtonTitle:nil
							  otherButtonTitles:@"OK", nil];
	[baseAlert show];
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

@end
