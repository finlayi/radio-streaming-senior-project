//
//  KVCUFullCalendarViewController.m
//  KVCUApp
//
//  Created by Ian Finlay on 4/3/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUFullCalendarViewController.h"
#import "KVCUMenuViewController.h"
#import "KVCUFullCalendarEvent.h"
#import "TapkuLibrary.h"
#import "KVCUFullCalendarCell.h"
#import "KVCUFullCalenderEventViewController.h"

@interface KVCUFullCalendarViewController ()

@end

@implementation KVCUFullCalendarViewController

@synthesize calendar;
@synthesize dataArray;
@synthesize dataDictionary;
@synthesize xmlDocument;
@synthesize showList;
@synthesize currentDate;
@synthesize currentMonth;
@synthesize currentYear;
@synthesize currentStartDate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.showList = [[NSMutableDictionary alloc] init];
    self.dataArray = [NSMutableArray array];
    self.dataDictionary = [[NSMutableDictionary alloc] init];
    
    UINib *cellNib = [UINib nibWithNibName:@"calendarCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"calendarCellIdentifier"];
    
    self.monthView.delegate = self;
    self.monthView.dataSource = self;
    //calendar.frame = CGRectMake(0, 0, 320,350);
    [self.monthView selectDate:[NSDate date]];
    [self.view addSubview:self.monthView];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM"];
    NSString *formattedDateString = [dateFormatter stringFromDate:[NSDate date]];
    NSLog(@"Formated String: %@", formattedDateString);
    [self loadEventsForDate:formattedDateString];
    
    [self.monthView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // shadowPath, shadowOffset, and rotation is handled by ECSlidingViewController.
    // You just need to set the opacity, radius, and color.
    self.navigationController.view.layer.shadowOpacity = 0.75f;
    self.navigationController.view.layer.shadowRadius = 10.0f;
    self.navigationController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[KVCUMenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    
    self.slidingViewController.underRightViewController = nil;
    
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

- (void)loadEventsForDate:(NSString *)date
{
    NSString *xmlPath = [NSString stringWithFormat:@"%@%@", kKVCUFullCalenderURL, date];
    NSArray *splitString = [date componentsSeparatedByString:@"-"];
    NSInteger month = [splitString[1] intValue];
    NSInteger year = [splitString[0] intValue];
    [self.dataArray removeAllObjects];

    
    self.currentMonth = month;
    self.currentYear = year;
    
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] init];
    [loading startAnimating];
    self.navigationItem.titleView = loading;
    
    KVCUXMLDocument *newDocument = [[KVCUXMLDocument alloc] initWithDelegate:self];
    self.xmlDocument = newDocument;
    [self.xmlDocument parseRemoteXMLWithPath:xmlPath];
}

#pragma mark MonthView Delegate & DataSource
- (NSArray*) calendarMonthView:(TKCalendarMonthView*)monthView marksFromDate:(NSDate*)startDate toDate:(NSDate*)lastDate{
	//[self generateRandomDataForStartDate:startDate endDate:lastDate];
    self.currentStartDate = startDate;
    return self.dataArray;
}
- (void) calendarMonthView:(TKCalendarMonthView*)monthView didSelectDate:(NSDate*)date{
    if (currentDate == nil) {
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 1)] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    if ([date compare:currentDate] == NSOrderedDescending) {
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 1)] withRowAnimation:UITableViewRowAnimationLeft];
    }
    if ([date compare:currentDate] == NSOrderedAscending) {
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 1)] withRowAnimation:UITableViewRowAnimationRight];
    }
    
    self.currentDate = date;
}
- (void) calendarMonthView:(TKCalendarMonthView*)mv monthDidChange:(NSDate*)d animated:(BOOL)animated{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM"];
    NSString *formattedDateString = [dateFormatter stringFromDate:d];
    [self loadEventsForDate:formattedDateString];
	[super calendarMonthView:mv monthDidChange:d animated:animated];
	[self.tableView reloadData];
}


#pragma mark UITableView Delegate & DataSource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSArray *ar = self.dataDictionary[[self.monthView dateSelected]];
	if(ar == nil) return 0;
	return [ar count];
}
- (UITableViewCell *) tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *calendarCellIdentifier = @"calendarCellIdentifier";
    KVCUFullCalendarCell *cell = [tv dequeueReusableCellWithIdentifier:calendarCellIdentifier];
    if (cell == nil) cell = [[KVCUFullCalendarCell alloc] init];
    
	NSArray *eventArray = self.dataDictionary[[self.monthView dateSelected]];
    KVCUFullCalendarEvent* event = eventArray[indexPath.row];
	cell.titleLabel.text = event.title;
    cell.dateLabel.text = event.date;
	
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIView *selectionColor = [[UIView alloc] init];
    [selectionColor setBackgroundColor:[UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f]];
    [[self.tableView cellForRowAtIndexPath:indexPath] setSelectedBackgroundView:selectionColor];

    NSArray *eventArray = self.dataDictionary[[self.monthView dateSelected]];
    KVCUFullCalendarEvent* event = eventArray[indexPath.row];
    KVCUFullCalenderEventViewController *destViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil] instantiateViewControllerWithIdentifier:@"fullCalendarEventView"];
    destViewController.eventURL = event.link;
    destViewController.eventTitle = event.title;
    
    [self.navigationController pushViewController:destViewController animated:YES];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 51;
}


- (void) generateRandomDataForStartDate:(NSDate*)start endDate:(NSDate*)end{
	// this function sets up dataArray & dataDictionary
	// dataArray: has boolean markers for each day to pass to the calendar view (via the delegate function)
	// dataDictionary: has items that are associated with date keys (for tableview)
	
	
	NSLog(@"Delegate Range: %@ %@ %d",start,end,[start daysBetweenDate:end]);
	
	self.dataArray = [NSMutableArray array];
	self.dataDictionary = [NSMutableDictionary dictionary];
	
	NSDate *d = start;
	while(YES){
		
		int r = arc4random();
		if(r % 3==1){
			[self.dataDictionary setObject:@[@"Item one",@"Item two"] forKey:d];
			[self.dataArray addObject:@YES];
			
		}else if(r%4==1){
			[self.dataDictionary setObject:@[@"Item one"] forKey:d];
			[self.dataArray addObject:@YES];
			
		}else
			[self.dataArray addObject:@NO];
		
		
		NSDateComponents *info = [d dateComponentsWithTimeZone:self.monthView.timeZone];
		info.day++;
		d = [NSDate dateWithDateComponents:info];
		if([d compare:end]==NSOrderedDescending) break;
	}
	
}

- (void)xmlDocumentDelegateParsingFinished:(KVCUXMLDocument *)paramSender
{
    self.navigationItem.titleView = nil;
    self.navigationItem.title = @"Calendar";
    [self getXMLContents: self.xmlDocument.rootElement];
    [self.monthView reloadData];
    [self.tableView reloadData];
}

- (void)xmlDocumentDelegateParsingFailed:(KVCUXMLDocument *)paramSender withError:(NSError *)paramError
{
    self.navigationItem.titleView = nil;
    self.navigationItem.title = @"Calendar";
    [self showNoNetworkAlert];
    NSLog(@"Parsing Failed: %@", paramError);
}

- (void)getXMLContents:(KVCUXMLElement *)paramElement
{
    NSMutableArray *showArray = [[NSMutableArray alloc] init];
    KVCUFullCalendarEvent *temp = [[KVCUFullCalendarEvent alloc] init];
    NSString *currentDay = nil;
    NSString *nextDay = @"1";
    NSInteger eventNum = 0;
    
    for(KVCUXMLElement *child in paramElement.children) {
        if ([child.name isEqualToString:@"day"] && child.text != nil) {
            currentDay = child.text;
            if (![currentDay isEqualToString:nextDay]) {
                [showList setObject:showArray forKey:nextDay];
                //NSLog(@"%@",nextDay);
                //NSLog(@"showArray: %d", [showArray count]);
                nextDay = currentDay;
                showArray = nil;
                eventNum = 0;
                showArray = [[NSMutableArray alloc] init];
            }
        }
        if ([child.name isEqualToString:[NSString stringWithFormat:@"event-%d", eventNum]]) {
            temp.title = child.text;
        }
        if ([child.name isEqualToString:[NSString stringWithFormat:@"event-%d-link", eventNum]]) {
            temp.link = child.text;
        }
        if ([child.name isEqualToString:[NSString stringWithFormat:@"event-%d-date", eventNum]]) {
            temp.date = child.text;
            [showArray addObject:temp];
            eventNum++;
            temp = nil;
            temp = [KVCUFullCalendarEvent alloc];
        }
    }
    [showList setObject:showArray forKey:nextDay];
    [self createMarkedArray];

}

- (void)createMarkedArray
{
    NSInteger day = 1;
    NSInteger maxDay = [self.showList count];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:day];
    [components setMonth: self.currentMonth];
    [components setYear: self.currentYear];
    
    NSCalendar *tempCal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *tempComp = [tempCal components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                             fromDate:self.currentStartDate
                                                 toDate:[NSDate dateWithDateComponents:components]
                                                options:0];
    [self.dataArray removeAllObjects];
    NSLog(@"%i", tempComp.day);
    for (int i = 0; i < tempComp.day; i++) {
        [self.dataArray addObject:@NO];
    }
    
    while (day <= maxDay) {
        NSArray *dayArray = [showList objectForKey:[NSString stringWithFormat:@"%d", day]];
        if ([dayArray count] != 0) {
            [components setDay: day];
            NSDate *date = [NSDate dateWithDateComponents:components];
            [self.dataDictionary setObject:dayArray forKey:date];
            [self.dataArray addObject:@YES];
        } else
            [self.dataArray addObject:@NO];
        day++;
    }

    if ([dataArray count] != 0) {
        for (int j = 0; j < 35; j++) {
            [self.dataArray addObject:@NO];
        }
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showNoNetworkAlert
{
	UIAlertView *baseAlert = [[UIAlertView alloc]
							  initWithTitle:@"No Network" message:@"Could not get calendar events at this time."
							  delegate:nil cancelButtonTitle:nil
							  otherButtonTitles:@"OK", nil];
	[baseAlert show];
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

@end
