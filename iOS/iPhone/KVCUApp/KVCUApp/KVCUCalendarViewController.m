//
//  KVCUCalendarViewController.m
//  KVCUApp
//
//  Created by Ian Finlay on 2/5/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUCalendarViewController.h"
#import "KVCUCalendarEvent.h"
#import "KVCUCalendarEventCell.h"
#import "KVCUCalendarEventViewController.h"
#import "KVCUDJSchedule.h"
#import "KVCUDJScheduleCell.h"
#import "KVCUMenuViewController.h"

@interface KVCUCalendarViewController ()

@end

@implementation KVCUCalendarViewController

@synthesize xmlDocument;
@synthesize tableView;
@synthesize eventList;
@synthesize currentPage;
@synthesize refresh;
@synthesize imageDict;
@synthesize djList;
@synthesize djDocument;
@synthesize currentSegment;
@synthesize firstLoad;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl = refreshControl;
    self.refreshControl.tintColor = [UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f];
    self.currentPage = 1;
    self.currentSegment = 0;
    self.firstLoad = YES;
    self.imageDict = [[NSMutableDictionary alloc] init];
    self. djList = [[NSMutableDictionary alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshEvents) forControlEvents:UIControlEventValueChanged];
    [self refreshEvents];
    [self loadShows];
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // shadowPath, shadowOffset, and rotation is handled by ECSlidingViewController.
    // You just need to set the opacity, radius, and color.
    self.navigationController.view.layer.shadowOpacity = 0.75f;
    self.navigationController.view.layer.shadowRadius = 10.0f;
    self.navigationController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[KVCUMenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    
    self.slidingViewController.underRightViewController = nil;
    
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.currentSegment == 1) {
        return 7;
    } else
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.currentSegment == 0) {
        if ([eventList count] == 0) {
            return 1;
        }
        return ([eventList count] + 1);
    } else {
        if (section == 0) {
            return [[djList objectForKey:@"Monday"] count];
        }
        if (section == 1) {
            return [[djList objectForKey:@"Tuesday"] count];
        }
        if (section == 2) {
            return [[djList objectForKey:@"Wednesday"] count];
        }
        if (section == 3) {
            return [[djList objectForKey:@"Thursday"] count];
        }
        if (section == 4) {
            return [[djList objectForKey:@"Friday"] count];
        }
        if (section == 5) {
            return [[djList objectForKey:@"Saturday"] count];
        }
        if (section == 6) {
            return [[djList objectForKey:@"Sunday"] count];
        }
    }
    return 1;
} 

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"eventIdentifier";
    static NSString *loadMoreIdentifier = @"loadMoreIdentifier";
    static NSString *djScheduleIdentifier = @"djSchedIdentifier";
    
    KVCUCalendarEventCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UITableViewCell *loadMoreCell = [self.tableView dequeueReusableCellWithIdentifier:loadMoreIdentifier];
    KVCUDJScheduleCell *djCell = [self.tableView dequeueReusableCellWithIdentifier:djScheduleIdentifier];
    
    if (cell == nil) {
        cell = [[KVCUCalendarEventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (loadMoreCell == nil) {
        loadMoreCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:loadMoreIdentifier];
    }
    if (self.currentSegment == 0) {
        if (([eventList count] != 0) && ([eventList count] > indexPath.row)) {
            KVCUCalendarEvent *event = [eventList objectAtIndex:indexPath.row];
            if ([imageDict objectForKey:indexPath] == nil) {
                cell.image = [cell.image initWithUrl:[NSURL URLWithString:event.image] withPlaceholder:@"blogPostPlaceholder.png" withDelegate:self withIndex:indexPath];
            } else {
                cell.image.image = [imageDict objectForKey:indexPath];
            }
            
            cell.titleLabel.font = [UIFont fontWithName:@"Novecento wide" size:14];
            cell.titleLabel.text = event.title;
            cell.dateLabel.text = event.startDate;
            return cell;
        } else if ([eventList count] == indexPath.row && indexPath.row != 0){
            loadMoreCell.textLabel.font = [UIFont fontWithName:@"Novecento wide" size:20];
            loadMoreCell.textLabel.text = kKVCULoadingText;
            return loadMoreCell;
        } else {
            loadMoreCell.textLabel.font = [UIFont fontWithName:@"Novecento wide" size:20];
            loadMoreCell.textLabel.text = kKVCULoadingText;
            return loadMoreCell;
        }
    } else {
        KVCUDJSchedule *show = [[KVCUDJSchedule alloc] init];
        if (indexPath.section == 0) {
            NSMutableArray *dayArray = [djList objectForKey:@"Monday"];
            show = [dayArray objectAtIndex:indexPath.row];
        }
        if (indexPath.section == 1) {
            NSMutableArray *dayArray = [djList objectForKey:@"Tuesday"];
            show = [dayArray objectAtIndex:indexPath.row];
        }
        if (indexPath.section == 2) {
            NSMutableArray *dayArray = [djList objectForKey:@"Wednesday"];
            show = [dayArray objectAtIndex:indexPath.row];
        }
        if (indexPath.section == 3) {
            NSMutableArray *dayArray = [djList objectForKey:@"Thursday"];
            show = [dayArray objectAtIndex:indexPath.row];
        }
        if (indexPath.section == 4) {
            NSMutableArray *dayArray = [djList objectForKey:@"Friday"];
            show = [dayArray objectAtIndex:indexPath.row];
        }
        if (indexPath.section == 5) {
            NSMutableArray *dayArray = [djList objectForKey:@"Saturday"];
            show = [dayArray objectAtIndex:indexPath.row];
        }
        if (indexPath.section == 6) {
            NSMutableArray *dayArray = [djList objectForKey:@"Sunday"];
            show = [dayArray objectAtIndex:indexPath.row];
        }
        
        djCell.showName.text = show.showName;
        djCell.showDesc.text = show.showDesc;
        djCell.startTime.text = show.startTime;
        djCell.endTime.text = show.endTime;
        djCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return djCell;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentSegment == 1) {
        return 66;
    }
    return 58;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentSegment == 1) {
        cell.backgroundColor = (indexPath.row%2)
        ? [UIColor colorWithRed:(240.0f/255.0f) green:(240.0f/255.0f) blue:(240.0f/255.0f) alpha:(240.0f/255.0f)]
        : [UIColor whiteColor];
    } else {
        if (indexPath.row == [eventList count]-1) {
            [self loadEvents];
        }
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentSegment == 0) {
        if ([eventList count] == indexPath.row) {
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            cell.textLabel.text = kKVCULoadingText;
            [self loadEvents];
        }
    } else {
        
    }
    UIView *selectionColor = [[UIView alloc] init];
    [selectionColor setBackgroundColor:[UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f]];
    [[self.tableView cellForRowAtIndexPath:indexPath] setSelectedBackgroundView:selectionColor];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (self.currentSegment == 1) {
        if (section == 0) {
            return @"Monday";
        }
        if (section == 1) {
            return @"Tuesday";
        }
        if (section == 2) {
            return @"Wednesday";
        }
        if (section == 3) {
            return @"Thursday";
        }
        if (section == 4) {
            return @"Friday";
        }
        if (section == 5) {
            return @"Saturday";
        }
        if (section == 6) {
            return @"Sunday";
        }
    }
    return @"";
}

- (void)loadEvents
{
    self.currentPage++;
    NSString *xmlPath = [NSString stringWithFormat:@"%@%d", kKVCUEventsURL, self.currentPage];
    KVCUXMLDocument *newDocument = [[KVCUXMLDocument alloc] initWithDelegate:self];
    self.xmlDocument = newDocument;
    [self.xmlDocument parseRemoteXMLWithPath:xmlPath];
}

- (void)loadShows
{
    NSString *xmlPath = [NSString stringWithFormat:@"%@", kKVCUDJScheduleURL];
    KVCUXMLDocument *newDocument = [[KVCUXMLDocument alloc] initWithDelegate:self];
    self.djDocument = newDocument;
    [self.djDocument parseRemoteXMLWithPath:xmlPath];
}

- (void)refreshEvents
{
    if (self.currentSegment == 0) {
        NSString *xmlPath = [NSString stringWithFormat:@"%@%d", kKVCUEventsURL, 1];
        self.currentPage = 1;
        self.refresh = YES;
        KVCUXMLDocument *newDocument = [[KVCUXMLDocument alloc] initWithDelegate:self];
        self.xmlDocument = newDocument;
        [self.xmlDocument parseRemoteXMLWithPath:xmlPath];
    } else {
        [self loadShows];
    }
}


- (void)xmlDocumentDelegateParsingFinished:(KVCUXMLDocument *)paramSender
{
    if (paramSender == self.xmlDocument) {
        if (self.refresh == YES) {
            [self.eventList removeAllObjects];
            [self.imageDict removeAllObjects];
            self.refresh = NO;
        }
        [self getXMLContents: self.xmlDocument.rootElement];
        [self.refreshControl endRefreshing];
        if (self.firstLoad == YES) {
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 1)] withRowAnimation:UITableViewRowAnimationAutomatic];
            self.firstLoad = NO;
        } else
            [self.tableView reloadData];
    }
    if (paramSender == self.djDocument) {
        [self getXMLContentsForDJ: self.djDocument.rootElement];
        [self.refreshControl endRefreshing];
        [self.tableView reloadData];
    }
}

- (void)xmlDocumentDelegateParsingFailed:(KVCUXMLDocument *)paramSender withError:(NSError *)paramError
{
    [self.refreshControl endRefreshing];
    [self showNoNetworkAlert];
    NSLog(@"Parsing Failed: %@", paramError);
}

- (void)getXMLContents:(KVCUXMLElement *)paramElement
{
    NSMutableArray *eventArray = [NSMutableArray array];
    
    KVCUCalendarEvent *temp = [[KVCUCalendarEvent alloc] init];
    
    for(KVCUXMLElement *child in paramElement.children) {
        if ([child.name isEqualToString:@"title"]) {
            temp.title = child.text;
        }
        if ([child.name isEqualToString:@"content"]) {
            temp.content = child.text;
        }
        if ([child.name isEqualToString:@"imageLink"]) {
            temp.image = child.text;
        }
        if ([child.name isEqualToString:@"startDate"]) {
            temp.startDate = child.text;
        }
        if ([child.name isEqualToString:@"endDate"]) {
            temp.endDate = child.text;
        }
        if ([child.name isEqualToString:@"venue"]) {
            temp.venue = child.text;
        }
        if ([child.name isEqualToString:@"phone"]) {
            temp.phone = child.text;
        }
        if ([child.name isEqualToString:@"address"]) {
            temp.address = child.text;
            [eventArray addObject:temp];
            temp = nil;
            temp = [KVCUCalendarEvent alloc];
        }
    }
    if ([eventList count] == 0) {
        eventList = eventArray;
    } else {
        for (int i = 0; i < [eventList count]; i++) {
            for (int j = 0; j < [eventArray count]; j++) {
                KVCUCalendarEvent* event1 = [eventList objectAtIndex:i];
                KVCUCalendarEvent* event2 = [eventArray objectAtIndex:j];
                if ([event1.title isEqualToString:event2.title]) {
                    [eventArray removeObjectAtIndex:j];
                }
            }
        }
        [eventList addObjectsFromArray:eventArray];
    }
}

- (void)getXMLContentsForDJ:(KVCUXMLElement *)paramElement
{
    NSMutableArray *showArray = [[NSMutableArray alloc] init];
    KVCUDJSchedule *temp = [[KVCUDJSchedule alloc] init];
    NSString *currentDay = nil;
    NSString *nextDay = @"Monday";
    
    for(KVCUXMLElement *child in paramElement.children) {
        if ([child.name isEqualToString:@"dayName"] && child.text != nil) {
            currentDay = child.text;
            if (![currentDay isEqualToString:nextDay]) {
                [djList setObject:showArray forKey:nextDay];
                nextDay = currentDay;
                showArray = nil;
                showArray = [[NSMutableArray alloc] init];
            }
        }
        if ([child.name isEqualToString:@"startTime"]) {
            temp.startTime = child.text;
        }
        if ([child.name isEqualToString:@"endTime"]) {
            temp.endTime = child.text;
        }
        if ([child.name isEqualToString:@"showName"]) {
            temp.showName = child.text;
        }
        if ([child.name isEqualToString:@"showDesc"]) {
            temp.showDesc = child.text;
        }
        if ([child.name isEqualToString:@"showSite"]) {
            temp.showSite = child.text;
            [showArray addObject:temp];
            temp = nil;
            temp = [KVCUDJSchedule alloc];
        }
    }
    [djList setObject:showArray forKey:nextDay];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"eventDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        KVCUCalendarEvent *detailEvent = [eventList objectAtIndex:indexPath.row];
        KVCUCalendarEventViewController *destViewController = segue.destinationViewController;
        destViewController.imageURL = detailEvent.image;
        destViewController.event = detailEvent;
    }
}

- (IBAction)segmentSwitch:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        self.currentSegment = 0;
        [self.tableView reloadData];
    }
    else{
        self.currentSegment = 1;
        [self.tableView reloadData];
    }
}

- (void)imageDidFinishLoading:(KVCULazyLoadImage *)paramSender
{
    if (paramSender.image != nil) {
        [imageDict setObject:paramSender.image forKey:paramSender.indexPath];
    }
}

- (void)showNoNetworkAlert
{
	UIAlertView *baseAlert = [[UIAlertView alloc]
							  initWithTitle:@"No Network" message:@"Could not get calendar events at this time."
							  delegate:nil cancelButtonTitle:nil
							  otherButtonTitles:@"OK", nil];
	[baseAlert show];
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

@end
