//
//  KVCUBlogPostViewController.m
//  KVCUApp
//
//  Created by Ian Finlay on 2/12/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUFullCalenderEventViewController.h"

@interface KVCUFullCalenderEventViewController ()

@end

@implementation KVCUFullCalenderEventViewController

@synthesize eventURL;
@synthesize webView;
@synthesize loading;
@synthesize eventTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self.loading startAnimating];
    [self.webView setAlpha:0.0];
    [self.webView setDelegate:self];
    NSURL *url = [NSURL URLWithString:self.eventURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"%@", error);
    [self showNoNetworkAlert];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.webView stringByEvaluatingJavaScriptFromString:@"$('#header-container').css('display', 'none');$('.socialbox-container').css('display', 'none');$('.masonry').css('display', 'none');$('.widecolumn').attr('style', 'width: 100% !important; margin: 0 !important; padding: 0 !important');$('.navigation').css('display', 'none'); $('img').attr('style', 'width: 100% !important; height: 100% !important');$('.back').css('display', 'none'); $('.hentry').attr('style', 'padding: 10px 5px 10px;');$('.entry-title').attr('style', 'font-size: 30px !important'); $('body').attr('style', '-webkit-text-size-adjust: 100%'); $('iframe').attr('style', 'width: 100% !important; height: 100% !important');"];
    
    [NSTimer scheduledTimerWithTimeInterval:1.5
                                     target:self
                                   selector:@selector(showWebView)
                                   userInfo:nil
                                    repeats:NO];
    
}

- (void)showWebView
{
    [self.loading stopAnimating];
    [self.loading setHidden:YES];
    
    [UIView animateWithDuration:0.2 animations:^() {
        self.webView.alpha = 1.0;
    }];
}

- (IBAction)shareButtonPressed:(id)sender {
    NSString* text = [NSString stringWithFormat:@"%@ - found on the Radio1190 iPhone App!", self.eventTitle];
    NSURL* url = [NSURL URLWithString:self.eventURL];
    
    NSArray* activityItems = [NSArray arrayWithObjects:text, url, nil];
    
    UIActivityViewController *avc = [[UIActivityViewController alloc] initWithActivityItems: activityItems applicationActivities:nil];
    avc.excludedActivityTypes = [NSArray arrayWithObjects:UIActivityTypeAssignToContact, UIActivityTypeMail, UIActivityTypePrint, UIActivityTypeSaveToCameraRoll, UIActivityTypeMessage, UIActivityTypeCopyToPasteboard, nil];
    [self presentViewController:avc animated:YES completion:nil];
    
}

- (void)showNoNetworkAlert
{
	UIAlertView *baseAlert = [[UIAlertView alloc]
							  initWithTitle:@"No Network" message:@"Could not connect to Radio 1190 at this time."
							  delegate:nil cancelButtonTitle:nil
							  otherButtonTitles:@"OK", nil];
	[baseAlert show];
}

@end
