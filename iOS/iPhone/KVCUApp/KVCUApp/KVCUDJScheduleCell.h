//
//  KVCUDJScheduleCell.h
//  KVCUApp
//
//  Created by Ian Finlay on 2/26/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * @brief
 * Custom DJ Schedule table view cell
 */
@interface KVCUDJScheduleCell : UITableViewCell

/// Start time of DJ
@property (nonatomic, weak) IBOutlet UILabel *startTime;

/// End time of DJ
@property (nonatomic, weak) IBOutlet UILabel *endTime;

/// Name of DJ show
@property (nonatomic, weak) IBOutlet UILabel *showName;

/// Show description
@property (nonatomic, weak) IBOutlet UILabel *showDesc;

@end
