//
//  KVCUXMLElement.h
//  KVCUApp
//
//  Created by Ian Finlay on 12/4/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief
 * An XML element class.
 * Used to keep track of each element's information
 */
@interface KVCUXMLElement : NSObject {
    @public
    /**
     * Name of the element
     */
    NSString *name;
    
    /**
     * Text contained in the element
     */
    NSString *text;
    
    /**
     * Parent XML element
     */
    KVCUXMLElement *parent;
    
    /**
     * Children of this XML element
     */
    NSMutableArray *children;
    
    /**
     * Attributes of the element
     */
    NSMutableDictionary *attributes;
}

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) KVCUXMLElement *parent;
@property (nonatomic, strong) NSMutableArray *children;
@property (nonatomic, strong) NSMutableDictionary *attributes;

@end
