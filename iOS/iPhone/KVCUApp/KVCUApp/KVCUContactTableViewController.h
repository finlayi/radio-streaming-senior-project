//
//  KVCUContactTableViewController.h
//  KVCUApp
//
//  Created by Ian Finlay on 2/4/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ECSlidingViewController.h"

/**
 * @brief
 * The Contact page view controller.
 * Uses a table view to show the contacts.
 */
@interface KVCUContactTableViewController : UITableViewController

/// Button to go to KVCU's facebook page
@property (nonatomic, weak) IBOutlet UIButton* fackbookButton;

/// Button to go to KVCU's twitter page
@property (nonatomic, weak) IBOutlet UIButton* twitterButton;

/// Button to go to KVCU's instagram page
@property (nonatomic, weak) IBOutlet UIButton* instagramButton;

- (IBAction)revealMenu:(id)sender;

@end
