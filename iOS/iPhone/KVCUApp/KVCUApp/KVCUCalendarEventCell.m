//
//  KVCUCalendarEventCell.m
//  KVCUApp
//
//  Created by Ian Finlay on 2/11/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUCalendarEventCell.h"

@implementation KVCUCalendarEventCell

@synthesize titleLabel;
@synthesize dateLabel;
@synthesize image;

@end
