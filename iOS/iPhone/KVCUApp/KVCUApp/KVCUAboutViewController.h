//
//  KVCUAboutViewController.h
//  KVCUApp
//
//  Created by Jackie Myrose on 12/2/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ECSlidingViewController.h"

/**
 * @brief
 * The About page view controller.
 * All logistics are in the storyboard.
 */
@interface KVCUAboutViewController : UIViewController

/// The donate button to take user to donation page
@property (nonatomic, weak) IBOutlet UIBarButtonItem* donateButton;

- (IBAction)revealMenu:(id)sender;

@end