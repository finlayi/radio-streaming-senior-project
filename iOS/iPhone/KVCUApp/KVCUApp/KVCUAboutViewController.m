//
//  KVCUAboutViewController.m
//  KVCUApp
//
//  Created by Jackie Myrose on 12/2/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import "KVCUAboutViewController.h"
#import "KVCUMenuViewController.h"

@interface KVCUAboutViewController ()

@end

@implementation KVCUAboutViewController

@synthesize donateButton;

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.view.layer.shadowOpacity = 0.75f;
    self.navigationController.view.layer.shadowRadius = 10.0f;
    self.navigationController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[KVCUMenuViewController class]]) {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    self.slidingViewController.underRightViewController = nil;
    
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)donateButtonAction:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kKVCUDonateURL]];
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

@end
