//
//  KVCUPlaylistDetailSimilarSong.h
//  KVCUApp
//
//  Created by Ian Finlay on 12/8/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief
 * Class for holding similar song information
 */
@interface KVCUPlaylistDetailSimilarSong : NSObject {
    /**
     * Name of song
     */
    NSString *similarSongName;
    
    /**
     * Name of artist
     */
    NSString *similarArtistName;
    
    /**
     * Name of album
     */
    NSString *similarArtName;
}

@property (nonatomic, strong) NSString *similarSongName;
@property (nonatomic, strong) NSString *similarArtistName;
@property (nonatomic, strong) NSString *similarArtName;

@end
