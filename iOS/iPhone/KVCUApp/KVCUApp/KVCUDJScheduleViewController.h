//
//  KVCUDJScheduleViewController.h
//  KVCUApp
//
//  Created by Ian Finlay on 4/2/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "KVCUXMLDocument.h"
#import "ECSlidingViewController.h"
#import "KVCUDJSchedule.h"
#import "KVCUDJScheduleCell.h"

@interface KVCUDJScheduleViewController : UITableViewController<KVCUXMLDocumentDelegate>
{
@public
    /**
     * The XML document to pull calendar information from
     */
    KVCUXMLDocument *xmlDocument;
}

@property (nonatomic, strong) KVCUXMLDocument *xmlDocument;

/// Dictionary for storage of parsed DJ Schedule information
@property (nonatomic, strong) NSMutableDictionary *djList;

/// Reveals side menu
- (IBAction)revealMenu:(id)sender;

@end
