//
//  KVCUCalendarEventCell.h
//  KVCUApp
//
//  Created by Ian Finlay on 2/11/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVCULazyLoadImage.h"

/**
 * @brief
 * Custom table view cell for events calendar
 */
@interface KVCUCalendarEventCell : UITableViewCell

/// Label for title of event
@property (nonatomic, weak) IBOutlet UILabel* titleLabel;

/// Label for date of event
@property (nonatomic, weak) IBOutlet UILabel* dateLabel;

/// Image for the event
@property (nonatomic, strong) IBOutlet KVCULazyLoadImage* image;


@end
