//
//  KVCUPlaylistSong.h
//  KVCUApp
//
//  Created by Ian Finlay on 12/5/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief
 * Class to hold playlist song information
 */
@interface KVCUPlaylistSong : NSObject {
    @public
    /**
     * Song artist
     */
    NSString *artist;
    
    /**
     * Time song was played
     */
    NSString *time;
    
    /**
     * Name of song
     */
    NSString *song;
    
    /**
     * Name of song's album
     */
    NSString *album;
}

@property (nonatomic, strong) NSString *artist;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *song;
@property (nonatomic, strong) NSString *album;

@end
