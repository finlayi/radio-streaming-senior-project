//
//  KVCUPlaylistDetailTableView.h
//  KVCUApp
//
//  Created by Ian Finlay on 12/8/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVCUXMLDocument.h"

@class KVCUPlaylistDetailTableView;

/**
 * @brief
 * Protocol for table view loading callbacks
 */
@protocol KVCUPlaylistDetailTableViewDelegate <NSObject>

@required

/**
 * Callback indicating the table view has successfully loaded
 *
 * @param tableView     the table view being loaded
 * @param count         the number of cells
 */
- (void)KVCUPlaylistDetailTableViewDidLoad:(KVCUPlaylistDetailTableView *)tableView withCount:(NSUInteger)count;

/**
 * Callback indicating the table view failed to load
 *
 * @param tableView     the table view being loaded
 * @param paramError    the error which caused the failure to load
 */
- (void)KVCUPlaylistDetailTableViewLoadFailed:(KVCUPlaylistDetailTableView*)tableView error:(NSError *)paramError;

@end

/**
 * @brief
 * Custom table view for detail screen
 */
@interface KVCUPlaylistDetailTableView : UIView<KVCUXMLDocumentDelegate,UITableViewDelegate,
                                                        UITableViewDataSource>
{
    @public
    /**
     * Delegate for table view loading callbacks
     */
    __weak id<KVCUPlaylistDetailTableViewDelegate> delegate;
}

/// Name of artist
@property (nonatomic, strong) NSString *artist;

/// Name of song
@property (nonatomic, strong) NSString *song;

/// Name of album
@property (nonatomic, strong) NSString *album;

/// Table for further song detail
@property (nonatomic, strong) IBOutlet UITableView *detailTable;

/// XML document to parse to get detailed song information
@property (nonatomic, strong) KVCUXMLDocument *xmlDocument;

/// List of similar songs
@property (nonatomic, strong) NSArray *similarSongs;

@property (nonatomic, weak) id<KVCUPlaylistDetailTableViewDelegate> delegate;

/**
 * Initialization method
 *
 * @param artist    name of artist
 * @param song      name of song
 * @param album     name of album
 */
- (id)initWithArtist:(NSString *)artist song:(NSString *)song album:(NSString *)album;

/**
 * Method to retrieve similar songs
 *
 * @param paramDelegate     delegate for similar song retrieval callbacks
 */
- (void)getSimilarSongswithDelegate:(id<KVCUPlaylistDetailTableViewDelegate>)paramDelegate;

@end
