//
//  KVCUFullCalendarEvent.h
//  KVCUApp
//
//  Created by Ian Finlay on 4/3/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KVCUFullCalendarEvent : NSObject

/// Title of event
@property (nonatomic, strong) NSString *title;

/// Link for event
@property (nonatomic, strong) NSString *link;

/// Date and time of event
@property (nonatomic, strong) NSString *date;

@end
