//
//  KVCUBlogViewController.m
//  KVCUApp
//
//  Created by Ian Finlay on 2/12/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUBlogViewController.h"
#import "KVCUBlogPost.h"
#import "KVCUBlogCell.h"
#import "KVCUBlogPostViewController.h"


@interface KVCUBlogViewController ()

@end

@implementation KVCUBlogViewController

@synthesize xmlDocument;
@synthesize tableView;
@synthesize postList;
@synthesize currentPage;
@synthesize refresh;
@synthesize imageDict;
@synthesize firstLoad;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

/// Initialize data structures for views and subviews
- (void)viewDidLoad
{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl = refreshControl;
    self.refreshControl.tintColor = [UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f];
    [self.refreshControl addTarget:self action:@selector(refreshPosts) forControlEvents:UIControlEventValueChanged];
    [super viewDidLoad];
    self.firstLoad = YES;
    self.tableView.separatorColor = [UIColor clearColor];
    self.imageDict = [[NSMutableDictionary alloc] init];
    self.currentPage = 1;
    [self refreshPosts];
}

/// Set up shadow on sliding view controller
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // shadowPath, shadowOffset, and rotation is handled by ECSlidingViewController.
    // You just need to set the opacity, radius, and color.
    self.navigationController.view.layer.shadowOpacity = 0.75f;
    self.navigationController.view.layer.shadowRadius = 10.0f;
    self.navigationController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[KVCUMenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    
    self.slidingViewController.underRightViewController = nil;
    
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/// Reset xml document, grab new blog information from site
- (void)refreshPosts
{
    NSString *xmlPath = [NSString stringWithFormat:@"%@%d", kKVCUBlogPostsURL, 1];
    self.currentPage = 1;
    self.refresh = YES;
    KVCUXMLDocument *newDocument = [[KVCUXMLDocument alloc] initWithDelegate:self];
    self.xmlDocument = newDocument;
    [self.xmlDocument parseRemoteXMLWithPath:xmlPath];
    //[self.postList removeAllObjects];
}

/// Increment current page, get new page from site
- (void)loadPosts
{
    self.currentPage++;
    NSString *xmlPath = [NSString stringWithFormat:@"%@%d", kKVCUBlogPostsURL, self.currentPage];
    KVCUXMLDocument *newDocument = [[KVCUXMLDocument alloc] initWithDelegate:self];
    self.xmlDocument = newDocument;
    [self.xmlDocument parseRemoteXMLWithPath:xmlPath];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([postList count] == 0) {
        return 1;
    }
    return ([postList count] + 1);
}

/// Cells for view
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"blogIdentifier";
    static NSString *loadMoreIdentifier = @"blogLoadMore";
    KVCUBlogCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UITableViewCell *loadMoreCell = [self.tableView dequeueReusableCellWithIdentifier:loadMoreIdentifier];
    if (cell == nil) {
        cell = [[KVCUBlogCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (loadMoreCell == nil) {
        loadMoreCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:loadMoreIdentifier];
    }
    /// Regular cell
    if (([postList count] != 0) && ([postList count] > indexPath.row)) {
        KVCUBlogPost *post = [postList objectAtIndex:indexPath.row];
        if ([imageDict objectForKey:indexPath] == nil) {
            cell.image = [cell.image initWithUrl:[NSURL URLWithString:post.image] withPlaceholder:@"blogPostPlaceholder.png" withDelegate:self withIndex:indexPath];
        } else {
            cell.image.image = [imageDict objectForKey:indexPath];
        }
        cell.titleLabel.text = post.title;
        cell.readMore.font = [UIFont fontWithName:@"Novecento wide" size:16];
        cell.titleLabel.font = [UIFont fontWithName:@"Museo Slab" size:16];
        cell.image.backgroundColor = [UIColor blackColor];
        cell.contentLabel.text = post.content;
        [cell.readMore setHidden:NO];
        return cell;
    } else if ([postList count] == indexPath.row && indexPath.row != 0){
        /// Last cell
        loadMoreCell.textLabel.text = kKVCULoadingText;
        loadMoreCell.textLabel.font = [UIFont fontWithName:@"Novecento wide" size:20];
        return loadMoreCell;
    } else {
        /// Default cell
        loadMoreCell.textLabel.text = kKVCULoadingText;
        loadMoreCell.textLabel.font = [UIFont fontWithName:@"Novecento wide" size:20];
        return loadMoreCell;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [postList count]-1) {
        [self loadPosts];
    }
    if (indexPath.row == [postList count]) {
        cell.backgroundColor = [UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [postList count]) {
        return 70;
    }
    return 342;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([postList count] == indexPath.row) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.textLabel.text = kKVCULoadingText;
        [self loadPosts];
    }
}

- (void)xmlDocumentDelegateParsingFinished:(KVCUXMLDocument *)paramSender
{
    if (self.refresh == YES) {
        [self.postList removeAllObjects];
        [self.imageDict removeAllObjects];
        self.refresh = NO;
    }
    [self getXMLContents:self.xmlDocument.rootElement];
    [self.refreshControl endRefreshing];
    self.tableView.separatorColor = [UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f];
    if (self.firstLoad == YES) {
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 1)] withRowAnimation:UITableViewRowAnimationAutomatic];
        self.firstLoad = NO;
    } else
        [self.tableView reloadData];    
}

- (void)xmlDocumentDelegateParsingFailed:(KVCUXMLDocument *)paramSender withError:(NSError *)paramError
{
    [self.refreshControl endRefreshing];
    [self showNoNetworkAlert];
    NSLog(@"Parsing Failed: %@", paramError);
}

- (void)getXMLContents:(KVCUXMLElement *)paramElement
{
    NSMutableArray *blogArray = [NSMutableArray array];
    
    KVCUBlogPost *temp = [[KVCUBlogPost alloc] init];
    for(KVCUXMLElement *child in paramElement.children) {
        if ([child.name isEqualToString:@"title"]) {
            temp.title = child.text;
        }
        if ([child.name isEqualToString:@"content"]) {
            temp.content = child.text;
        }
        if ([child.name isEqualToString:@"imageLink"]) {
            temp.image = child.text;
        }
        if ([child.name isEqualToString:@"blogLink"]) {
            temp.blogLink = child.text;
            [blogArray addObject:temp];
            temp = nil;
            temp = [KVCUBlogPost alloc];
        }
    }
    if ([postList count] == 0) {
        postList = blogArray;
    } else {
        for (int i = 0; i < [postList count]; i++) {
            for (int j = 0; j < [blogArray count]; j++) {
                KVCUBlogPost* post1 = [postList objectAtIndex:i];
                KVCUBlogPost* post2 = [blogArray objectAtIndex:j];
                if ([post1.title isEqualToString:post2.title]) {
                    [blogArray removeObjectAtIndex:j];
                }
            }
        }
        [postList addObjectsFromArray:blogArray];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"postDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        KVCUBlogPost *detailBlog = [postList objectAtIndex:indexPath.row];
        KVCUBlogPostViewController *destViewController = segue.destinationViewController;
        destViewController.blogURL = detailBlog.blogLink;
        destViewController.blogTitle = detailBlog.title;
    }
}

- (void)imageDidFinishLoading:(KVCULazyLoadImage *)paramSender
{
    if (paramSender.image != nil) {
        [imageDict setObject:paramSender.image forKey:paramSender.indexPath];
    }
}

- (void)showNoNetworkAlert
{
	UIAlertView *baseAlert = [[UIAlertView alloc]
							  initWithTitle:@"No Network" message:@"Could not connect to Radio 1190 at this time."
							  delegate:nil cancelButtonTitle:nil
							  otherButtonTitles:@"OK", nil];
	[baseAlert show];
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

@end
