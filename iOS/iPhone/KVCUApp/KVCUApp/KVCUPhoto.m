//
//  KVCUPhoto.m
//  KVCUApp
//
//  Created by Ian Finlay on 4/2/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUPhoto.h"

@implementation KVCUPhoto

@synthesize imageLink;
@synthesize thumbLink;
@synthesize description;

@end
