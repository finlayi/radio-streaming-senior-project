//
//  KVCUPhotosViewController.m
//  KVCUApp
//
//  Created by Ian Finlay on 4/2/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUPhotosViewController.h"
#import "KVCUMenuViewController.h"
#import "KVCUPhotoCell.h"
#import "KVCUPhoto.h"

@interface KVCUPhotosViewController ()

@end

@implementation KVCUPhotosViewController

@synthesize xmlDocument;
@synthesize photoList;
@synthesize imageDict;
@synthesize photos;
@synthesize refreshControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadPhotos];
    self.photos = [NSMutableArray array];
    self.imageDict = [[NSMutableDictionary alloc] init];

	UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(73, 73)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setSectionInset:UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 5.0f)];
    [flowLayout setMinimumInteritemSpacing:5.0f];
    [flowLayout setMinimumLineSpacing:5.0f];
    
    [self.collectionView setCollectionViewLayout:flowLayout];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f];
    [refreshControl addTarget:self action:@selector(loadPhotos) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // shadowPath, shadowOffset, and rotation is handled by ECSlidingViewController.
    // You just need to set the opacity, radius, and color.
    self.navigationController.view.layer.shadowOpacity = 0.75f;
    self.navigationController.view.layer.shadowRadius = 10.0f;
    self.navigationController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[KVCUMenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    
    self.slidingViewController.underRightViewController = nil;
    
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

- (void)loadPhotos
{
    NSString *xmlPath = [NSString stringWithFormat:@"%@", kKVCUPhotosURL];
    KVCUXMLDocument *newDocument = [[KVCUXMLDocument alloc] initWithDelegate:self];
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] init];
    [loading startAnimating];
    self.navigationItem.titleView = loading;
    self.xmlDocument = newDocument;
    [self.xmlDocument parseRemoteXMLWithPath:xmlPath];
}

- (void)xmlDocumentDelegateParsingFinished:(KVCUXMLDocument *)paramSender
{
    self.navigationItem.titleView = nil;
    self.navigationItem.title = @"Photos";
    [self.photoList removeAllObjects];
    [self.imageDict removeAllObjects];
    [self.photos removeAllObjects];
    [self getXMLContents:self.xmlDocument.rootElement];
    [self.collectionView reloadData];
    [self.refreshControl endRefreshing];
}

- (void)xmlDocumentDelegateParsingFailed:(KVCUXMLDocument *)paramSender withError:(NSError *)paramError
{
    self.navigationItem.titleView = nil;
    self.navigationItem.title = @"Photos";
    [self showNoNetworkAlert];
    NSLog(@"Parsing Failed: %@", paramError);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([self.photoList count] != 0) {
        return [self.photoList count];
    }
    return 28;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    KVCUPhotoCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[KVCUPhotoCell alloc] init];
    }
    KVCUPhoto *photo = [self.photoList objectAtIndex:indexPath.row];
    if ([imageDict objectForKey:indexPath] == nil) {
        cell.image = [cell.image initWithUrl:[NSURL URLWithString:photo.thumbLink] withPlaceholder:@"missingAlbum.png" withDelegate:self withIndex:indexPath];
    } else {
        cell.image.image = [imageDict objectForKey:indexPath];
    }
    
    [cell setFrame:cell.frame];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.wantsFullScreenLayout = NO;
    browser.displayActionButton = NO;
    [browser setInitialPageIndex:indexPath.row];
    [self.navigationController pushViewController:browser animated:YES];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return [self.photos count];
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count)
        return [self.photos objectAtIndex:index];
    return nil;
}

- (void)getXMLContents:(KVCUXMLElement *)paramElement
{
    NSMutableArray *photoArray = [NSMutableArray array];
    
    KVCUPhoto *temp = [[KVCUPhoto alloc] init];
    for(KVCUXMLElement *child in paramElement.children) {
        if ([child.name isEqualToString:@"description"]) {
            temp.description = child.text;
        }
        if ([child.name isEqualToString:@"imageLink"]) {
            temp.imageLink = child.text;
        }
        if ([child.name isEqualToString:@"thumbnail"]) {
            temp.thumbLink = child.text;
            [photoArray addObject:temp];
            MWPhoto *tempPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:temp.imageLink]];
            tempPhoto.caption = temp.description;
            [self.photos addObject:tempPhoto];
            temp = nil;
            temp = [KVCUPhoto alloc];
        }
    }
    self.photoList = photoArray;
}

- (void)imageDidFinishLoading:(KVCULazyLoadImage *)paramSender
{
    if (paramSender.image != nil) {
        [imageDict setObject:paramSender.image forKey:paramSender.indexPath];
    }
}


- (void)showNoNetworkAlert
{
	UIAlertView *baseAlert = [[UIAlertView alloc]
							  initWithTitle:@"No Network" message:@"Could not connect to Radio 1190 at this time."
							  delegate:nil cancelButtonTitle:nil
							  otherButtonTitles:@"OK", nil];
	[baseAlert show];
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

@end
