//
//  KVCUMenuViewController.h
//  KVCUApp
//
//  Created by Ian Finlay on 4/1/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"

@interface KVCUMenuViewController : UIViewController <UITableViewDataSource, UITabBarControllerDelegate>

/// Dictionary for view controller storage
@property (nonatomic, strong) NSMutableDictionary* viewControllerMap;

/// Array holding icon colors
@property (nonatomic, strong) NSMutableArray* isOrange;

@end
