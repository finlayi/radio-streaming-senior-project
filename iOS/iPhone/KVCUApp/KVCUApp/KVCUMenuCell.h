//
//  KVCUMenuCell.h
//  KVCUApp
//
//  Created by Ian Finlay on 4/5/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KVCUMenuCell : UITableViewCell

/// Menu item label
@property (nonatomic, weak) IBOutlet UILabel *menuTitleLabel;

/// Menu icon image
@property (nonatomic, weak) IBOutlet UIImageView *menuImage;

@end
