//
//  KVCUMenuViewController.m
//  KVCUApp
//
//  Created by Ian Finlay on 4/1/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUMenuViewController.h"
#import "KVCUMenuCell.h"

@interface KVCUMenuViewController ()
@property (nonatomic, strong) NSArray *menuItems;
@end

@implementation KVCUMenuViewController
@synthesize menuItems;
@synthesize viewControllerMap;
@synthesize isOrange;

- (void)awakeFromNib
{
    self.menuItems = [NSArray arrayWithObjects:@"Radio", @"Playlist", @"Blog", @"Calendar", @"DJ Schedule", @"Photos", @"Contact", @"About", nil];
}

/// Set up sliding view menu options
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.viewControllerMap = [[NSMutableDictionary alloc] init];
    self.isOrange = [NSMutableArray array];
    [self.slidingViewController setAnchorRightRevealAmount:260.0f];
    self.slidingViewController.underLeftWidthLayout = ECFullWidth;
    
    UIViewController *newViewController = nil;
    newViewController = self.slidingViewController.topViewController;
    [self.viewControllerMap setValue:newViewController forKey:@"RadioView"];
    
    for (int i = 0; i < [self.menuItems count]; i++) {
        [self.isOrange setObject:@NO atIndexedSubscript:i];
    }
    [self.isOrange setObject:@YES atIndexedSubscript:0];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return self.menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"KVCUMenuItemCell";
    KVCUMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[KVCUMenuCell alloc] init];
    }
    BOOL orange = [[self.isOrange objectAtIndex:indexPath.row] boolValue];
    if (orange == YES) {
        cell.menuImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@ImageOrange", [self.menuItems objectAtIndex:indexPath.row]]];
    }
    else
        cell.menuImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@ImageGray", [self.menuItems objectAtIndex:indexPath.row]]];

    
    cell.menuTitleLabel.text = [self.menuItems objectAtIndex:indexPath.row];
    
    return cell;
}

/// Set up new top view and refresh it, store views in a dictionary to be reused
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = [NSString stringWithFormat:@"%@View", [self.menuItems objectAtIndex:indexPath.row]];
        
    UIView *selectionColor = [[UIView alloc] init];
    [selectionColor setBackgroundColor:[UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f]];
    [[tableView cellForRowAtIndexPath:indexPath] setSelectedBackgroundView:selectionColor];
    
    [self.slidingViewController anchorTopViewTo:ECRight animations:nil onComplete:^
    {
        UIViewController *newViewController = nil;
        if ([self.viewControllerMap.allKeys containsObject:identifier])
        {
            // Reuse view controller
            newViewController = [self.viewControllerMap objectForKey:identifier];
        }
        else
        {
            // New view controller
            newViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
            [self.viewControllerMap setValue:newViewController forKey:identifier];
        }
        
        // Get new top view controller (with navigation controllers taken into account)
        UIViewController *newTopViewController = newViewController;
        if ([newViewController isKindOfClass:[UINavigationController class]])
            newTopViewController = ((UINavigationController *) newTopViewController).topViewController;
        
        // Get current top view controllre (with navigation controllers taken into account)
        UIViewController *curTopViewController = self.slidingViewController.topViewController;
        if ([curTopViewController isKindOfClass:[UINavigationController class]])
            curTopViewController = ((UINavigationController *) curTopViewController).topViewController;
        
        // Change if the new and the current controller is not of the same class
        if (![newTopViewController isKindOfClass:[curTopViewController class]])
        {
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newViewController;
            self.slidingViewController.topViewController.view.frame = frame;
        }
        
        // Reset top view
        [self.slidingViewController resetTopView];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        for (int i = 0; i < [self.menuItems count]; i++) {
            [self.isOrange setObject:@NO atIndexedSubscript:i];
        }
        [self.isOrange setObject:@YES atIndexedSubscript:indexPath.row];
        [tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
