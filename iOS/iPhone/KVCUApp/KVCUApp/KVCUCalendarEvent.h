//
//  KVCUCalendarEvent.h
//  KVCUApp
//
//  Created by Ian Finlay on 2/11/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief
 * Class to hold calendar event data
 */
@interface KVCUCalendarEvent : NSObject {
@public
    /**
     * Title of event
     */
    NSString *title;
    
    /**
     * Start date of event
     */
    NSString *startDate;
    
    /**
     * Image corresponding to event
     */
    NSString *image;
}

@property (nonatomic, strong) NSString* title;

@property (nonatomic, strong) NSString* startDate;

@property (nonatomic, strong) NSString* image;

/// Detailed event information
@property (nonatomic, strong) NSString* content;

/// Event end date
@property (nonatomic, strong) NSString* endDate;

/// Venue for the event
@property (nonatomic, strong) NSString* venue;

/// Phone number for the event
@property (nonatomic, strong) NSString* phone;

/// Address of the event
@property (nonatomic, strong) NSString* address;

@end
