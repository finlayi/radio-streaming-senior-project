//
//  KVCUAppDelegate.m
//  KVCUApp
//
//  Created by Jackie Myrose on 11/23/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import "KVCUAppDelegate.h"

@implementation KVCUAppDelegate
@synthesize radioView;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];

    /// NavBar Appearence
    UIImage *backButtonImage = [[UIImage imageNamed:@"buttonBack.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13, 0, 6)];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    UIImage *barButtonImage = [[UIImage imageNamed:@"buttonNormal.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 6, 0, 6)];
    [[UIBarButtonItem appearance] setBackgroundImage:barButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navBarBackground.png"] forBarMetrics:UIBarMetricsDefault];
    //[[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Novecento wide" size:16], UITextAttributeFont, nil]];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)event
{
	//NSLog(@"UIEventTypeRemoteControl: %d - %d", event.type, event.subtype);
	if (event.subtype == UIEventSubtypeRemoteControlTogglePlayPause) {
		//NSLog(@"UIEventSubtypeRemoteControlTogglePlayPause");
		[self.radioView playPause:nil];
	}
	if (event.subtype == UIEventSubtypeRemoteControlPlay) {
		//NSLog(@"UIEventSubtypeRemoteControlPlay");
		[self.radioView playPause:nil];
	}
	if (event.subtype == UIEventSubtypeRemoteControlPause) {
		//NSLog(@"UIEventSubtypeRemoteControlPause");
		[self.radioView playPause:nil];
	}
	if (event.subtype == UIEventSubtypeRemoteControlStop) {
		//NSLog(@"UIEventSubtypeRemoteControlStop");
		[self.radioView playPause:nil];
	}
}

@end
