//
//  KVCUFullCalendarViewController.h
//  KVCUApp
//
//  Created by Ian Finlay on 4/3/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVCUXMLDocument.h"
#import "ECSlidingViewController.h"
#import "TKCalendarMonthTableViewController.h"

@interface KVCUFullCalendarViewController : TKCalendarMonthTableViewController<KVCUXMLDocumentDelegate>
{
@public
    /**
     * The XML document to pull calendar information from
     */
    KVCUXMLDocument *xmlDocument;
}
/// Calendar to display
@property (nonatomic, strong) TKCalendarMonthView *calendar;

/// Data for calendar view (Yes/No) for events
@property (nonatomic,strong) NSMutableArray *dataArray;

/// Data of which events occur each day 
@property (nonatomic,strong) NSMutableDictionary *dataDictionary;

@property (nonatomic, strong) KVCUXMLDocument *xmlDocument;

/// Complete dictionary of shows for the month
@property (nonatomic, strong) NSMutableDictionary *showList;

/// Current date
@property (nonatomic, strong) NSDate *currentDate;

/// Current month
@property (nonatomic) NSInteger currentMonth;

/// Current year
@property (nonatomic) NSInteger currentYear;

/// Current start date
@property (nonatomic, strong) NSDate *currentStartDate;

- (IBAction)revealMenu:(id)sender;

@end
