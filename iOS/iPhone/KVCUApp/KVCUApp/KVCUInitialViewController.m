//
//  KVCUInitialViewController.m
//  KVCUApp
//
//  Created by Ian Finlay on 4/1/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUInitialViewController.h"

@interface KVCUInitialViewController ()

@end

@implementation KVCUInitialViewController

/// Set up story board for views
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIStoryboard *storyboard;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    } else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        storyboard = [UIStoryboard storyboardWithName:@"iPad" bundle:nil];
    }
    
    self.topViewController = [storyboard instantiateViewControllerWithIdentifier:@"RadioView"];
}

@end
