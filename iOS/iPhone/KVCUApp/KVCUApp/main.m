//
//  main.m
//  KVCUApp
//
//  Created by Jackie Myrose on 11/23/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KVCUAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KVCUAppDelegate class]));
    }
}
