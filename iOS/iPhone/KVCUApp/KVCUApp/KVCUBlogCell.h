//
//  KVCUBlogCell.h
//  KVCUApp
//
//  Created by Ian Finlay on 2/12/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVCULazyLoadImage.h"

/**
 * @brief
 * The custom table view cell for each blog post
 */
@interface KVCUBlogCell : UITableViewCell

/// The title label for the post
@property (nonatomic, weak) IBOutlet UILabel* titleLabel;

/// The content label for the post
@property (nonatomic, weak) IBOutlet UILabel* contentLabel;

/// The image for the post
@property (nonatomic, strong) IBOutlet KVCULazyLoadImage* image;

/// The extra information for the post
@property (nonatomic, weak) IBOutlet UILabel* readMore;


@end
