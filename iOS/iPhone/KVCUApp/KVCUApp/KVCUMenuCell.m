//
//  KVCUMenuCell.m
//  KVCUApp
//
//  Created by Ian Finlay on 4/5/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUMenuCell.h"

@implementation KVCUMenuCell

@synthesize menuImage;
@synthesize menuTitleLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
