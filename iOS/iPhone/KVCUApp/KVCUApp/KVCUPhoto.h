//
//  KVCUPhoto.h
//  KVCUApp
//
//  Created by Ian Finlay on 4/2/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KVCUPhoto : NSObject

/// Link to load image data from
@property (nonatomic, strong) NSString* imageLink;

/// Link for thumbnail image
@property (nonatomic, strong) NSString* thumbLink;

/// Description for image
@property (nonatomic, strong) NSString* description;

@end
