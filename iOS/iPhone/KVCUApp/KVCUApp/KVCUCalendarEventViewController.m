//
//  KVCUCalendarEventViewController.m
//  KVCUApp
//
//  Created by Ian Finlay on 2/11/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUCalendarEventViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface KVCUCalendarEventViewController ()

@end

@implementation KVCUCalendarEventViewController

@synthesize bannerImage;
@synthesize imageURL;
@synthesize titleLabel;
@synthesize contentLabel;
@synthesize startLabel;
@synthesize endLabel;
@synthesize venueLabel;
@synthesize phoneLabel;
@synthesize addressLabel;
@synthesize event;
@synthesize venueStaticLabel;
@synthesize phoneStaticLabel;
@synthesize addressStaticLabel;
@synthesize startStaticLabel;
@synthesize endStaticLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.bannerImage = [self.bannerImage initWithUrl:[NSURL URLWithString:imageURL] withPlaceholder:@"blogPostPlaceholder.png"];
    
    titleLabel.font = [UIFont fontWithName:@"Novecento wide" size:24];
    titleLabel.text = event.title;
    contentLabel.text = event.content;
    startLabel.text = event.startDate;
    endLabel.text = event.endDate;
    if (event.venue != nil) {
        venueLabel.text = event.venue;
    } else {
        venueStaticLabel.text = nil;
        venueLabel.text = nil;
    }
    if (event.address != nil) {
        addressLabel.text = event.address;
    } else {
        addressStaticLabel.text = nil;
        addressLabel.text = nil;
    }
    if (event.phone != nil) {
        phoneLabel.text = event.phone;
    } else {
        phoneStaticLabel.text = nil;
        phoneLabel.text = nil;
    }
    
    venueStaticLabel.font = [UIFont fontWithName:@"Novecento wide" size:14];
    startStaticLabel.font = [UIFont fontWithName:@"Novecento wide" size:14];
    endStaticLabel.font = [UIFont fontWithName:@"Novecento wide" size:14];
    phoneStaticLabel.font = [UIFont fontWithName:@"Novecento wide" size:14];
    
    [titleLabel.layer setShadowRadius:7];
    [titleLabel.layer setMasksToBounds:NO];
    [titleLabel.layer setShadowOpacity:1];
    [titleLabel.layer setShadowColor:[UIColor blackColor].CGColor];
    [titleLabel.layer setShadowOffset:CGSizeMake(0, 0)];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
