//
//  KVCUInitialViewController.h
//  KVCUApp
//
//  Created by Ian Finlay on 4/1/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "ECSlidingViewController.h"

@interface KVCUInitialViewController : ECSlidingViewController

@end
