//
//  KVCUPlaylistDetailSimilarSong.m
//  KVCUApp
//
//  Created by Ian Finlay on 12/8/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import "KVCUPlaylistDetailSimilarSong.h"

@implementation KVCUPlaylistDetailSimilarSong

@synthesize similarArtistName;
@synthesize similarSongName;
@synthesize similarArtName;

- (id)init
{
    self = [super init];
    return self;
}

@end
