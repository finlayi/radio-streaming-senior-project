//
//  KVCUBlogPostViewController.m
//  KVCUApp
//
//  Created by Ian Finlay on 2/12/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUBlogPostViewController.h"

@interface KVCUBlogPostViewController ()

@end

@implementation KVCUBlogPostViewController

@synthesize blogURL;
@synthesize webView;
@synthesize loading;
@synthesize blogTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self.loading startAnimating];
    [self.webView setAlpha:0.0];
    [self.webView setDelegate:self];
    NSURL *url = [NSURL URLWithString:self.blogURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self showNoNetworkAlert];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.webView stringByEvaluatingJavaScriptFromString:@"$('#header-container').css('display', 'none');$('.socialbox-container').css('display', 'none');$('.masonry').css('display', 'none');$('.single-image img').attr('style', 'height:100% !important; width:100% !important; margin:0px 0px 0px 0px');$('.navigation').css('display', 'none');"];
    
    [NSTimer scheduledTimerWithTimeInterval:1.5
                                     target:self
                                   selector:@selector(showWebView)
                                   userInfo:nil
                                    repeats:NO];
    
}

- (void)showWebView
{
    [self.loading stopAnimating];
    [self.loading setHidden:YES];
    
    [UIView animateWithDuration:0.2 animations:^() {
        self.webView.alpha = 1.0;
    }];
}

- (IBAction)shareButtonPressed:(id)sender {
    NSString* text = [NSString stringWithFormat:@"%@ - found on the Radio1190 iPhone App!", self.blogTitle];
    NSURL* url = [NSURL URLWithString:self.blogURL];
    NSArray* activityItems = [NSArray arrayWithObjects:text, url, nil];
    
    UIActivityViewController *avc = [[UIActivityViewController alloc] initWithActivityItems: activityItems applicationActivities:nil];
    avc.excludedActivityTypes = [NSArray arrayWithObjects:UIActivityTypeAssignToContact, UIActivityTypeMail, UIActivityTypePrint, UIActivityTypeSaveToCameraRoll, UIActivityTypeMessage, UIActivityTypeCopyToPasteboard, nil];
    [self presentViewController:avc animated:YES completion:nil];
    
}

- (void)showNoNetworkAlert
{
	UIAlertView *baseAlert = [[UIAlertView alloc]
							  initWithTitle:@"No Network" message:@"Could not connect to Radio 1190 at this time."
							  delegate:nil cancelButtonTitle:nil
							  otherButtonTitles:@"OK", nil];
	[baseAlert show];
}

@end
