//
//  KVCUDJScheduleCell.m
//  KVCUApp
//
//  Created by Ian Finlay on 2/26/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUDJScheduleCell.h"

@implementation KVCUDJScheduleCell

@synthesize startTime;
@synthesize endTime;
@synthesize showDesc;
@synthesize showName;

@end
