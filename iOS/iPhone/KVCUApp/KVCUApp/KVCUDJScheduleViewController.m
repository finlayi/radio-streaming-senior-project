//
//  KVCUDJScheduleViewController.m
//  KVCUApp
//
//  Created by Ian Finlay on 4/2/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUDJScheduleViewController.h"
#import "KVCUMenuViewController.h"

@interface KVCUDJScheduleViewController ()

@end

@implementation KVCUDJScheduleViewController

@synthesize djList;
@synthesize xmlDocument;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self. djList = [[NSMutableDictionary alloc] init];
    [self loadShows];
    

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // shadowPath, shadowOffset, and rotation is handled by ECSlidingViewController.
    // You just need to set the opacity, radius, and color.
    self.navigationController.view.layer.shadowOpacity = 0.75f;
    self.navigationController.view.layer.shadowRadius = 10.0f;
    self.navigationController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[KVCUMenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    
    self.slidingViewController.underRightViewController = nil;
    
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Load DJ shows from path
- (void)loadShows
{
    NSString *xmlPath = [NSString stringWithFormat:@"%@", kKVCUDJScheduleURL];
    KVCUXMLDocument *newDocument = [[KVCUXMLDocument alloc] initWithDelegate:self];
    self.xmlDocument = newDocument;
    [self.xmlDocument parseRemoteXMLWithPath:xmlPath];
}
#pragma mark - Table view data source

// 7 sections for 7 days
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if ([djList count] != 0) {
        return 7;
    }
    return 7;
}

// Rows in section set equal to the number of shows each day
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ([djList count] == 0) {
        return 1;
    }
    if (section == 0) {
        return [[djList objectForKey:@"Monday"] count];
    }
    if (section == 1) {
        return [[djList objectForKey:@"Tuesday"] count];
    }
    if (section == 2) {
        return [[djList objectForKey:@"Wednesday"] count];
    }
    if (section == 3) {
        return [[djList objectForKey:@"Thursday"] count];
    }
    if (section == 4) {
        return [[djList objectForKey:@"Friday"] count];
    }
    if (section == 5) {
        return [[djList objectForKey:@"Saturday"] count];
    }
    if (section == 6) {
        return [[djList objectForKey:@"Sunday"] count];
    }
    return 0;
}

// Create cells for section
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *djScheduleIdentifier = @"djSchedIdentifier";
    static NSString *loadMoreIdentifier = @"loadMoreIdentifier";
    KVCUDJScheduleCell *djCell = [self.tableView dequeueReusableCellWithIdentifier:djScheduleIdentifier];
    UITableViewCell *loadMoreCell = [self.tableView dequeueReusableCellWithIdentifier:loadMoreIdentifier];
    
    if ([self.djList count] == 0) {
        loadMoreCell.textLabel.font = [UIFont fontWithName:@"Novecento wide" size:20];
        loadMoreCell.textLabel.text = kKVCULoadingText;
        return loadMoreCell;
    }
    
    KVCUDJSchedule *show = [[KVCUDJSchedule alloc] init];
    if (indexPath.section == 0) {
        NSMutableArray *dayArray = [djList objectForKey:@"Monday"];
        show = [dayArray objectAtIndex:indexPath.row];
    }
    if (indexPath.section == 1) {
        NSMutableArray *dayArray = [djList objectForKey:@"Tuesday"];
        show = [dayArray objectAtIndex:indexPath.row];
    }
    if (indexPath.section == 2) {
        NSMutableArray *dayArray = [djList objectForKey:@"Wednesday"];
        show = [dayArray objectAtIndex:indexPath.row];
    }
    if (indexPath.section == 3) {
        NSMutableArray *dayArray = [djList objectForKey:@"Thursday"];
        show = [dayArray objectAtIndex:indexPath.row];
    }
    if (indexPath.section == 4) {
        NSMutableArray *dayArray = [djList objectForKey:@"Friday"];
        show = [dayArray objectAtIndex:indexPath.row];
    }
    if (indexPath.section == 5) {
        NSMutableArray *dayArray = [djList objectForKey:@"Saturday"];
        show = [dayArray objectAtIndex:indexPath.row];
    }
    if (indexPath.section == 6) {
        NSMutableArray *dayArray = [djList objectForKey:@"Sunday"];
        show = [dayArray objectAtIndex:indexPath.row];
    }
    
    djCell.showName.text = show.showName;
    djCell.showDesc.text = show.showDesc;
    djCell.startTime.text = show.startTime;
    djCell.endTime.text = show.endTime;
    djCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return djCell;
}


// Return headers for sections
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([djList count] != 0) {
        if (section == 0) {
            return @"Monday";
        }
        if (section == 1) {
            return @"Tuesday";
        }
        if (section == 2) {
            return @"Wednesday";
        }
        if (section == 3) {
            return @"Thursday";
        }
        if (section == 4) {
            return @"Friday";
        }
        if (section == 5) {
            return @"Saturday";
        }
        if (section == 6) {
            return @"Sunday";
        }
    }
    return @"";
}

// Return header views for section
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionName = nil;
    
    switch (section) {
        case 0:
            sectionName = [NSString stringWithFormat:@" Monday"];
            break;
        case 1:
            sectionName = [NSString stringWithFormat:@" Tuesday"];
            break;
        case 2:
            sectionName = [NSString stringWithFormat:@" Wednesday"];
            break;
        case 3:
            sectionName = [NSString stringWithFormat:@" Thursday"];
            break;
        case 4:
            sectionName = [NSString stringWithFormat:@" Friday"];
            break;
        case 5:
            sectionName = [NSString stringWithFormat:@" Saturday"];
            break;
        case 6:
            sectionName = [NSString stringWithFormat:@" Sunday"];
            break;
    }
    
    UILabel *sectionHeader = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 45)];
    sectionHeader.backgroundColor = [UIColor colorWithRed:(253.0f/255.0f) green:(84.0f/255.0f) blue:(1.0f/255.0f) alpha:1.0f];
    sectionHeader.layer.borderWidth = 0.5;
    sectionHeader.layer.borderColor = [UIColor blackColor].CGColor;
    sectionHeader.font = [UIFont fontWithName:@"Novecento wide" size:18];
    sectionHeader.textColor = [UIColor whiteColor];
    sectionHeader.text = sectionName;
    return sectionHeader;
}

// Return height for cells
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56;
}

// Delegate methods for XMLDocument
- (void)xmlDocumentDelegateParsingFinished:(KVCUXMLDocument *)paramSender
{
    [self getXMLContents: self.xmlDocument.rootElement];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 7)] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)xmlDocumentDelegateParsingFailed:(KVCUXMLDocument *)paramSender withError:(NSError *)paramError
{
    [self showNoNetworkAlert];
    NSLog(@"Parsing Failed: %@", paramError);
}

- (void)getXMLContents:(KVCUXMLElement *)paramElement
{
    NSMutableArray *showArray = [[NSMutableArray alloc] init];
    KVCUDJSchedule *temp = [[KVCUDJSchedule alloc] init];
    NSString *currentDay = nil;
    NSString *nextDay = @"Monday";
    
    for(KVCUXMLElement *child in paramElement.children) {
        if ([child.name isEqualToString:@"dayName"] && child.text != nil) {
            currentDay = child.text;
            if (![currentDay isEqualToString:nextDay]) {
                [djList setObject:showArray forKey:nextDay];
                nextDay = currentDay;
                showArray = nil;
                showArray = [[NSMutableArray alloc] init];
            }
        }
        if ([child.name isEqualToString:@"startTime"]) {
            temp.startTime = child.text;
        }
        if ([child.name isEqualToString:@"endTime"]) {
            temp.endTime = child.text;
        }
        if ([child.name isEqualToString:@"showName"]) {
            temp.showName = child.text;
        }
        if ([child.name isEqualToString:@"showDesc"]) {
            temp.showDesc = child.text;
        }
        if ([child.name isEqualToString:@"showSite"]) {
            temp.showSite = child.text;
            [showArray addObject:temp];
            temp = nil;
            temp = [KVCUDJSchedule alloc];
        }
    }
    [djList setObject:showArray forKey:nextDay];
}

// Alert for no network connection
- (void)showNoNetworkAlert
{
	UIAlertView *baseAlert = [[UIAlertView alloc]
							  initWithTitle:@"No Network" message:@"Could not get calendar events at this time."
							  delegate:nil cancelButtonTitle:nil
							  otherButtonTitles:@"OK", nil];
	[baseAlert show];
}

// Reveal side menu
- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

@end
