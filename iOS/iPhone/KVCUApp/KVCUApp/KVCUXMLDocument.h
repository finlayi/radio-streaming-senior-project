//
//  KVCUXMLDocument.h
//  KVCUApp
//
//  Created by Ian Finlay on 12/4/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//
//  This class returns a xml tree object which can be parsed recursively

#import <Foundation/Foundation.h>
#import "KVCUXMLElement.h"

@class KVCUXMLDocument;

/**
 * @brief
 * Protocol for parsing XML documents
 */
@protocol KVCUXMLDocumentDelegate <NSObject>

@required

/**
 * Callback when a document has finished being parsed
 *
 * @param paramSender   the document which was parsed
 */
- (void)xmlDocumentDelegateParsingFinished:(KVCUXMLDocument *)paramSender;

/**
 * Callback when a document has failed to be parsed
 *
 * @param paramSender   the documnet which was attempted to be parsed
 * @param paramError    the error which caused the parsing failure
 */
- (void)xmlDocumentDelegateParsingFailed:(KVCUXMLDocument *)paramSender withError:(NSError *)paramError;

@end

/**
 * @brief
 * XML document class, used for parsing
 */
@interface KVCUXMLDocument : NSObject <NSXMLParserDelegate> {
    @public
    /**
     * Root element for parsing
     */
    KVCUXMLElement *rootElement;
    
    /**
     * Delegate for parsing callbacks
     */
    __weak id<KVCUXMLDocumentDelegate> delegate;
    
    @private
    /**
     * The XML parser (built-in)
     */
    __weak NSXMLParser *xmlParser;
    
    /**
     * The XML element currently being parsed
     */
    KVCUXMLElement *currentElement;
    
    /**
     * The URL connection used for pulling the XML from the web
     */
    NSURLConnection *connection;
    
    /**
     * The data recieved from the URL connection
     */
    NSMutableData *connectionData;
    
    /**
     * Boolean indicating if a parsing error has occured
     */
    BOOL parsingErrorHasHappened;
}

@property (nonatomic, strong) KVCUXMLElement *rootElement;
@property (nonatomic, weak) id<KVCUXMLDocumentDelegate> delegate;

@property (nonatomic, weak) NSXMLParser *xmlParser;
@property (nonatomic, strong) KVCUXMLElement *currentElement;
@property (nonatomic, strong) NSURLConnection *connection;
@property (nonatomic, strong) NSMutableData *connectionData;
@property (nonatomic, assign) BOOL parsingErrorHasHappened;


/**
 * Initialization method
 *
 * @param paramDelegate the delegate for document parsing callbacks
 */
- (id)initWithDelegate:(id<KVCUXMLDocumentDelegate>)paramDelegate;

/**
 * Method to parse local XML
 * NOT IMPLEMENTED
 *
 * @param paramLocalXMLPath URL path of local XML to parse
 *
 * @return whether or not the XML was parsed successfully
 *          (always returns NO currently)
 */
- (BOOL)parseLocalXMLWithPath:(NSString *)paramLocalXMLPath;

/**
 * Method to parse remote XML
 *
 * @param paramRemoteXMLURL URL path of remote XML to parse
 *
 * @return whether or not the XML was parsed successfully
 */
- (BOOL)parseRemoteXMLWithPath:(NSString *)paramRemoteXMLURL;



@end
