//
//  KVCUFullCalendarCell.h
//  KVCUApp
//
//  Created by Ian Finlay on 4/3/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KVCUFullCalendarCell : UITableViewCell

/// Title label for event
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

/// Date label for event
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;

@end
