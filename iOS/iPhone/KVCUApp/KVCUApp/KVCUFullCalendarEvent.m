//
//  KVCUFullCalendarEvent.m
//  KVCUApp
//
//  Created by Ian Finlay on 4/3/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUFullCalendarEvent.h"

@implementation KVCUFullCalendarEvent

@synthesize title;
@synthesize date;
@synthesize link;

@end
