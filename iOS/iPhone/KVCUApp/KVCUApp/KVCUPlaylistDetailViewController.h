//
//  KVCUPlaylistDetailViewController.h
//  KVCUApp
//
//  Created by Ian Finlay on 12/6/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVCUPlaylistDetailTableView.h"

/**
 * @brief
 * Playlist Detail screen view controller.
 * Downloads similar song information and controls custom table views.
 */
@interface KVCUPlaylistDetailViewController : UIViewController<KVCUPlaylistDetailTableViewDelegate>

/// Label for song title
@property (nonatomic, weak) IBOutlet UILabel* songLabel;

/// Label for artist name
@property (nonatomic, weak) IBOutlet UILabel* artistLabel;

/// Label for album name
@property (nonatomic, weak) IBOutlet UILabel* albumLabel;

/// Label for song title
@property (nonatomic, weak) IBOutlet UILabel* staticSongLabel;

/// Label for artist name
@property (nonatomic, weak) IBOutlet UILabel* staticArtistLabel;

/// Label for album name
@property (nonatomic, weak) IBOutlet UILabel* staticAlbumLabel;

/// The song name
@property (nonatomic, strong) NSString *songName;

/// the artist name
@property (nonatomic, strong) NSString *artistName;

/// The album name
@property (nonatomic, strong) NSString *albumName;

/// The album cover image
@property (nonatomic, weak) IBOutlet UIImageView* albumArt;

/// iTunes link to song
@property (nonatomic, strong) NSString* buyButtonLink;

/// iTunes buy button
@property (nonatomic, weak) IBOutlet UIButton *buyButton;

/// Table view for similar songs
@property (nonatomic, weak) IBOutlet KVCUPlaylistDetailTableView *detailTableView;

/// Loading similar songs indicator
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loadingTable;

/// Label for similar songs table
@property (nonatomic, weak) IBOutlet UILabel* similarSongsLabel;

/// Data pulled from URL connection containing detailed song information
@property (nonatomic, strong) NSMutableData* connectionData;

/// URL connection used to pull song information
@property (nonatomic, strong) NSURLConnection* connection;

/// Button to share song through social networks
@property (nonatomic, weak) IBOutlet UIBarButtonItem* shareButton;

/// ImageView used to create white background
@property (nonatomic, weak) IBOutlet UIImageView *whiteBackground;

@end
