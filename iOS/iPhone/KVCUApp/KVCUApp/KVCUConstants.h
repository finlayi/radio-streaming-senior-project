//
//  KVCUConstants.h
//  KVCUApp
//
//  Created by Jackie Myrose on 3/3/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief
 * Class for storing any strings used in the app
 */
@interface KVCUConstants : NSObject

/// URL for pulling blog posts
extern NSString *const kKVCUBlogPostsURL;

/// URL for the donation page
extern NSString *const kKVCUDonateURL;

/// Radio 1190 facebook page URL
extern NSString *const kKVCUFacebookURL;

/// Radio 1190 twitter page URL
extern NSString *const kKVCUTwitterURL;

/// Radio 1190 instagram URL
extern NSString *const kKVCUInstagramURL;

/// URL for pulling current song/artist
extern NSString *const kKVCUCurrentPlaylistURL;

/// URL for the streaming mp3 file
extern NSString *const kKVCURadioURL;

/// URL for pulling current and past playlists
extern NSString *const kKVCUPlaylistURL;

/// URL for pulling the DJ schedule
extern NSString *const kKVCUDJScheduleURL;

/// URL for pulling upcoming events
extern NSString *const kKVCUEventsURL;

/// URL for pulling photos
extern NSString *const kKVCUPhotosURL;



/// Default song text for Radio view
extern NSString *const kKVCUDefaultSongText;

/// Default artist text for Radio view
extern NSString *const kKVCUDefaultArtistText;

/// Default loading text
extern NSString *const kKVCULoadingText;

extern NSString *const kKVCUFullCalenderURL;


@end
