//
//  KVCUBlogPostViewController.h
//  KVCUApp
//
//  Created by Ian Finlay on 2/12/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * @brief
 * The individual blog post view controller.
 * Sets up the proper css and layout for the blog post.
 */
@interface KVCUBlogPostViewController : UIViewController<UIWebViewDelegate>

/// The webview for the post
@property (nonatomic, strong) IBOutlet UIWebView* webView;

/// The URL the blog post is coming from
@property (nonatomic, strong) NSString* blogURL;

/// Blog post loading indicator
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView* loading;

@property (nonatomic, strong) NSString* blogTitle;


@end
