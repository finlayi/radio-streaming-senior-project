//
//  KVCUPlaylistDetailTableView.m
//  KVCUApp
//
//  Created by Ian Finlay on 12/8/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import "KVCUPlaylistDetailTableView.h"
#import "KVCUPlaylistDetailTableViewCell.h"
#import "KVCUXMLElement.h"
#import "KVCUPlaylistDetailSimilarSong.h"
#import "KVCULazyLoadImage.h"
#import <QuartzCore/QuartzCore.h>


@implementation KVCUPlaylistDetailTableView

@synthesize artist;
@synthesize song;
@synthesize album;
@synthesize detailTable;
@synthesize xmlDocument;
@synthesize similarSongs;
@synthesize delegate;


- (id)initWithArtist:(NSString *)artistName song:(NSString *)songName album:(NSString *)albumName
{
    self = [super init];
    if (self) {
        self.artist = artistName;
        self.song = songName;
        self.album = albumName;
        detailTable = [[UITableView alloc] initWithFrame:CGRectZero];
        self.detailTable.delegate = self;
        self.detailTable.dataSource = self;
        self.delegate = nil;
    }
    return self;
}

- (void)getSimilarSongswithDelegate:(id<KVCUPlaylistDetailTableViewDelegate>)paramDelegate;
{
    NSString *urlToAppend = @"http://ws.audioscrobbler.com/2.0/?method=track.getsimilar&artist=ARTISTNAMEHERE&track=SONGNAMEHERE&api_key=4c6ae04a8d82889298b80ac9e1fb1328";
    
    urlToAppend = [urlToAppend stringByReplacingOccurrencesOfString:@"SONGNAMEHERE" withString:self.song];
    urlToAppend = [urlToAppend stringByReplacingOccurrencesOfString:@"ARTISTNAMEHERE" withString:self.artist];
    
    self.delegate = paramDelegate;
    KVCUXMLDocument *newDocument = [[KVCUXMLDocument alloc] initWithDelegate:self];
    self.xmlDocument = newDocument;
    [self.xmlDocument parseRemoteXMLWithPath:urlToAppend];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.similarSongs count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"playlistDetailIdentifier";
    KVCUPlaylistDetailTableViewCell *cell = [self.detailTable dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[KVCUPlaylistDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    KVCUPlaylistDetailSimilarSong *thisSong = [self.similarSongs objectAtIndex:indexPath.row];
        
    if ([self.similarSongs count] != 0) {
        cell.artistLabel.text = thisSong.similarArtistName;
        cell.songLabel.text = thisSong.similarSongName;
        cell.albumImage = [cell.albumImage initWithUrl:[NSURL URLWithString:thisSong.similarArtName] withPlaceholder:@"similarSongsPlaceholder.png"];
    }
    
    UIView *blackBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 77)];
    [blackBackground setBackgroundColor:[UIColor blackColor]];
    [cell.cellBackground addSubview:blackBackground];
    [cell.cellBackground.layer setCornerRadius:4];
    [cell.cellBackground setClipsToBounds:YES];
    
    return cell;
}

- (void)xmlDocumentDelegateParsingFinished:(KVCUXMLDocument *)paramSender
{
    [self getXMLContents:self.xmlDocument.rootElement];
    [self.detailTable reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 1)] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)xmlDocumentDelegateParsingFailed:(KVCUXMLDocument *)paramSender withError:(NSError *)paramError
{
    [self.delegate KVCUPlaylistDetailTableViewLoadFailed:self error:paramError];
}


//  Last.FM API to get similar songs (looks gross but it isn't really)
- (void)getXMLContents:(KVCUXMLElement *)paramElement
{
    NSMutableArray *similarSongArray = [NSMutableArray array];
    
    KVCUPlaylistDetailSimilarSong *similarSong = [[KVCUPlaylistDetailSimilarSong alloc] init];
    int count = 0;
    for(KVCUXMLElement *child in paramElement.children) {
        
        if ([child.name isEqualToString:@"similartracks"]) {
            for (KVCUXMLElement *similarTracks in child.children) {
                if (count < 10) {
                    if ([similarTracks.name isEqualToString:@"track"]) {
                        for (KVCUXMLElement *track in similarTracks.children) {
                            if ([track.name isEqualToString:@"name"]) {
                                similarSong.similarSongName = track.text;
                            }
                            if ([track.name isEqualToString:@"artist"]) {
                                for (KVCUXMLElement *artistChild in track.children) {
                                    if ([artistChild.name isEqualToString:@"name"]) {
                                        similarSong.similarArtistName = artistChild.text;
                                    }
                                }
                            }
                            if ([track.name isEqualToString:@"image"]) {
                                if ([[track.attributes valueForKey:@"size"] isEqualToString:@"medium"]) {
                                    similarSong.similarArtName = track.text;
                                    [similarSongArray addObject:similarSong];
                                    similarSong = nil;
                                    similarSong = [[KVCUPlaylistDetailSimilarSong alloc] init];
                                    count++;
                                }
                            }
                        }
                    }
                } else {
                    break;
                }
            }
        }
        
    }
    self.similarSongs = similarSongArray;
    [self.delegate KVCUPlaylistDetailTableViewDidLoad:self withCount:[self.similarSongs count]];
}


@end
