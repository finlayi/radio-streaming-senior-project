//
//  KVCUCalendarEventViewController.h
//  KVCUApp
//
//  Created by Ian Finlay on 2/11/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVCULazyLoadImage.h"
#import "KVCUCalendarEvent.h"

/**
 * @brief
 * Event detail screen view controller.
 */
@interface KVCUCalendarEventViewController : UIViewController

/// Image for event
@property (nonatomic, strong) IBOutlet KVCULazyLoadImage* bannerImage;

/// URL to pull event image from
@property (nonatomic, strong) NSString* imageURL;

/// Label for title of event
@property (nonatomic, weak) IBOutlet UILabel* titleLabel;

/// Label for event detail text
@property (nonatomic, weak) IBOutlet UILabel* contentLabel;

/// Label for start date and time of event
@property (nonatomic, weak) IBOutlet UILabel* startLabel;

/// Label for end date and time of event
@property (nonatomic, weak) IBOutlet UILabel* endLabel;

/// Label for name of venue
@property (nonatomic, weak) IBOutlet UILabel* venueLabel;

/// Label for venue phone number
@property (nonatomic, weak) IBOutlet UILabel* phoneLabel;

/// Label for address of event
@property (nonatomic, weak) IBOutlet UILabel* addressLabel;

/// Static label for name of venue
@property (nonatomic, weak) IBOutlet UILabel* venueStaticLabel;

/// Static label for venue phone number
@property (nonatomic, weak) IBOutlet UILabel* phoneStaticLabel;

/// Static label for venue address
@property (nonatomic, weak) IBOutlet UILabel* addressStaticLabel;

/// Static label for start date of the event
@property (nonatomic, weak) IBOutlet UILabel* startStaticLabel;

/// Static label for end date of the event
@property (nonatomic, weak) IBOutlet UILabel* endStaticLabel;



/// Event for which this information is being shown
@property (nonatomic, strong) KVCUCalendarEvent* event;

@end
