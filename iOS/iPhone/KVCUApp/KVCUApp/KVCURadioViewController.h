//
//  KVCUFirstViewController.h
//  KVCUApp
//
//  Created by Jackie Myrose on 11/23/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import "KVCUXMLDocument.h"
#import "ECSlidingViewController.h"
#import "KVCUMenuViewController.h"

/**
 * @brief
 * The Radio screen view controller.
 * (The startup screen)
 */
@interface KVCURadioViewController : UIViewController<KVCUXMLDocumentDelegate>
{
    @public
    /**
     * The XML document to parse to get currently playing song/artist
     */
    KVCUXMLDocument *xmlDocument;
}

/// The player for actually playing the music file
@property (strong, nonatomic) AVPlayer *player;

@property (nonatomic, strong) KVCUXMLDocument *xmlDocument;

/// The current song text view
@property (nonatomic, weak) IBOutlet UITextView *song;

/// The current artist text view
@property (nonatomic, weak) IBOutlet UITextView *artist;

/// The play/pause button
@property (nonatomic, weak) IBOutlet UIButton *playPauseButton;

/// The volume manager
@property (nonatomic, weak) IBOutlet MPVolumeView *volumeManager;

/// Navigation Bar
@property (nonatomic, weak) IBOutlet UINavigationBar *navBar;

- (IBAction)revealMenu:(id)sender;

- (IBAction)playPause:(id)sender;

@end
