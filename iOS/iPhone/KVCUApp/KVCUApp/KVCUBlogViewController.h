//
//  KVCUBlogViewController.h
//  KVCUApp
//
//  Created by Ian Finlay on 2/12/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#include "KVCUXMLDocument.h"
#include "KVCULazyLoadImage.h"
#include "ECSlidingViewController.h"
#include "KVCUMenuViewController.h"

/**
 * @brief
 * The Blog screen view controller.
 * Loads posts and configures full table view.
 */
@interface KVCUBlogViewController : UITableViewController<KVCUXMLDocumentDelegate, KVCULazyLoadImageDelegate>
{
    @public
    /**
     * XML document parsed to get blog posts
     */
    KVCUXMLDocument *xmlDocument;
}

@property (nonatomic, strong) KVCUXMLDocument *xmlDocument;

/// The table view to push posts to
@property (nonatomic, strong) IBOutlet UITableView *tableView;

/// The list of blog posts
@property (nonatomic, strong) NSMutableArray *postList;

/// The current page of loaded posts (starts at 1)
@property (nonatomic, assign) NSInteger currentPage;

/// If the user has pressed the refresh button (resets to NO aftrer refresh)
@property (nonatomic, assign) BOOL refresh;

/// Dictionary of images for all the blog posts
@property (nonatomic, strong) NSMutableDictionary* imageDict;

/// Variable that represents first load for animations
@property (nonatomic, assign) BOOL firstLoad;

- (IBAction)revealMenu:(id)sender;

@end
