//
//  KVCUDJSchedule.h
//  KVCUApp
//
//  Created by Ian Finlay on 2/26/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief
 * DJ schedule table
 */
@interface KVCUDJSchedule : NSObject

/// Start time of DJ
@property (nonatomic, strong) NSString *startTime;

/// End time of DJ
@property (nonatomic, strong) NSString *endTime;

/// Name of DJ show
@property (nonatomic, strong) NSString *showName;

/// Show description
@property (nonatomic, strong) NSString *showDesc;

/// Website of show
@property (nonatomic, strong) NSString *showSite;

@end
