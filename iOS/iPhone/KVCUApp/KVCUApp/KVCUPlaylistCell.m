//
//  KVCUPlaylistCell.m
//  KVCUApp
//
//  Created by Ian Finlay on 12/5/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import "KVCUPlaylistCell.h"

@implementation KVCUPlaylistCell

@synthesize artistLabel;
@synthesize songLabel;
@synthesize timeLabel;

@end
