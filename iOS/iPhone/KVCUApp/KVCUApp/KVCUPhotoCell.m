//
//  KVCUPhotoCell.m
//  KVCUApp
//
//  Created by Ian Finlay on 4/2/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUPhotoCell.h"

@implementation KVCUPhotoCell

@synthesize image;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self setNeedsDisplay]; // force drawRect:
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
