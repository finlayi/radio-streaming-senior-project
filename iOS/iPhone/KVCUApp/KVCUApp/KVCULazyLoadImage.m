//
//  KVCULazyLoadImage.m
//  KVCUApp
//
//  Created by Ian Finlay on 2/10/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCULazyLoadImage.h"

@implementation KVCULazyLoadImage

@synthesize delegate;
@synthesize indexPath;

- (id)initWithUrl:(NSURL *)url withPlaceholder:(NSString *)imageName withDelegate:(id<KVCULazyLoadImageDelegate>)paramDelegate withIndex:(NSIndexPath *)imageIndexPath
{
    self = [super init];
    if (self) {
        receivedData = [[NSMutableData alloc] init];
        self.delegate = paramDelegate;
        self.indexPath = imageIndexPath;
        self.image = [UIImage imageNamed:imageName];
        [self loadWithUrl:url];
    }
    return self;
}

- (id)initWithUrl:(NSURL *)url withPlaceholder:(NSString *)imageName
{
    self = [super init];
    if (self) {
        receivedData = [[NSMutableData alloc] init];
        self.image = [UIImage imageNamed:imageName];
        [self loadWithUrl:url];
    }
    return self;
}

- (void)loadWithUrl:(NSURL *)url
{
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:
                                   [NSURLRequest requestWithURL:url] delegate:self];
    [connection self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSData *)data
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    self.image = [[UIImage alloc] initWithData:receivedData];
    
    [UIImageView transitionWithView:self
                      duration:0.2f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.image = [[UIImage alloc] initWithData:receivedData];
                    } completion:NULL];
    
    [self.delegate imageDidFinishLoading:self];
}

@end
