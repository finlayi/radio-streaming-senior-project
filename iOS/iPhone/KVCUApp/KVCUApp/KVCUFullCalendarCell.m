//
//  KVCUFullCalendarCell.m
//  KVCUApp
//
//  Created by Ian Finlay on 4/3/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUFullCalendarCell.h"

@implementation KVCUFullCalendarCell

@synthesize titleLabel;
@synthesize dateLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
