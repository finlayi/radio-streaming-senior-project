//
//  KVCUPlaylistDetailTableViewCell.h
//  KVCUApp
//
//  Created by Ian Finlay on 12/8/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVCULazyLoadImage.h"

/**
 * @brief
 * Custom table view cell for detailed table view
 */
@interface KVCUPlaylistDetailTableViewCell : UITableViewCell

/// Label for name of song
@property (nonatomic, weak) IBOutlet UILabel *songLabel;

/// Label for name of artist
@property (nonatomic, weak) IBOutlet UILabel *artistLabel;

/// Space for album art
@property (nonatomic, strong) IBOutlet KVCULazyLoadImage *albumImage;

/// Background for similar song cell
@property (nonatomic, weak) IBOutlet UIImageView* cellBackground;

@end
