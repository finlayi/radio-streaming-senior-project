//
//  KVCUXMLDocument.m
//  KVCUApp
//
//  Created by Ian Finlay on 12/4/12.
//  Copyright (c) 2012 CU Senior Projects. All rights reserved.
//

#import "KVCUXMLDocument.h"

@implementation KVCUXMLDocument

@synthesize rootElement;
@synthesize xmlParser;
@synthesize currentElement;
@synthesize connection;
@synthesize connectionData;
@synthesize parsingErrorHasHappened;
@synthesize delegate;

- (id)init
{
    return [self initWithDelegate:nil];
}

- (id)initWithDelegate:(id<KVCUXMLDocumentDelegate>)paramDelegate
{
    self = [super init];
    
    if (self != nil) {
        self.delegate = paramDelegate;
    }
    
    return self;
}

- (BOOL)parseLocalXMLWithPath:(NSString *)paramLocalXMLPath
{
    return FALSE;
}

- (BOOL)parseRemoteXMLWithPath:(NSString *)paramRemoteXMLURL
{
    BOOL result = NO;
    
    if ([paramRemoteXMLURL length] == 0) {
        NSLog(@"URL is empty");
        return NO;
    }
    
    paramRemoteXMLURL = [paramRemoteXMLURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    self.connection = nil;
    
    NSURL *url = [NSURL URLWithString:paramRemoteXMLURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    self.connectionData = nil;
    
    NSMutableData *newData = [[NSMutableData alloc] init];
    self.connectionData = newData;
    
    NSURLConnection *newConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    self.connection = newConnection;
        
    return result;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.connectionData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.connectionData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"A connection error has occured");
    [self.delegate xmlDocumentDelegateParsingFailed:self withError:error];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (self.connectionData != nil) {
        NSXMLParser *newParser = [[NSXMLParser alloc] initWithData:self.connectionData];
        self.xmlParser = newParser;
        
        [self.xmlParser setShouldProcessNamespaces:NO];
        [self.xmlParser setShouldReportNamespacePrefixes:NO];
        [self.xmlParser setShouldResolveExternalEntities:NO];
        [self.xmlParser setDelegate:self];
        
        if ([self.xmlParser parse] == YES) {
            DLog(@"Successfully parsed");
        } else {
            NSLog(@"Failed to parse");
        }
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    NSLog(@"Parsing error occured");
    self.parsingErrorHasHappened = YES;
    [parser abortParsing];
    [self.delegate xmlDocumentDelegateParsingFailed:self withError:parseError];
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    NSLog(@"Validation error in parsing");
    self.parsingErrorHasHappened = YES;
    [self.delegate xmlDocumentDelegateParsingFailed:self withError:validationError];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    self.parsingErrorHasHappened = NO;
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    if (self.parsingErrorHasHappened == NO) {
        [self.delegate xmlDocumentDelegateParsingFinished:self];
    }
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if (self.rootElement == nil) {
        KVCUXMLElement *newElement = [[KVCUXMLElement alloc] init];
        self.rootElement = newElement;
        self.currentElement = self.rootElement;
    } else {
        KVCUXMLElement *newElement = [[KVCUXMLElement alloc] init];
        newElement.parent = self.currentElement;
        [self.currentElement.children addObject:newElement];
        self.currentElement = newElement;
    }
    self.currentElement.name = elementName;
    
    if ([attributeDict count] > 0) {
        [self.currentElement.attributes addEntriesFromDictionary:attributeDict];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
//    NSString *decodedString = [NSString stringWithUTF8String:[string cStringUsingEncoding:[NSString defaultCStringEncoding]]];
    if (self.currentElement != nil) {
        if (self.currentElement.text == nil) {
            self.currentElement.text = string;
        } else {
            self.currentElement.text = [self.currentElement.text stringByAppendingString:string];
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if (self.currentElement != nil) {
        self.currentElement = self.currentElement.parent;
    }
}

@end
