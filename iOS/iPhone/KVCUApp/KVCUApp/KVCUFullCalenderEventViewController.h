//
//  KVCUFullCalenderEventViewController.h
//  KVCUApp
//
//  Created by Ian Finlay on 4/3/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KVCUFullCalenderEventViewController : UIViewController<UIWebViewDelegate>

/// Web view for event detail
@property (nonatomic, strong) IBOutlet UIWebView* webView;

/// URL for web view to connect to
@property (nonatomic, strong) NSString* eventURL;

/// Event title
@property (nonatomic, strong) NSString* eventTitle;

/// Loading indicator
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView* loading;

@end
