//
//  KVCUCalendarViewController.h
//  KVCUApp
//
//  Created by Ian Finlay on 2/5/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "KVCUXMLDocument.h"
#import "KVCULazyLoadImage.h"
#import "ECSlidingViewController.h"

/**
 * @brief
 * Calendar screen view controller
 */
@interface KVCUCalendarViewController : UITableViewController<KVCUXMLDocumentDelegate, KVCULazyLoadImageDelegate>
{
    @public
    /**
     * The XML document to pull calendar information from
     */
    KVCUXMLDocument *xmlDocument;
}

@property (nonatomic, strong) KVCUXMLDocument *xmlDocument;

/// Table view for list of events
@property (nonatomic, strong) IBOutlet UITableView *tableView;

/// List of events
@property (nonatomic, strong) NSMutableArray *eventList;

/// Controller for switchign between event and DJ schedule views
@property (nonatomic, weak) IBOutlet UISegmentedControl *viewSwitch;

/// Currently loaded event list page (starts at 1)
@property (nonatomic, assign) NSInteger currentPage;

/// If the user has pressed the refresh button (resets to NO aftrer refresh)
@property (nonatomic, assign) BOOL refresh;

/// Dictionary of images for all events
@property (nonatomic ,strong) NSMutableDictionary *imageDict;

/// Dictionary of DJ show arrays with day as their key
@property (nonatomic, strong) NSMutableDictionary *djList;

/// DJ XML Parsing document
@property (nonatomic, strong) KVCUXMLDocument *djDocument;

/// Current switch segment activated in the view
@property (nonatomic, assign) NSInteger currentSegment;

/// Variable to represent first load for animation
@property (nonatomic, assign) BOOL firstLoad;

- (IBAction)revealMenu:(id)sender;

@end
