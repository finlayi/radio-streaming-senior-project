//
//  KVCUXMLElementTests.m
//  KVCUApp
//
//  Created by Jackie Myrose on 1/21/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUXMLElementTests.h"
#import "KVCUXMLElement.h"

@implementation KVCUXMLElementTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testInit
{
    KVCUXMLElement *testElement = [[KVCUXMLElement alloc] init];
    
    STAssertNotNil(testElement, @"testElement is nil");
    STAssertNotNil([testElement children], @"testElement.children is nil");
    STAssertNotNil([testElement attributes], @"testElement.attributes is nil");
}

@end
