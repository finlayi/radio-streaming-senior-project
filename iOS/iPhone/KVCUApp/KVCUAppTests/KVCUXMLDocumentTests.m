//
//  KVCUXMLDocumentTests.m
//  KVCUApp
//
//  Created by Jackie Myrose on 1/21/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import "KVCUXMLDocumentTests.h"
#import "KVCUXMLDocument.h"

@implementation KVCUXMLDocumentTests

KVCUXMLDocument *testDocument;

- (void)setUp
{
    [super setUp];
    
    testDocument = [[KVCUXMLDocument alloc] init];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testInit
{
    STAssertNotNil(testDocument, @"testDocument is nil");
}

- (void)testParseLocalXML
{
    STAssertFalse([testDocument parseLocalXMLWithPath:nil], @"testDocument.parseLocalXMLWithPath not False");
}

- (void)testParseRemoteXML
{
    STAssertFalse([testDocument parseRemoteXMLWithPath:nil], @"testDocument.parseRemoteXMLWithPath given nil not returning False");
}

@end
