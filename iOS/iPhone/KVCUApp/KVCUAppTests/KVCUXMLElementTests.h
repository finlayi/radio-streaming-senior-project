//
//  KVCUXMLElementTests.h
//  KVCUApp
//
//  Created by Jackie Myrose on 1/21/13.
//  Copyright (c) 2013 CU Senior Projects. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

@interface KVCUXMLElementTests : SenTestCase

- (void)testInit;

@end
