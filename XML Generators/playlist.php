<?php

function parseURL($url) {
    $contents = file_get_contents($url);

    $startPos = strpos($contents, '<tbody>');
    $endPos = strpos($contents, '</tbody>');
    
    $table = substr($contents, $startPos, ($endPos+8)-$startPos);
    
    return $table;
}

$playlistArray = array();

function getData($table, $playlistArray) {
    $contents = $table;
    $DOM = new DomDocument;
    $DOM->loadHTML($contents);
    
    $items = $DOM->getElementsByTagName('tr');
    
    function tdRows($elements) {
        $elementArray = array();
        $count = 0;
        foreach ($elements as $element) {
            $str = $element->nodeValue;
            if ($count == 0) {
                $elementArray['time'] = htmlspecialchars($str);
            }
            if ($count == 2) {
                $elementArray['artist'] = htmlspecialchars($str);
            }
            if ($count == 4) {
                $elementArray['song'] = htmlspecialchars($str);
            }
            if ($count == 6) {
                $elementArray['album'] = htmlspecialchars($str);
            }
            $count++;
        }
        return $elementArray;
    }
    foreach ($items as $node) {
        $playlistArray[] = (tdRows($node->childNodes)); 
    }
    return $playlistArray;
}

function arrayToXML($array, $xml) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)) {
                $subnode = $xml->addChild("$key");
                arrayToXML($value, $subnode);
            } else {
                arrayToXML($value, $xml);
            }
        } else {
            $xml->addChild("$key", "$value");
        }
    }
}

function makeURLToParse() {
    $url = 'http://radio1190.colorado.edu/playlist';
    $date = $_REQUEST['id'];
    $dateParts = explode("-", $date);
    $newDate = $dateParts[0] . "%2F" . $dateParts[1] . "%2F" . $dateParts[2];
    
    if($date != NULL) {
        $url = 'http://radio1190.colorado.edu/playlist' . '?day=' . $newDate;
    }
    
    return $url;
}

$url = makeURLToParse();
$table = parseURL($url);

$playlistArray = getData($table, $playlistArray);

$xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><playlist></playlist>");
arrayToXML($playlistArray, $xml);

header('Content-Type: text/html; charset=UTF-8');

print $xml->asXML();