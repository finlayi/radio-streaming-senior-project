<?php
include('simple_html_dom.php');

function scrapeEvents($url) {
    $html = file_get_html($url);
    
    foreach($html->find('div[class="type-tribe_events"]') as $article) {
    
        $item['title'] = trim($article->find('h2', 0)->plaintext);
        
        $item['imageLink'] = trim($article->find('img[class="size-full"]',0)->src);
        $item['startDate'] = trim($article->find('dd[class="event-meta-start"]',0)->plaintext);
        $item['endDate'] = trim($article->find('dd[class="event-meta-end"]',0)->plaintext);
        $item['venue'] = trim($article->find('dd[class="event-meta-venue"]',0)->plaintext);
        $item['phone'] = trim($article->find('dd[itemprop="telephone"]',0)->plaintext);
        $item['address'] = trim($article->find('div[itemprop="address"]',0)->plaintext);        
                
        $item = preg_replace("/&#?[a-z0-9]+;/i","",$item);
        
        $ret[] = $item;
    }
    
    $html->clear();
    unset($html);
    
    return $ret;
}

function arrayToXML($array, $xml) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)) {
                $subnode = $xml->addChild("$key");
                arrayToXML($value, $subnode);
            } else {
                arrayToXML($value, $xml);
            }
        } else {
            $xml->addChild("$key", "$value");
        }
    }
}

function makeURLToParse() {
    $url = 'http://www.radio1190.org/event/%e2%9c%aa-purity-ring/';
    $page = $_REQUEST['id'];
    
    if($page != NULL) {
        $url = 'http://www.radio1190.org/events/upcoming/' . 'page/' . $page;
    }
    
    return $url;
}
$url = makeURLToParse();
$ret = scrapeEvents($url);

$xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><event></event>");
arrayToXML($ret, $xml);
header('Content-Type: text/html; charset=UTF-8');

print $xml->asXML();
/*
foreach ($ret as $v) {
    echo $v['date'].'<br>';
    echo utf8_decode($v['title']).'<br>';
    echo utf8_decode($v['content']).'<br>';
    echo $v['image'].'<br>';
    echo $v['startDate'].'<br>';
    echo $v['endDate'].'<br>';
    echo $v['venue'].'<br>';
    echo $v['phone'].'<br>';
    echo $v['address'].'<br>';
}
*/