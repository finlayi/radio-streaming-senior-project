<?php
include('simple_html_dom.php');

function scrapeEvents($url) {
    $html = file_get_html($url);
    
    foreach($html->find('div.show') as $article) {
        $item['dayName'] = htmlspecialchars(trim($article->find('p[class="dayName"]', 0)->plaintext));
        $item['startTime'] = htmlspecialchars(trim($article->find('p[class="startTime"]', 0)->plaintext));
        $item['endTime'] = htmlspecialchars(trim($article->find('p[class="endTime"]',0)->plaintext));
        $item['showName'] = htmlspecialchars(trim($article->find('p[class="showName"]',0)->plaintext));
        $item['showDesc'] = htmlspecialchars(trim($article->find('p[class="showDesc"]',0)->plaintext));
        $item['showSite'] = htmlspecialchars(trim($article->find('p[class="showSite"]',0)->plaintext));
        
        //$item = preg_replace("/&#?[a-z0-9]+;/i","",$item);
        
        $ret[] = $item;
    }
    
    $html->clear();
    unset($html);
    
    return $ret;
}

function arrayToXML($array, $xml) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)) {
                $subnode = $xml->addChild("$key");
                arrayToXML($value, $subnode);
            } else {
                arrayToXML($value, $xml);
            }
        } else {
            $xml->addChild("$key", "$value");
        }
    }
}

function makeURLToParse() {
    $url = 'http://www.hireianfinlay.com/radio1190/djSched.html';
    
    return $url;
}
$url = makeURLToParse();
$ret = scrapeEvents($url);

$xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><djsched></djsched>");
arrayToXML($ret, $xml);
header('Content-Type: text/html; charset=UTF-8');

print $xml->asXML();
/*
foreach ($ret as $v) {
    echo $v['date'].'<br>';
    echo utf8_decode($v['title']).'<br>';
    echo utf8_decode($v['content']).'<br>';
    echo $v['image'].'<br>';
    echo $v['startDate'].'<br>';
    echo $v['endDate'].'<br>';
    echo $v['venue'].'<br>';
    echo $v['phone'].'<br>';
    echo $v['address'].'<br>';
}
*/