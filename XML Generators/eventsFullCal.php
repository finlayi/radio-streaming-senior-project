<?php
include('simple_html_dom.php');

function scrapeEvents($url) {
    $html = file_get_html($url);
    
    foreach($html->find('td[class="tribe-events-thismonth"]') as $article) {
        $item = NULL;
/*
        $item['title'] = trim($article->find('h2', 0)->plaintext);
        $item['content'] = trim($article->find('p', 0)->plaintext);
        $item['imageLink'] = trim($article->find('img[class="size-full"]',0)->src);
        $item['startDate'] = trim($article->find('td[itemprop="startDate"]',0)->plaintext);
        $item['endDate'] = trim($article->find('td[itemprop="endDate"]',0)->plaintext);
        $item['venue'] = trim($article->find('td[itemprop="name"]',0)->plaintext);
        $item['phone'] = trim($article->find('td[itemprop="telephone"]',0)->plaintext);
        $item['address'] = trim($article->find('div[itemprop="address"]',0)->plaintext);
*/
        $dayNum = explode('_', trim($article->find('div[class=daynum]', 0)->id));
        $item['day'] = $dayNum[1];
        
        $count = 0;
        
        foreach($article->find('div[class=tribe-events-real-event]') as $event) {
            $item['event-' . $count] = trim($event->find('h5', 0)->plaintext);
            $item['event-' . $count . '-link'] = trim($event->find('a',0)->href);
            $item['event-' . $count . '-date'] = trim($event->find('div[class=tribe-events-event-date]', 0)->plaintext);
            $count++;
        }
        
        $item = preg_replace("/&#?[a-z0-9]+;/i","",$item);
        
        $ret[] = $item;
    }
    
    $html->clear();
    unset($html);
    
    return $ret;
}

function arrayToXML($array, $xml) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)) {
                $subnode = $xml->addChild("$key");
                arrayToXML($value, $subnode);
            } else {
                arrayToXML($value, $xml);
            }
        } else {
            $xml->addChild("$key", "$value");
        }
    }
}

function makeURLToParse() {
    $url = 'http://www.radio1190.org/events/';
    $page = $_REQUEST['id'];
    
    if($page != NULL) {
        $url = 'http://www.radio1190.org/events/' . $page;
    }
    
    return $url;
}
$url = makeURLToParse();
$ret = scrapeEvents($url);

$xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><events></events>");
arrayToXML($ret, $xml);
header('Content-Type: text/html; charset=UTF-8');

print $xml->asXML();
/*
foreach ($ret as $v) {
    echo $v['date'].'<br>';
    echo utf8_decode($v['title']).'<br>';
    echo utf8_decode($v['content']).'<br>';
    echo $v['image'].'<br>';
    echo $v['startDate'].'<br>';
    echo $v['endDate'].'<br>';
    echo $v['venue'].'<br>';
    echo $v['phone'].'<br>';
    echo $v['address'].'<br>';
}
*/