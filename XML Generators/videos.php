<?php
include('simple_html_dom.php');

function scrapeEvents($url) {
    $oldhtml = file_get_html($url);
    
    $newhtml = str_replace('video_page_posts_last', 'video_page_posts', $oldhtml);
    
    $html = str_get_html($newhtml);
    
    foreach($html->find('div.video_page_posts') as $article) {
        $item['description'] = trim($article->find('span[class=short_desc]', 0)->plaintext);
        $videoHref = trim($article->find('a',0)->href);
        preg_match('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $videoHref, $matches);
        $item['videoLink'] = $matches[0];
        //print_r($matches);
        $item['thumbnail'] = trim($article->find('img',0)->src);
        
        $item = preg_replace("/&#?[a-z0-9]+;/i","",$item);
        
        $ret[] = $item;
    }
    
    $html->clear();
    unset($html);
    
    return $ret;
}

function arrayToXML($array, $xml) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)) {
                $subnode = $xml->addChild("$key");
                arrayToXML($value, $subnode);
            } else {
                arrayToXML($value, $xml);
            }
        } else {
            $xml->addChild("$key", "$value");
        }
    }
}

function makeURLToParse() {
    $url = 'http://www.radio1190.org/videos';
    
    return $url;
}
$url = makeURLToParse();
$ret = scrapeEvents($url);

$xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><videos></videos>");
arrayToXML($ret, $xml);
header('Content-Type: text/html; charset=UTF-8');

print $xml->asXML();
/*
foreach ($ret as $v) {
    echo $v['date'].'<br>';
    echo utf8_decode($v['title']).'<br>';
    echo utf8_decode($v['content']).'<br>';
    echo $v['image'].'<br>';
    echo $v['startDate'].'<br>';
    echo $v['endDate'].'<br>';
    echo $v['venue'].'<br>';
    echo $v['phone'].'<br>';
    echo $v['address'].'<br>';
}
*/